import sys
from h5py import *
import numpy as np

filename = 'dataset.h5'
h5file = File(filename,mode='w')

Po = 1.0e6  # [Pa]
p_offset = 1.0e6  # [Pa]
L = 1.0     # [m]

# 1d line in x
# Pressure boundary condition
h5grp = h5file.create_group('x_line_node_centered')
h5grp.attrs['Dimension'] = np.string_('X')
# Delta length between points [m]
h5grp.attrs['Discretization'] = [1.]
# Location of origin
h5grp.attrs['Origin'] = [0.]
# Load the dataset values
nx = 2
rarray = np.zeros(nx,'=f8')
for i in range(nx):
  x = (float(i)*L)/(nx-1)
  rarray[i] = Po*(x/L) + p_offset
h5dset = h5grp.create_dataset('Data', data=rarray)

# 1d line in y
# Pressure boundary condition
h5grp = h5file.create_group('y_line_node_centered')
h5grp.attrs['Dimension'] = np.string_('Y')
# Delta length between points [m]
h5grp.attrs['Discretization'] = [1.]
# Location of origin
h5grp.attrs['Origin'] = [0.]
# Load the dataset values
ny = 2
rarray = np.zeros(ny,'=f8')
for j in range(ny):
  y = (float(j)*L)/(ny-1)
  rarray[j] = Po*(y/L) + p_offset
h5dset = h5grp.create_dataset('Data', data=rarray)