import sys

nx = 1; dx = 1; lx = 1
ny = 1; dy = 1; ly = 1
nz = 1; dz = 1; lz = 1
filename = ''
for k in range(len(sys.argv)-1):
  k = k + 1
  if sys.argv[k] == '-nx':
    nx = int(float(sys.argv[k+1]))
  if sys.argv[k] == '-ny':
    ny = int(float(sys.argv[k+1]))
  if sys.argv[k] == '-nz':
    nz = int(float(sys.argv[k+1]))
  if sys.argv[k] == '-dx':
    dx = float(sys.argv[k+1])
  if sys.argv[k] == '-dy':
    dy = float(sys.argv[k+1])
  if sys.argv[k] == '-dz':
    dz = float(sys.argv[k+1])
  if sys.argv[k] == '-lx':
    lx = float(sys.argv[k+1])
  if sys.argv[k] == '-ly':
    ly = float(sys.argv[k+1])
  if sys.argv[k] == '-lz':
    lz = float(sys.argv[k+1])
  if sys.argv[k] == '-input_prefix':
    filename = sys.argv[k+1] + '.in'

f = open(filename,'w')

simulation = """ 
SIMULATION
  SIMULATION_TYPE SUBSURFACE
  PROCESS_MODELS
    SUBSURFACE_FLOW flow
      MODE MPHASE
    /
  / 
END

"""
grid = """
GRID
  TYPE structured
  NXYZ """ + str(nx) + " " + str(ny) + " " + str(nz) + """
  DXYZ
   """ + str(dx) + """d0
   """ + str(dy) + """d0
   """ + str(dz) + """d0
  END
END
"""
region = """
REGION all
  COORDINATES
    0.0d0 0.0d0 0.0d0
    """ + str(lx) + "d0 " + str(ly) + "d0 " + str(lz) + """d0
  /
END

REGION left_end
  FACE WEST
  COORDINATES
    0.d0 0.d0 0.d0
    0.d0 """ + str(ly) + "d0 " + str(lz) + """d0
  /
END

REGION right_end
  FACE EAST
  COORDINATES
    """ + str(lx) + """d0 0.d0 0.d0
    """ + str(lx) + "d0 " + str(ly) + "d0 " + str(lz) + """d0
  /
END
"""
mat_prop = """
MATERIAL_PROPERTY sand1
  ID 1
  POROSITY 0.25
  TORTUOSITY 1.0
  ROCK_DENSITY 2700.
    SPECIFIC_HEAT 1E3
  THERMAL_CONDUCTIVITY_DRY 1.0d0
  THERMAL_CONDUCTIVITY_WET 3.1d0
  SATURATION_FUNCTION default
  PERMEABILITY
    PERM_ISO 1.d-12
  /
END
"""
char_curves = """
SATURATION_FUNCTION default
  SATURATION_FUNCTION_TYPE VAN_GENUCHTEN_DOUGHTY
  RESIDUAL_SATURATION LIQUID_PHASE 0.1d0
  RESIDUAL_SATURATION GAS_PHASE 0.1d0
  LAMBDA 0.8d0
  ALPHA 1.0
  MAX_CAPILLARY_PRESSURE 1.d7
END
"""

fluid_prop = """
CO2_DATABASE co2data0.dat
FLUID_PROPERTY
DIFFUSION_COEFFICIENT 1.d-9
/
"""
strata = """
STRATA
  REGION all
  MATERIAL sand1
END
"""
time = """
TIME
  FINAL_TIME 0.8d0 y
  INITIAL_TIMESTEP_SIZE 1.d0 h
  MAXIMUM_TIMESTEP_SIZE 5.d-2 y
END
"""
output = """
OUTPUT
  SNAPSHOT_FILE
    TIMES y 0.01 0.1 0.5 0.80
    NO_PRINT_INITIAL
    FORMAT HDF5
  /
  SNAPSHOT_FILE
    TIMES y 0.01 0.1 0.5 0.8
    FORMAT TECPLOT POINT
  /
END
"""
flow_cond = """
FLOW_CONDITION saturated
UNITS Pa,C,M,yr
TYPE
  PRESSURE hydrostatic
  TEMPERATURE dirichlet
  CONCENTRATION dirichlet
  ENTHALPY dirichlet
/
IPHASE 1
  PRESSURE 1.d7 1.d7#about 1km deep with hydrostatic pressure
  TEMPERATURE 50.0 #about 1km deep with typical geothermal gradient 
  CONCENTRATION 0.0
  ENTHALPY 0.d0 0.d0
/
FLOW_CONDITION gas_injection
UNITS Pa,C,M,yr
  SYNC_TIMESTEP_WITH_UPDATE
  TYPE
    RATE mass_rate
    PRESSURE dirichlet
    TEMPERATURE dirichlet
    CONCENTRATION dirichlet
    ENTHALPY dirichlet
  /
  RATE LIST
    TIME_UNITS y
    DATA_UNITS g/s
      0.  0. 8.d-3
      0.8 0. 0.
    /
  PRESSURE 1.d7 1.d7
  TEMPERATURE 50.0
  CONCENTRATION 0.d0
  ENTHALPY 0.d0 0.d0
/
"""
init_cond = """
INITIAL_CONDITION initial
  REGION all
  FLOW_CONDITION saturated
END

SOURCE_SINK left_end
  REGION left_end
  FLOW_CONDITION gas_injection
END

BOUNDARY_CONDITION right_end
  FLOW_CONDITION saturated
  REGION right_end
END
"""
  
  
f.write(simulation)
f.write("SUBSURFACE\n")
f.write(grid)
f.write(region)
f.write(mat_prop)
f.write(char_curves)
f.write(fluid_prop)
f.write(strata)
f.write(time)
f.write(output)
f.write(flow_cond)
f.write(init_cond)
f.write("\nEND_SUBSURFACE\n") 
# Must have \n at the end, or HDF5 output will not work
