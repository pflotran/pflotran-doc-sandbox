import sys

nx = 1; dx = 1; lx = 1
ny = 1; dy = 1; ly = 1
nz = 1; dz = 1; lz = 1
filename = ''
for k in range(len(sys.argv)-1):
  k = k + 1
  if sys.argv[k] == '-nx':
    nx = int(float(sys.argv[k+1]))
  if sys.argv[k] == '-ny':
    ny = int(float(sys.argv[k+1]))
  if sys.argv[k] == '-nz':
    nz = int(float(sys.argv[k+1]))
  if sys.argv[k] == '-dx':
    dx = float(sys.argv[k+1])
  if sys.argv[k] == '-dy':
    dy = float(sys.argv[k+1])
  if sys.argv[k] == '-dz':
    dz = float(sys.argv[k+1])
  if sys.argv[k] == '-lx':
    lx = float(sys.argv[k+1])
  if sys.argv[k] == '-ly':
    ly = float(sys.argv[k+1])
  if sys.argv[k] == '-lz':
    lz = float(sys.argv[k+1])
  if sys.argv[k] == '-input_prefix':
    filename = sys.argv[k+1] + '.in'

f = open(filename,'w')

simulation = """ 
SIMULATION
  SIMULATION_TYPE SUBSURFACE
  PROCESS_MODELS
    SUBSURFACE_FLOW flow
      MODE GENERAL ! two-phase flow and energy
      OPTIONS
        ANALYTICAL_DERIVATIVES
        ISOTHERMAL ! ignore energy component for this simulation
	GAS_COMPONENT_FORMULA_WEIGHT 28.9598 g/mol
      /
    /
  /
END

"""
grid = """
GRID
  TYPE structured
  NXYZ """ + str(nx) + " " + str(ny) + " " + str(nz) + """
  DXYZ
   """ + str(dx) + """d0
   """ + str(dy) + """d0
   """ + str(dz) + """d0
  END
END
"""
region = """
REGION all
  COORDINATES
    0.0d0 0.0d0 0.0d0
    """ + str(lx) + "d0 " + str(ly) + "d0 " + str(lz) + """d0
  /
END

REGION left_end
  FACE WEST
  COORDINATES
    0.d0 0.d0 0.d0
    0.d0 """ + str(ly) + "d0 " + str(lz) + """d0
  /
END

REGION right_end
  FACE EAST
  COORDINATES
    """ + str(lx) + """d0 0.d0 0.d0
    """ + str(lx) + "d0 " + str(ly) + "d0 " + str(lz) + """d0
  /
END
"""
mat_prop = """
MATERIAL_PROPERTY soil1
  ID 1
  CHARACTERISTIC_CURVES default
  POROSITY 0.25
  TORTUOSITY 1.0
  ROCK_DENSITY 2700.
  THERMAL_CONDUCTIVITY_DRY 1.0d0
  THERMAL_CONDUCTIVITY_WET 3.1d0
  HEAT_CAPACITY 830.
  PERMEABILITY
    PERM_ISO 1.d-12
  /
END
"""

char_curves = """
CHARACTERISTIC_CURVES default
  SATURATION_FUNCTION VAN_GENUCHTEN
    ALPHA 1.d0
    M 0.8
    LIQUID_RESIDUAL_SATURATION 0.1d0
    MAX_CAPILLARY_PRESSURE 1.d6
  /
  PERMEABILITY_FUNCTION BURDINE_BC_LIQ
    PHASE LIQUID
    LAMBDA 0.8
    LIQUID_RESIDUAL_SATURATION 0.1d0
  /
  PERMEABILITY_FUNCTION BURDINE_BC_GAS
    PHASE GAS
    LAMBDA 0.8
    LIQUID_RESIDUAL_SATURATION 0.1d0
    GAS_RESIDUAL_SATURATION 0.1d0
  /
END
"""

fluid_prop = """
FLUID_PROPERTY
  PHASE LIQUID
  DIFFUSION_COEFFICIENT 1.d-9
END

FLUID_PROPERTY
  PHASE GAS
  DIFFUSION_COEFFICIENT 1.d-9
END

EOS WATER
  #DENSITY CONSTANT  1.0d3 kg/m^3
  VISCOSITY CONSTANT 1.0d-3 Pa-s
END

EOS GAS
  #DENSITY CONSTANT  4.0876d-2 kmol/m^3 #ideal gas at 25C and 101325 Pa
  VISCOSITY CONSTANT 1.61d-5 Pa-s
  #HENRYS_CONSTANT CONSTANT 8.d6
  HENRYS_CONSTANT CONSTANT 1.5d8
  FORMULA_WEIGHT 28.9598 g/mol
END
"""

strata = """
STRATA
  REGION all
  MATERIAL soil1
END
"""
time = """
TIME
  FINAL_TIME 1.d0 y
  INITIAL_TIMESTEP_SIZE 1.d0 h
  MAXIMUM_TIMESTEP_SIZE 5.d-2 y
END
"""
output = """
OUTPUT
  SNAPSHOT_FILE
    TIMES y 0.01 0.1 0.5 1.0
    NO_PRINT_INITIAL
    FORMAT HDF5
  /
  SNAPSHOT_FILE
    TIMES y 0.01 0.1 0.5 1.0
    FORMAT TECPLOT POINT
  /
END
"""
flow_cond = """
FLOW_CONDITION saturated #single phase liquid
  TYPE
    LIQUID_PRESSURE DIRICHLET
    MOLE_FRACTION DIRICHLET
    TEMPERATURE DIRICHLET
  /
  LIQUID_PRESSURE 101325. #Pa
  MOLE_FRACTION 1.0e-8
  TEMPERATURE 25.d0
END

FLOW_CONDITION gas_injection
  TYPE
    RATE MASS_RATE #there is only one cell in the injection region
  /
  RATE 0.0 7.5d-5 0.d0 g/s g/s MW
END
"""

init_cond = """
INITIAL_CONDITION all
  FLOW_CONDITION saturated
  REGION all
END

BOUNDARY_CONDITION outlet
  FLOW_CONDITION saturated
  REGION right_end
END

SOURCE_SINK gas
  FLOW_CONDITION gas_injection
  REGION left_end
END
"""

  
f.write(simulation)
f.write("SUBSURFACE\n")
f.write(grid)
f.write(region)
f.write(mat_prop)
f.write(char_curves)
f.write(fluid_prop)
f.write(strata)
f.write(time)
f.write(output)
f.write(flow_cond)
f.write(init_cond)
f.write("\nEND_SUBSURFACE\n") 
# Must have \n at the end, or HDF5 output will not work
