import numpy as np
np.set_printoptions(threshold=np.inf)
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import math
import os.path
#import sympy as sym
from scipy.special import erf
from scipy.optimize import minimize
from qa_tests_helper import *


#== global variables ==#
#======================#
epsilon_value = 1.e-30
#======================#

###############################################################################
def run_pflotran(input_prefix,nxyz,dxyz,lxyz,remove,screen_on,pf_exe,mpi_exe):
  nx = int(nxyz[0]); ny = int(nxyz[1]); nz = int(nxyz[2])
  dx = dxyz[0]; dy = dxyz[1]; dz = dxyz[2]
  lx = lxyz[0]; ly = lxyz[1]; lz = lxyz[2]
  command_line = ""
  command_line = "python generate_input_deck.py " 
  command_line = command_line + "-nx " + str(nx) + " "
  command_line = command_line + "-ny " + str(ny) + " "
  command_line = command_line + "-nz " + str(nz) + " "
  command_line = command_line + "-dx " + str(dx) + " "
  command_line = command_line + "-dy " + str(dy) + " "
  command_line = command_line + "-dz " + str(dz) + " "
  command_line = command_line + "-lx " + str(lx) + " "
  command_line = command_line + "-ly " + str(ly) + " "
  command_line = command_line + "-lz " + str(lz) + " "
  command_line = command_line + "-input_prefix " + input_prefix + " "
  os.system(command_line)
  command_line = ""
  command_line = command_line + mpi_exe + " -n 1 "
  command_line = command_line + pf_exe + " "
  command_line = command_line + "-input_prefix " + input_prefix + " "
  if not screen_on:
    command_line = command_line + " > screen.txt"
    #command_line = command_line + "-screen_output off "
  print 'Running PFLOTRAN simulation . . . '
  os.system(command_line)
  if remove:
    os.system('rm *.out')
  return
###############################################################################



################################################################################
#### STEADY THERMAL TESTS ######################################################
################################################################################

#==============================================================================#
def thermal_steady_1D_BC1stkind(path,input_prefix,remove,screen_on,pf_exe,
				                             mpi_exe,num_tries):
#==============================================================================#
# Based on:
# Kolditz, et al. (2015) Thermo-Hydro-Mechanical-Chemical Processes in
# Fractured Porous Media: Modelling and Benchmarking, Closed Form Solutions,
# Springer International Publishing, Switzerland.
# Section 2.1.1, pg.14
# "A 1D Steady-State Temperature Distribution, Boundary Conditions of 1st Kind"
#
# Author: Jenn Frederick
# Date: 06/27/2016
# ******************************************************************************
  nxyz = np.zeros(3) + 1
  dxyz = np.zeros(3) + 1.
  lxyz = np.zeros(3) + 1.
  error_analysis = np.zeros(num_tries)
  dxyz_record = np.zeros((num_tries,3))
  nxyz_record = np.zeros((num_tries,3))
  test_pass = False
  try_count = 0
  
  # initial discretization values
  lxyz[0] = 100.    # [m] lx
  nxyz[0] = 5       # [m] nx 
  dxyz = lxyz/nxyz  # [m]
  adjust_nondim_lxyz(dxyz,lxyz,1); dxyz = lxyz/nxyz
  T0 = 1.           # [C]
  ierr = 0
  
  while (not test_pass) and (try_count < num_tries): 
    print_discretization(lxyz,nxyz,dxyz)
    nx = int(nxyz[0]); ny = int(nxyz[1]); nz = int(nxyz[2])
    dx = dxyz[0]; dy = dxyz[1]; dz = dxyz[2]
    Lx = lxyz[0]; Ly = lxyz[1]; Lz = lxyz[2]
    try_count = try_count + 1

    x_soln = np.linspace(0.+(dx/2.),Lx-(dx/2.),nx)    # [m]
    x_pflotran = x_soln                               # [m]
    x_soln = np.concatenate(([0.],x_soln),axis=0)     # [m]
    x_soln = np.concatenate((x_soln,[Lx]),axis=0)     # [m]
    T_soln = np.zeros(nx+2)                           # [C]
    T_pflotran = np.zeros(nx)                         # [C]

    # create the analytical solution
    T_soln = np.array(x_soln/Lx + T0)                 # [C]
  
    # run PFLOTRAN simulation
    run_pflotran(input_prefix,nxyz,dxyz,lxyz,remove,screen_on,pf_exe,mpi_exe)

    # load data from hdf5
    output_str = '/1D_steady_thermal_BC_1st_kind.h5'
    index_str = 'Time:  1.00000E+02 y/Temperature [C]'
    T_pflotran[:] = read_pflotran_output_1D(path+output_str,index_str,remove)
    ierr = check(T_pflotran)
    
    max_percent_error = calc_relative_error(T_soln[1:nx+1],T_pflotran,ierr)
    record_error(error_analysis,nxyz_record,dxyz_record,max_percent_error,
                 nxyz,dxyz,try_count)
    test_pass = does_pass(max_percent_error,try_count,num_tries)
    increase_nxyz(nxyz,1,0); dxyz = lxyz/nxyz
    adjust_nondim_lxyz(dxyz,lxyz,1); dxyz = lxyz/nxyz

  # Plot the PFLOTRAN and analytical solutions
  plot_1D_steady(path,x_soln,T_soln,x_pflotran,T_pflotran,'Distance [m]',
                 'Temperature [C]',"{0:.2f}".format(max_percent_error))
  
  # Plot error analysis
  plot_error(error_analysis,nxyz_record,dxyz_record,path,try_count,1)
  
  # Add test result to report card
  add_to_report(path,test_pass,max_percent_error,ierr)

  return;
################################################################################


#==============================================================================#
def thermal_steady_1D_BC1st2ndkind(path,input_prefix,remove,screen_on,pf_exe,
				                             mpi_exe,num_tries):
#==============================================================================#
# Based on:
# Kolditz, et al. (2015) Thermo-Hydro-Mechanical-Chemical Processes in
# Fractured Porous Media: Modelling and Benchmarking, Closed Form Solutions,
# Springer International Publishing, Switzerland.
# Section 2.1.2, pg.15
# "A 1D Steady-State Temperature Distribution, Boundary Conditions of 1st and
# 2nd Kind"
#
# Author: Jenn Frederick
# Date: 06/27/2016
# *****************************************************************************
  nxyz = np.zeros(3) + 1
  dxyz = np.zeros(3) + 1.
  lxyz = np.zeros(3) + 1.
  error_analysis = np.zeros(num_tries)
  dxyz_record = np.zeros((num_tries,3))
  nxyz_record = np.zeros((num_tries,3))
  test_pass = False
  try_count = 0

  # initial discretization values
  lxyz[0] = 100.    # [m] lx
  nxyz[0] = 5       # [m] nx 
  dxyz = lxyz/nxyz  # [m]
  adjust_nondim_lxyz(dxyz,lxyz,1); dxyz = lxyz/nxyz
  K1 = 100.         # [W/m-C]
  K2 = 300.         # [W/m-C]
  q = -1.5          # [W/m^2]
  T0 = 1.           # [C]
  ierr = 0

  while (not test_pass) and (try_count < num_tries): 
    print_discretization(lxyz,nxyz,dxyz)
    nx = int(nxyz[0]); ny = int(nxyz[1]); nz = int(nxyz[2])
    dx = dxyz[0]; dy = dxyz[1]; dz = dxyz[2]
    Lx = lxyz[0]; Ly = lxyz[1]; Lz = lxyz[2]
    try_count = try_count + 1
    
    x_soln = np.linspace(0.+(dx/2.),Lx-(dx/2.),nx)    # [m]
    x_pflotran = x_soln                               # [m]
    x_soln = np.concatenate(([0.],x_soln),axis=0)     # [m]
    T_soln = np.zeros(nx+1)                           # [C]
    T_pflotran = np.zeros(nx)                         # [C]

    # create the analytical solution
    k = -1
    for j in x_soln:
      k = k + 1
      if j <= (2.*Lx/5.):
        T_soln[k] = np.array( -((q/K1)*j) + T0 )
      else:
        T_soln[k] = np.array( -((q/K2)*j) + T0 + (q*(2.*Lx/5.)*((1/K2)-(1/K1))) )

    # run PFLOTRAN simulation
    run_pflotran(input_prefix,nxyz,dxyz,lxyz,remove,screen_on,pf_exe,mpi_exe)

    # load data from hdf5
    index_string = 'Time:  1.00000E+02 y/Temperature [C]'
    T_pflotran[:] = read_pflotran_output_1D(path+
                    '/1D_steady_thermal_BC_1st_2nd_kind.h5',index_string,remove)
    ierr = check(T_pflotran)

    max_percent_error = calc_relative_error(T_soln[1:nx+1],T_pflotran,ierr)
    record_error(error_analysis,nxyz_record,dxyz_record,max_percent_error,
                 nxyz,dxyz,try_count)
    test_pass = does_pass(max_percent_error,try_count,num_tries)
    increase_nxyz(nxyz,1,0); dxyz = lxyz/nxyz
    adjust_nondim_lxyz(dxyz,lxyz,1); dxyz = lxyz/nxyz

  # Plot the PFLOTRAN and analytical solutions
  plot_1D_steady(path,x_soln,T_soln,x_pflotran,T_pflotran,'Distance [m]',
                 'Temperature [C]',"{0:.2f}".format(max_percent_error))
  
  # Plot error analysis
  plot_error(error_analysis,nxyz_record,dxyz_record,path,try_count,1)
  
  # Add test result to report card
  add_to_report(path,test_pass,max_percent_error,ierr)

  return;
################################################################################


#==============================================================================#
def thermal_steady_2D_BC1stkind(path,input_prefix,remove,screen_on,pf_exe,
				                             mpi_exe,num_tries):
#==============================================================================#
# Based on:
# Kolditz, et al. (2015) Thermo-Hydro-Mechanical-Chemical Processes in
# Fractured Porous Media: Modelling and Benchmarking, Closed Form Solutions,
# Springer International Publishing, Switzerland.
# Section 2.1.3, pg.16
# "A 2D Steady-State Temperature Distribution, Boundary Conditions of 1st Kind"
#
# Author: Jenn Frederick
# Date: 06/28/2016
# ******************************************************************************
  nxyz = np.zeros(3) + 1
  dxyz = np.zeros(3) + 1.
  lxyz = np.zeros(3) + 1.
  error_analysis = np.zeros(num_tries)
  dxyz_record = np.zeros((num_tries,3))
  nxyz_record = np.zeros((num_tries,3))
  lxyz[0] = 1.      # [m] lx
  lxyz[1] = 1.      # [m] ly
  test_pass = False
  try_count = 0

  # initial discretization values
  nxyz[0] = 5       # [m] nx
  nxyz[1] = 5       # [m] ny
  dxyz = lxyz/nxyz  # [m]
  adjust_nondim_lxyz(dxyz,lxyz,2); dxyz = lxyz/nxyz
  T0 = 1.           # [C]
  ierr = 0
  
  while (not test_pass) and (try_count < num_tries): 
    print_discretization(lxyz,nxyz,dxyz)
    nx = int(nxyz[0]); ny = int(nxyz[1]); nz = int(nxyz[2])
    dx = dxyz[0]; dy = dxyz[1]; dz = dxyz[2]
    Lx = lxyz[0]; Ly = lxyz[1]; Lz = lxyz[2]
    try_count = try_count + 1

    x_soln = np.linspace(0.+(dx/2.),Lx-(dx/2.),nx)     # [m]
    y_soln = np.linspace(0.+(dy/2.),Ly-(dy/2.),ny)     # [m]
    T_pflotran = np.zeros((nx,ny))                     # [C]
    T_soln = np.zeros((nx,ny))                         # [C]

    # create the analytical solution
    i = -1
    for x in x_soln:
      i = i + 1
      j = -1
      for y in y_soln:
        j = j + 1
        T_soln[i,j] = T0*(x/Lx)*(y/Ly)
        
    # run PFLOTRAN simulation
    run_pflotran(input_prefix,nxyz,dxyz,lxyz,remove,screen_on,pf_exe,mpi_exe)

    # load data from hdf5
    index_string = 'Time:  1.00000E+02 y/Temperature [C]'
    T_pflotran[:,:] = read_pflotran_output_2D(path+
		      '/2D_steady_thermal_BC_1st_kind.h5',index_string,remove)
    ierr = check(T_pflotran)

    max_percent_error = calc_relative_error(T_soln,T_pflotran,ierr)
    record_error(error_analysis,nxyz_record,dxyz_record,max_percent_error,
                 nxyz,dxyz,try_count)
    test_pass = does_pass(max_percent_error,try_count,num_tries)
    increase_nxyz(nxyz,2,0); dxyz = lxyz/nxyz
    adjust_nondim_lxyz(dxyz,lxyz,2); dxyz = lxyz/nxyz

  # Plot the PFLOTRAN and analytical solutions
  plot_2D_steady(path,x_soln,y_soln,T_soln,T_pflotran,'Distance [m]',
                 'Temperature [C]',"{0:.2f}".format(max_percent_error))
  
  # Plot error analysis
  plot_error(error_analysis,nxyz_record,dxyz_record,path,try_count,2)
  
  # Add test result to report card
  add_to_report(path,test_pass,max_percent_error,ierr)

  return;
################################################################################


#==============================================================================#
def thermal_steady_2D_BC1st2ndkind(path,input_prefix,remove,screen_on,pf_exe,
				                             mpi_exe,num_tries):
#==============================================================================#
# Based on:
# Kolditz, et al. (2015) Thermo-Hydro-Mechanical-Chemical Processes in
# Fractured Porous Media: Modelling and Benchmarking, Closed Form Solutions,
# Springer International Publishing, Switzerland.
# Section 2.1.4, pg.17
# "A 2D Steady-State Temperature Distribution, Boundary Conditions of 1st and
# 2nd Kind"
#
# Author: Jenn Frederick
# Date: 06/29/2016
# ******************************************************************************
  nxyz = np.zeros(3) + 1
  dxyz = np.zeros(3) + 1.
  lxyz = np.zeros(3) + 1.
  error_analysis = np.zeros(num_tries)
  dxyz_record = np.zeros((num_tries,3))
  nxyz_record = np.zeros((num_tries,3))
  lxyz[0] = 2.      # [m] lx
  lxyz[1] = 1.      # [m] ly
  test_pass = False
  try_count = 0

  # initial discretization values
  nxyz[0] = 8       # [m] nx
  nxyz[1] = 4       # [m] ny
  dxyz = lxyz/nxyz  # [m]
  adjust_nondim_lxyz(dxyz,lxyz,2); dxyz = lxyz/nxyz
  T0 = 1.           # [C]
  ierr = 0
  
  while (not test_pass) and (try_count < num_tries):  
    print_discretization(lxyz,nxyz,dxyz)
    nx = int(nxyz[0]); ny = int(nxyz[1]); nz = int(nxyz[2])
    dx = dxyz[0]; dy = dxyz[1]; dz = dxyz[2]
    Lx = lxyz[0]; Ly = lxyz[1]; Lz = lxyz[2]
    try_count = try_count + 1

    x_soln = np.linspace(0.+(dx/2.),Lx-(dx/2.),nx)  # [m]
    y_soln = np.linspace(0.+(dy/2.),Ly-(dy/2.),ny)  # [m]
    T_pflotran = np.zeros((nx,ny))                  # [C]
    T_soln = np.zeros((nx,ny))                      # [C]

    # create the analytical solution
    i = -1
    for x in x_soln:
      i = i + 1
      j = -1
      for y in y_soln:
        j = j + 1
        T_soln[i,j] = (T0/Ly)*(x+(2*y))
        
    # run PFLOTRAN simulation
    run_pflotran(input_prefix,nxyz,dxyz,lxyz,remove,screen_on,pf_exe,mpi_exe)

    # load data from hdf5
    index_string = 'Time:  1.00000E+02 y/Temperature [C]'
    T_pflotran[:,:] = read_pflotran_output_2D(path+
	              '/2D_steady_thermal_BC_1st_2nd_kind.h5',index_string,remove)
    ierr = check(T_pflotran)

    max_percent_error = calc_relative_error(T_soln,T_pflotran,ierr)
    record_error(error_analysis,nxyz_record,dxyz_record,max_percent_error,
                 nxyz,dxyz,try_count)
    test_pass = does_pass(max_percent_error,try_count,num_tries)
    increase_nxyz(nxyz,2,0); dxyz = lxyz/nxyz
    adjust_nondim_lxyz(dxyz,lxyz,2); dxyz = lxyz/nxyz

  # Plot the PFLOTRAN and analytical solutions
  plot_2D_steady(path,x_soln,y_soln,T_soln,T_pflotran,'Distance [m]',
                 'Temperature [C]',"{0:.2f}".format(max_percent_error))
  # Plot error analysis
  plot_error(error_analysis,nxyz_record,dxyz_record,path,try_count,2)
  
  # Add test result to report card
  add_to_report(path,test_pass,max_percent_error,ierr)

  return;
################################################################################


#==============================================================================#
def thermal_steady_3D_BC1stkind(path,input_prefix,remove,screen_on,pf_exe,
				                             mpi_exe,num_tries):
#==============================================================================#
# Based on:
# Kolditz, et al. (2015) Thermo-Hydro-Mechanical-Chemical Processes in
# Fractured Porous Media: Modelling and Benchmarking, Closed Form Solutions,
# Springer International Publishing, Switzerland.
# Section 2.1.5, pg.18
# "A 3D Steady-State Temperature Distribution"
#
# Author: Jenn Frederick
# Date: 06/28/2016
# ******************************************************************************
  nxyz = np.zeros(3) + 1
  dxyz = np.zeros(3) + 1.
  lxyz = np.zeros(3) + 1.
  error_analysis = np.zeros(num_tries)
  dxyz_record = np.zeros((num_tries,3))
  nxyz_record = np.zeros((num_tries,3))
  lxyz[0] = 1.      # [m] lx
  lxyz[1] = 1.      # [m] ly
  lxyz[2] = 1.      # [m] lz
  test_pass = False
  try_count = 0

  # initial discretization values
  nxyz[0] = 3       # [m] nx
  nxyz[1] = 3       # [m] ny
  nxyz[2] = 3       # [m] nz
  dxyz = lxyz/nxyz  # [m]
  T0 = 1.     # [C]
  ierr = 0
  
  while (not test_pass) and (try_count < num_tries): 
    print_discretization(lxyz,nxyz,dxyz)
    nx = int(nxyz[0]); ny = int(nxyz[1]); nz = int(nxyz[2])
    dx = dxyz[0]; dy = dxyz[1]; dz = dxyz[2]
    Lx = lxyz[0]; Ly = lxyz[1]; Lz = lxyz[2]
    try_count = try_count + 1

    T_soln = np.zeros((nx,ny,nz))                   # [C]
    T_pflotran = np.zeros((nx,ny,nz))               # [C]
    x_soln = np.linspace(0.+(dx/2.),Lx-(dx/2.),nx)  # [m]
    y_soln = np.linspace(0.+(dy/2.),Ly-(dx/2.),ny)  # [m]
    z_soln = np.linspace(0.+(dz/2.),Lz-(dx/2.),nz)  # [m]

    # create the analytical solution
    i = -1
    for x in x_soln:
      i = i + 1
      j = -1
      for y in y_soln:
        j = j + 1
        k = -1
        for z in z_soln:
          k = k + 1
          T_soln[i,j,k] = T0*((x/Lx)+(y/Ly)+(z/Lz))

    # run PFLOTRAN simulation
    run_pflotran(input_prefix,nxyz,dxyz,lxyz,remove,screen_on,pf_exe,mpi_exe)
    
    # load data from hdf5
    index_string = 'Time:  1.00000E+01 y/Temperature [C]'
    T_pflotran[:,:,:] = read_pflotran_output_3D(path+
			'/3D_steady_thermal_BC_1st_kind.h5',
                        index_string,remove)
    ierr = check(T_pflotran)

    max_percent_error = calc_relative_error(T_soln,T_pflotran,ierr)
    record_error(error_analysis,nxyz_record,dxyz_record,max_percent_error,
                 nxyz,dxyz,try_count)
    test_pass = does_pass(max_percent_error,try_count,num_tries)
    increase_nxyz(nxyz,3,0); dxyz = lxyz/nxyz
    adjust_nondim_lxyz(dxyz,lxyz,3); dxyz = lxyz/nxyz

  # Plot the PFLOTRAN and analytical solutions
  z_levels = np.array([0,math.floor((nx-1)/3),math.floor((nx-1)/1.5),(nx-1)])
  z_levels = z_levels.astype(int)

  plot_3D_steady(path,x_soln,y_soln,z_levels,T_soln,T_pflotran,'Distance [m]',
                 'Temperature [C]',"{0:.2f}".format(max_percent_error))
  
  # Plot error analysis
  plot_error(error_analysis,nxyz_record,dxyz_record,path,try_count,3)
  
  # Add test result to report card
  add_to_report(path,test_pass,max_percent_error,ierr)

  return;
################################################################################


################################################################################
#### TRANSIENT THERMAL TESTS ###################################################
################################################################################

#==============================================================================#
def thermal_transient_1D_BC1stkind(path,input_prefix,remove,screen_on,pf_exe,
				                             mpi_exe,num_tries):
#==============================================================================#
# Based on:
# Kolditz, et al. (2015) Thermo-Hydro-Mechanical-Chemical Processes in
# Fractured Porous Media: Modelling and Benchmarking, Closed Form Solutions,
# Springer International Publishing, Switzerland.
# Section 2.1.6, pg.19
# "A Transient 1D Temperature Distribution, Time-Dependent Boundary Conditions
# of 1st Kind"
# With some modification of the parameter values given in Kolditz (2015)
#
# Author: Jenn Frederick
# Date: 06/30/2016
# ******************************************************************************
  nxyz = np.zeros(3) + 1
  dxyz = np.zeros(3) + 1.
  lxyz = np.zeros(3) + 1.
  error_analysis = np.zeros(num_tries)
  dxyz_record = np.zeros((num_tries,3))
  nxyz_record = np.zeros((num_tries,3))
  test_pass = False
  try_count = 0

  # initial discretization values
  lxyz[0] = 20.              # [m] lx
  nxyz[0] = 4                # [m] nx  NOTE: Must be even number
  dxyz = lxyz/nxyz           # [m]
  adjust_nondim_lxyz(dxyz,lxyz,1); dxyz = lxyz/nxyz
  K = 2.0                    # [W/m-C]
  Cp = 1.5                   # [J/kg-C]
  rho = 2500.                # [kg/m^3]
  Tb_day = 2.0               # [C/day]
  Tb_sec = 2.0/(24.0*3600.0) # [C/sec]
  chi = K/(Cp*rho)
  ierr = 0
  
  while (not test_pass) and (try_count < num_tries): 
    print_discretization(lxyz,nxyz,dxyz)
    nx = int(nxyz[0]); ny = int(nxyz[1]); nz = int(nxyz[2])
    dx = dxyz[0]; dy = dxyz[1]; dz = dxyz[2]
    Lx = lxyz[0]; Ly = lxyz[1]; Lz = lxyz[2]
    try_count = try_count + 1

    x_soln1 = np.linspace(0.+(dx/2.),(Lx/2)-(dx/2.),nx/2.)        # [m]
    x_soln2 = np.linspace(-((Lx/2)-(dx/2.)),-(0.+(dx/2.)),nx/2.)  # [m]
    x_soln = np.concatenate((x_soln2,x_soln1),axis=0)             # [m]
    # Add boundary values to analytical solution
    x_soln = np.concatenate(([-Lx/2],x_soln),axis=0)            # [m]
    x_soln = np.concatenate((x_soln,[Lx/2]),axis=0)             # [m]
    t_soln = np.array([0.0,0.25,0.50,0.75])                     # [day]
    T_soln = np.zeros((4,(nx+2)))                               # [C]
    x_pflotran1 = np.linspace(0.+(dx/2.),(Lx/2)-(dx/2.),nx/2.)        # [m]
    x_pflotran2 = np.linspace(-((Lx/2)-(dx/2.)),-(0.+(dx/2.)),nx/2.)  # [m]
    x_pflotran = np.concatenate((x_pflotran2,x_pflotran1),axis=0)     # [m]
    T_pflotran = np.zeros((4,nx))                                     # [C]

    # create the analytical solution
    for time in range(4):
      t = t_soln[time]*(24.0*3600.0)   # [sec]
      T_soln[time,:] = Tb_sec*t + ((Tb_sec*(pow(x_soln,2)-pow(Lx/2,2)))/(2.*chi))
      sum_term = np.zeros(nx+2)
      sum_term_old = np.zeros(nx+2)
      n = 0
      epsilon = 1.0
      while epsilon > epsilon_value:
        sum_term_old = sum_term
        sum_term = sum_term_old + (((pow(-1.,n))/(pow(((2*n)+1),3)))*np.cos((math.pi*x_soln*((2*n)+1))/(2*Lx/2))*np.exp(-chi*pow((2*n)+1,2)*pow(math.pi,2)*(t/(4*pow(Lx/2,2)))))
        epsilon = np.max(np.abs(sum_term_old-sum_term))
        n = n + 1
      T_soln[time,:] = T_soln[time,:] + ((16.*Tb_sec*pow(Lx/2,2))/(chi*pow(math.pi,3)))*sum_term

    # run PFLOTRAN simulation
    run_pflotran(input_prefix,nxyz,dxyz,lxyz,remove,screen_on,pf_exe,mpi_exe)
    
    # load data from HDF5
    index_string = 'Time:  0.00000E+00 d/Temperature [C]'
    T_pflotran[0,:] = read_pflotran_output_1D(path+
		      '/1D_transient_thermal_BC_1st_kind.h5',index_string,False)
    index_string = 'Time:  2.50000E-01 d/Temperature [C]'
    T_pflotran[1,:] = read_pflotran_output_1D(path+
		      '/1D_transient_thermal_BC_1st_kind.h5',index_string,False)
    index_string = 'Time:  5.00000E-01 d/Temperature [C]'
    T_pflotran[2,:] = read_pflotran_output_1D(path+
		      '/1D_transient_thermal_BC_1st_kind.h5',index_string,False)
    index_string = 'Time:  7.50000E-01 d/Temperature [C]'
    T_pflotran[3,:] = read_pflotran_output_1D(path+
		      '/1D_transient_thermal_BC_1st_kind.h5',index_string,remove)
    ierr = check(T_pflotran)

    max_percent_error = calc_relative_error(T_soln[:,1:nx+1],T_pflotran,ierr)
    record_error(error_analysis,nxyz_record,dxyz_record,max_percent_error,
                 nxyz,dxyz,try_count)
    test_pass = does_pass(max_percent_error,try_count,num_tries)
    increase_nxyz(nxyz,1,1); dxyz = lxyz/nxyz
    adjust_nondim_lxyz(dxyz,lxyz,1); dxyz = lxyz/nxyz

  # Plot the PFLOTRAN and analytical solutions
  plot_1D_transient(path,t_soln,'day',x_soln,T_soln,x_pflotran,T_pflotran,
                    'Distance [m]','Temperature [C]',"{0:.2f}".format(max_percent_error))
  
  # Plot error analysis
  plot_error(error_analysis,nxyz_record,dxyz_record,path,try_count,1)
  
  # Add test result to report card
  add_to_report(path,test_pass,max_percent_error,ierr)

  return;
################################################################################


#==============================================================================#
def thermal_transient_1D_BC2ndkind(path,input_prefix,remove,screen_on,pf_exe,
				                             mpi_exe,num_tries):
#==============================================================================#
# Based on:
# Kolditz, et al. (2015) Thermo-Hydro-Mechanical-Chemical Processes in
# Fractured Porous Media: Modelling and Benchmarking, Closed Form Solutions,
# Springer International Publishing, Switzerland.
# Section 2.1.7, pg.20
# "A Transient 1D Temperature Distribution, Time-Dependent Boundary Conditions
# of 2nd Kind"
# With some modification of the parameter values given in Kolditz (2015)
#
# Author: Jenn Frederick
# Date: 07/12/2016
# ******************************************************************************
  nxyz = np.zeros(3) + 1
  dxyz = np.zeros(3) + 1.
  lxyz = np.zeros(3) + 1.
  error_analysis = np.zeros(num_tries)
  dxyz_record = np.zeros((num_tries,3))
  nxyz_record = np.zeros((num_tries,3))
  test_pass = False
  try_count = 0

  # initial discretization values
  lxyz[0] = 25.        # [m] lx 
  nxyz[0] = 5          # [m] nx
  dxyz = lxyz/nxyz     # [m]
  adjust_nondim_lxyz(dxyz,lxyz,1); dxyz = lxyz/nxyz
  K = 1.16             # [W/m-C]
  Cp = 0.01            # [J/kg-C]
  rho = 2000.          # [kg/m^3]
  q = 0.385802         # [C/day]
  chi = K/(Cp*rho)
  ierr = 0
  
  while (not test_pass) and (try_count < num_tries): 
    print_discretization(lxyz,nxyz,dxyz)
    nx = int(nxyz[0]); ny = int(nxyz[1]); nz = int(nxyz[2])
    dx = dxyz[0]; dy = dxyz[1]; dz = dxyz[2]
    Lx = lxyz[0]; Ly = lxyz[1]; Lz = lxyz[2]
    try_count = try_count + 1

    x_soln = np.linspace(0.+(dx/2.),Lx-(dx/2.),nx)  # [m]
    x_soln = np.concatenate((x_soln,[Lx]),axis=0)   # [m]
    T_soln = np.zeros((4,nx+1))                     # [C]
    x_pflotran = x_soln[0:nx]                       # [m]
    T_pflotran = np.zeros((4,nx))                   # [C]
    t_soln = np.array([0.01,0.04,0.09,0.12])        # [day]

    # create the analytical solution
    p1 = np.zeros(nx+1)
    p2 = np.zeros(nx+1)
    i3erfc1 = np.zeros(nx+1)
    i3erfc2 = np.zeros(nx+1)
    for time in range(4):
      t = t_soln[time]*24.0*3600.0  # [sec]
      sum_term = np.zeros(nx+1)
      sum_term_old = np.zeros(nx+1)
      n = 0
      epsilon = 1.0
      while epsilon > epsilon_value:
        p1 = ((((2*n)+1)*Lx)-x_soln)/(2.*math.sqrt(chi*t))
        i3erfc1 = (1./12.)*np.sqrt(math.pi)*p1**3*erf(1.0*p1) - (1./12.)*np.sqrt(math.pi)*p1**3 + (1./3.)*p1**2*np.exp(-1.0*p1**2) + 0.5*p1*(-0.5*p1*np.exp(-1.0*p1**2) + 0.25*np.sqrt(math.pi)*erf(1.0*p1)) - 0.125*np.sqrt(math.pi)*p1 + (1./12.)*np.exp(-1.0*p1**2)
        p2 = ((((2*n)+1)*Lx)+x_soln)/(2.*math.sqrt(chi*t))
        i3erfc2 = (1./12.)*np.sqrt(math.pi)*p2**3*erf(1.0*p2) - (1./12.)*np.sqrt(math.pi)*p2**3 + (1./3.)*p2**2*np.exp(-1.0*p2**2) + 0.5*p2*(-0.5*p2*np.exp(-1.0*p2**2) + 0.25*np.sqrt(math.pi)*erf(1.0*p2)) - 0.125*np.sqrt(math.pi)*p2 + (1./12.)*np.exp(-1.0*p2**2)
        sum_term_old = sum_term
        sum_term = sum_term_old + (2./math.sqrt(math.pi))*i3erfc1 + (2./math.sqrt(math.pi))*i3erfc2
        epsilon = np.max(np.abs(sum_term_old-sum_term))
        n = n + 1
      T_soln[time,:] = ((8*q*(1./(24.*3600.))*math.sqrt(chi*pow(t,3)))/K)*sum_term

    # To calculate the definite integral, use sympy:
    # p1, s = sym.symbols('p1 s')
    # i3erfc1 = sym.integrate(((pow((s-p1),3))/(3.*2.*1.))*sym.exp(-1.*pow(s,2)),(s,p1,sym.oo))
    # p2, s = sym.symbols('p2 s')
    # i3erfc2 = sym.integrate(((pow((s-p2),3))/(3.*2.*1.))*sym.exp(-1.*pow(s,2)),(s,p2,sym.oo))

    # run PFLOTRAN simulation
    run_pflotran(input_prefix,nxyz,dxyz,lxyz,remove,screen_on,pf_exe,mpi_exe)

    index_string = 'Time:  1.00000E-02 d/Temperature [C]'
    T_pflotran[0,:] = read_pflotran_output_1D(path+
                     '/1D_transient_thermal_BC_2nd_kind.h5',index_string,False)
    index_string = 'Time:  4.00000E-02 d/Temperature [C]'
    T_pflotran[1,:] = read_pflotran_output_1D(path+
                     '/1D_transient_thermal_BC_2nd_kind.h5',index_string,False)
    index_string = 'Time:  9.00000E-02 d/Temperature [C]'
    T_pflotran[2,:] = read_pflotran_output_1D(path+
                     '/1D_transient_thermal_BC_2nd_kind.h5',index_string,False)
    index_string = 'Time:  1.20000E-01 d/Temperature [C]'
    T_pflotran[3,:] = read_pflotran_output_1D(path+
                     '/1D_transient_thermal_BC_2nd_kind.h5',index_string,remove)
    ierr = check(T_pflotran)

    max_percent_error = calc_relative_error(T_soln[:,0:nx],T_pflotran,ierr)
    record_error(error_analysis,nxyz_record,dxyz_record,max_percent_error,
                 nxyz,dxyz,try_count)
    test_pass = does_pass(max_percent_error,try_count,num_tries)
    increase_nxyz(nxyz,1,0); dxyz = lxyz/nxyz
    adjust_nondim_lxyz(dxyz,lxyz,1); dxyz = lxyz/nxyz

  # Plot the PFLOTRAN and analytical solutions
  plot_1D_transient(path,t_soln,'day',x_soln,T_soln,x_pflotran,T_pflotran,
                    'Distance [m]','Temperature [C]',"{0:.2f}".format(max_percent_error))
  
  # Plot error analysis
  plot_error(error_analysis,nxyz_record,dxyz_record,path,try_count,1)
  
  # Add test result to report card
  add_to_report(path,test_pass,max_percent_error,ierr)

  return;
################################################################################


#==============================================================================#
def thermal_transient_1D_BC1st2ndkind(path,input_prefix,remove,screen_on,pf_exe,
				                             mpi_exe,num_tries):
#==============================================================================#
# Based on:
# Kolditz, et al. (2015) Thermo-Hydro-Mechanical-Chemical Processes in
# Fractured Porous Media: Modelling and Benchmarking, Closed Form Solutions,
# Springer International Publishing, Switzerland.
# Section 2.1.8, pg.21
# "A Transient 1D Temperature Distribution, Non-Zero Initial Temperature,
# Boundary Conditions of 1st and 2nd Kind"
# With some modification of the parameter values given in Kolditz (2015)
#
# Author: Jenn Frederick
# Date: 07/13/2016
# ******************************************************************************
  nxyz = np.zeros(3) + 1
  dxyz = np.zeros(3) + 1.
  lxyz = np.zeros(3) + 1.
  error_analysis = np.zeros(num_tries)
  dxyz_record = np.zeros((num_tries,3))
  nxyz_record = np.zeros((num_tries,3))
  test_pass = False
  try_count = 0

  # initial discretization values
  lxyz[0] = 100.       # [m] lx 
  nxyz[0] = 10         # [m] nx
  dxyz = lxyz/nxyz     # [m]
  adjust_nondim_lxyz(dxyz,lxyz,1); dxyz = lxyz/nxyz
  K = 0.5787037  # [W/m-C]
  Cp = 0.01      # [J/kg-C]
  rho = 2000.    # [kg/m^3]
  chi = K/(Cp*rho)
  ierr = 0
  
  while (not test_pass) and (try_count < num_tries): 
    print_discretization(lxyz,nxyz,dxyz)
    nx = int(nxyz[0]); ny = int(nxyz[1]); nz = int(nxyz[2])
    dx = dxyz[0]; dy = dxyz[1]; dz = dxyz[2]
    Lx = lxyz[0]; Ly = lxyz[1]; Lz = lxyz[2]
    try_count = try_count + 1

    x_soln = np.linspace(0.+(dx/2.),Lx-(dx/2.),nx)      # [m]
    x_pflotran = x_soln                                 # [m]
    t_soln = np.array([0.015,0.05,0.10,0.50])           # [day]
    T_soln = np.zeros((4,nx))                           # [C]
    T_pflotran = np.zeros((4,nx))                       # [C]

    # create the analytical solution
    for time in range(4):
      t = t_soln[time]*24.0*3600.0  # [sec]
      sum_term_old = np.zeros(nx)
      sum_term = np.zeros(nx)
      n = 1
      epsilon = 1.0
      while epsilon > epsilon_value:
        sum_term_old = sum_term
        sum_term = sum_term_old + (np.cos(n*math.pi*x_soln/Lx)*np.exp(-chi*pow(n,2)*pow(math.pi,2)*t/pow(Lx,2))*(80./(3.*pow((n*math.pi),2)))*np.cos(n*math.pi/2.)*np.sin(n*math.pi/4.)*np.sin(3.*n*math.pi/20.))
        epsilon = np.max(np.abs(sum_term_old-sum_term))
        n = n + 1
      T_soln[time,:] = 0.50 + sum_term
      
    # run PFLOTRAN simulation
    run_pflotran(input_prefix,nxyz,dxyz,lxyz,remove,screen_on,pf_exe,mpi_exe)

    index_string = 'Time:  1.50000E-02 d/Temperature [C]'
    T_pflotran[0,:] = read_pflotran_output_1D(path+
                 '/1D_transient_thermal_BC_1st_2nd_kind.h5',index_string,False)
    index_string = 'Time:  5.00000E-02 d/Temperature [C]'
    T_pflotran[1,:] = read_pflotran_output_1D(path+
                 '/1D_transient_thermal_BC_1st_2nd_kind.h5',index_string,False)
    index_string = 'Time:  1.00000E-01 d/Temperature [C]'
    T_pflotran[2,:] = read_pflotran_output_1D(path+
                 '/1D_transient_thermal_BC_1st_2nd_kind.h5',index_string,False)
    index_string = 'Time:  5.00000E-01 d/Temperature [C]'
    T_pflotran[3,:] = read_pflotran_output_1D(path+
                 '/1D_transient_thermal_BC_1st_2nd_kind.h5',index_string,remove)
    ierr = check(T_pflotran)

    max_percent_error = calc_relative_error(T_soln,T_pflotran,ierr)
    record_error(error_analysis,nxyz_record,dxyz_record,max_percent_error,
                 nxyz,dxyz,try_count)
    test_pass = does_pass(max_percent_error,try_count,num_tries)
    increase_nxyz(nxyz,1,0); dxyz = lxyz/nxyz
    adjust_nondim_lxyz(dxyz,lxyz,1); dxyz = lxyz/nxyz

  # Plot the PFLOTRAN and analytical solutions
  plot_1D_transient(path,t_soln,'day',x_soln,T_soln,x_pflotran,T_pflotran,
                    'Distance [m]','Temperature [C]',"{0:.2f}".format(max_percent_error))

  # Plot error analysis
  plot_error(error_analysis,nxyz_record,dxyz_record,path,try_count,1)
  
  # Add test result to report card
  add_to_report(path,test_pass,max_percent_error,ierr)

  return;
################################################################################


#==============================================================================#
def thermal_transient_2D_BC1st2ndkind(path,input_prefix,remove,screen_on,pf_exe,
				                             mpi_exe,num_tries):
#==============================================================================#
# Based on:
# Kolditz, et al. (2015) Thermo-Hydro-Mechanical-Chemical Processes in
# Fractured Porous Media: Modelling and Benchmarking, Closed Form Solutions,
# Springer International Publishing, Switzerland.
# Section 2.1.9, pg.23
# "A Transient 2D Temperature Distribution, Non-Zero Initial Temperature,
# Boundary Conditions of 1st and 2nd Kind"
#
# Author: Jenn Frederick
# Date: 07/13/2016
# ******************************************************************************
  nxyz = np.zeros(3) + 1
  dxyz = np.zeros(3) + 1.
  lxyz = np.zeros(3) + 1.
  error_analysis = np.zeros(num_tries)
  dxyz_record = np.zeros((num_tries,3))
  nxyz_record = np.zeros((num_tries,3))
  test_pass = False
  try_count = 0

  # initial discretization values
  lxyz[0] = 100.       # [m] lx 
  lxyz[1] = 100.       # [m] ly 
  nxyz[0] = 10         # [m] nx
  nxyz[1] = 10         # [m] ny
  dxyz = lxyz/nxyz     # [m]
  adjust_nondim_lxyz(dxyz,lxyz,2); dxyz = lxyz/nxyz 
  T0 = 1.        # [C]
  T_offset = 0.1 # [C]
  K = 0.5787037  # [W/m-C]
  rho = 2000.    # [kg/m^3]
  Cp = 0.01      # [J/kg-C]
  chi = K/(rho*Cp)
  ierr = 0
  
  while (not test_pass) and (try_count < num_tries): 
    print_discretization(lxyz,nxyz,dxyz)
    nx = int(nxyz[0]); ny = int(nxyz[1]); nz = int(nxyz[2])
    dx = dxyz[0]; dy = dxyz[1]; dz = dxyz[2]
    Lx = lxyz[0]; Ly = lxyz[1]; Lz = lxyz[2]
    try_count = try_count + 1

    T_soln = np.zeros((4,ny,nx))                     # [C]
    T_pflotran = np.zeros((4,ny,nx))                 # [C]
    t_soln = np.array([0.00,0.04,0.06,0.10])         # [day]
    x_soln = np.linspace(0.+(dx/2.),Lx-(dx/2.),nx)   # [m]
    y_soln = np.linspace(0.+(dy/2.),Ly-(dy/2.),ny)   # [m]
    T1x = np.zeros(int(nx))
    T2y = np.zeros(int(ny))

    # for time=0, use the actual initial condition as the analytical
    # solution, not the analytical solution with t=0:
    fx = np.zeros(int(nx))
    for i in range(int(nx)):
      x = x_soln[i]
      if (0. <= x < (Lx/10.)):
        fx[i] = 0.
      if ((Lx/10.) <= x < (4.*Lx/10.)):
        fx[i] = (10./(3.*Lx))*(x) - (1./3.)
      if ((4.*Lx/10.) <= x < (6.*Lx/10.)):
        fx[i] = 1.
      if ((6.*Lx/10.) <= x < (9.*Lx/10.)):
        fx[i] = 3. - (10./(3.*Lx))*(x)
      if ((9.*Lx/10.) <= x < Lx):
        fx[i] = 0.

    fy = np.zeros(int(ny))
    for j in range(int(ny)):
      y = y_soln[j]
      if (0. <= y < (Ly/10.)):
        fy[j] = 0.
      if ((Ly/10.) <= y < (4.*Ly/10.)):
        fy[j] = (10./(3.*Ly))*(y) - (1./3.)
      if ((4.*Ly/10.) <= y < (6.*Ly/10.)):
        fy[j] = 1.
      if ((6.*Ly/10.) <= y < (9.*Ly/10.)):
        fy[j] = 3. - (10./(3.*Ly))*(y)
      if ((9.*Ly/10.) <= y < Ly):
        fy[j] = 0.

    T_soln_t0 = np.zeros((nx,ny))
    for i in range(int(nx)):
      for j in range(int(ny)):
        T_soln_t0[i][j] = T0*fx[i]*fy[j] + T_offset

    # create the analytical solution
    for time in range(4):
      t = t_soln[time]*24.0*3600.0  # [sec]
      # create T1y
      sum_term_y = np.zeros(int(ny))
      sum_term_old_y = np.zeros(int(ny))
      n = 1
      epsilon = 1
      while epsilon > epsilon_value:
        sum_term_old_y = sum_term_y
        sum_term_y = sum_term_old_y + (np.cos(n*math.pi*y_soln/Ly)*np.exp(-chi*pow(n,2)*pow(math.pi,2)*t/pow(Ly,2))*(80./(3.*pow((n*math.pi),2)))*np.cos(n*math.pi/2.)*np.sin(n*math.pi/4.)*np.sin(3.*n*math.pi/20.))
        epsilon = np.max(np.abs(sum_term_old_y-sum_term_y))
        n = n + 1
      T2y = 0.5 + sum_term_y
      # create T1x
      sum_term_x = np.zeros(int(nx))
      sum_term_old_x = np.zeros(int(nx))
      n = 1
      epsilon = 1
      while epsilon > epsilon_value:
        sum_term_old_x = sum_term_x
        sum_term_x = sum_term_old_x + (np.sin(n*math.pi*x_soln/Lx)*np.exp(-chi*pow(n,2)*pow(math.pi,2)*t/pow(Lx,2))*(80./(3.*pow((n*math.pi),2)))*np.sin(n*math.pi/2.)*np.sin(n*math.pi/4.)*np.sin(3.*n*math.pi/20.))
        epsilon = np.max(np.abs(sum_term_old_x-sum_term_x))
        n = n + 1
      T1x = sum_term_x
      for i in range(int(nx)):
        for j in range(int(ny)):
          T_soln[time,i,j] = T0*T1x[i]*T2y[j] + T_offset

    T_soln[0,:,:] = T_soln_t0[:,:]
    
    # run PFLOTRAN simulation
    run_pflotran(input_prefix,nxyz,dxyz,lxyz,remove,screen_on,pf_exe,mpi_exe)

    index_string = 'Time:  0.00000E+00 d/Temperature [C]'
    T_pflotran[0,:,:] = read_pflotran_output_2D(path+
                 '/2D_transient_thermal_BC_1st_2nd_kind.h5',index_string,False)
    index_string = 'Time:  4.00000E-02 d/Temperature [C]'
    T_pflotran[1,:,:] = read_pflotran_output_2D(path+
                 '/2D_transient_thermal_BC_1st_2nd_kind.h5',index_string,False)
    index_string = 'Time:  6.00000E-02 d/Temperature [C]'
    T_pflotran[2,:,:] = read_pflotran_output_2D(path+
                 '/2D_transient_thermal_BC_1st_2nd_kind.h5',index_string,False)
    index_string = 'Time:  1.00000E-01 d/Temperature [C]'
    T_pflotran[3,:,:] = read_pflotran_output_2D(path+
                 '/2D_transient_thermal_BC_1st_2nd_kind.h5',index_string,remove)
    ierr = check(T_pflotran)

    max_percent_error = calc_relative_error(T_soln,T_pflotran,ierr)
    record_error(error_analysis,nxyz_record,dxyz_record,max_percent_error,
                 nxyz,dxyz,try_count)
    test_pass = does_pass(max_percent_error,try_count,num_tries)
    increase_nxyz(nxyz,2,0); dxyz = lxyz/nxyz
    adjust_nondim_lxyz(dxyz,lxyz,2); dxyz = lxyz/nxyz

  # Plot the PFLOTRAN and analytical solution:
  loaded = 0
  plot_2D_transient(path,t_soln,'day',x_soln,y_soln,T_soln,T_pflotran,
                    'Distance [m]','Temperature [C]',"{0:.2f}".format(max_percent_error),
                    loaded)
  
  # Plot error analysis
  plot_error(error_analysis,nxyz_record,dxyz_record,path,try_count,2)
  
  # Add test result to report card
  add_to_report(path,test_pass,max_percent_error,ierr)

  return;
################################################################################


################################################################################
#### STEADY FLOW TESTS #########################################################
################################################################################


#==============================================================================#
def flow_steady_1D_BC1stkind(path,input_prefix,remove,screen_on,pf_exe,
				                             mpi_exe,num_tries):
#==============================================================================#
#
# Kolditz, et al. (2015) Thermo-Hydro-Mechanical-Chemical Processes in
# Fractured Porous Media: Modelling and Benchmarking, Closed Form Solutions,
# Springer International Publishing, Switzerland.
# Section 2.2.1, pg.27
# "A 1D Steady-State Pressure Distribution, Boundary Conditions of 1st Kind"
#
# Author: Jenn Frederick
# Date: 07/21/2016
#*******************************************************************************
  nxyz = np.zeros(3) + 1
  dxyz = np.zeros(3) + 1.
  lxyz = np.zeros(3) + 1.
  error_analysis = np.zeros(num_tries)
  dxyz_record = np.zeros((num_tries,3))
  nxyz_record = np.zeros((num_tries,3))
  test_pass = False
  try_count = 0

  # initial discretization values
  lxyz[0] = 100.       # [m] lx 
  nxyz[0] = 10         # [m] nx
  dxyz = lxyz/nxyz     # [m]
  adjust_nondim_lxyz(dxyz,lxyz,1); dxyz = lxyz/nxyz
  P0 = 2.              # [C]
  ierr = 0
  
  while (not test_pass) and (try_count < num_tries): 
    print_discretization(lxyz,nxyz,dxyz)
    nx = int(nxyz[0]); ny = int(nxyz[1]); nz = int(nxyz[2])
    dx = dxyz[0]; dy = dxyz[1]; dz = dxyz[2]
    Lx = lxyz[0]; Ly = lxyz[1]; Lz = lxyz[2]
    try_count = try_count + 1

    x_soln = np.linspace(0.+(dx/2.),Lx-(dx/2.),nx)    # [m]
    x_pflotran = x_soln                               # [m]
    x_soln = np.concatenate(([0.],x_soln),axis=0)     # [m]
    x_soln = np.concatenate((x_soln,[Lx]),axis=0)     # [m]
    p_soln = np.zeros(nx+2)                           # [MPa]
    p_pflotran = np.zeros(nx)                         # [Pa]

    # create the analytical solution
    p_soln = np.array(-x_soln/Lx + P0)                # [MPa]
    
    # run PFLOTRAN simulation
    run_pflotran(input_prefix,nxyz,dxyz,lxyz,remove,screen_on,pf_exe,mpi_exe)

    # load data from hdf5
    index_string = 'Time:  1.00000E+01 d/Liquid_Pressure [Pa]'
    p_pflotran[:] = read_pflotran_output_1D(path+
		    '/1D_steady_pressure_BC_1st_kind.h5',index_string,remove)
    ierr = check(p_pflotran)

    # convert pressure units [Pa -> MPa]
    p_pflotran = p_pflotran/1.0e6         # [MPa]

    max_percent_error = calc_relative_error(p_soln[1:nx+1],p_pflotran,ierr)
    record_error(error_analysis,nxyz_record,dxyz_record,max_percent_error,
                 nxyz,dxyz,try_count)
    test_pass = does_pass(max_percent_error,try_count,num_tries)
    increase_nxyz(nxyz,1,0); dxyz = lxyz/nxyz
    adjust_nondim_lxyz(dxyz,lxyz,1); dxyz = lxyz/nxyz

  # Plot the PFLOTRAN and analytical solutions
  plot_1D_steady(path,x_soln,p_soln,x_pflotran,p_pflotran,'Distance [m]',
                 'Pressure [MPa]',"{0:.2f}".format(max_percent_error))
  
  # Plot error analysis
  plot_error(error_analysis,nxyz_record,dxyz_record,path,try_count,1)
  
  # Add test result to report card
  add_to_report(path,test_pass,max_percent_error,ierr)

  return;
################################################################################


#==============================================================================#
def flow_steady_1D_BC1st2ndkind(path,input_prefix,remove,screen_on,pf_exe,
				                             mpi_exe,num_tries):
#==============================================================================#
# Based on:
# Kolditz, et al. (2015) Thermo-Hydro-Mechanical-Chemical Processes in
# Fractured Porous Media: Modelling and Benchmarking, Closed Form Solutions,
# Springer International Publishing, Switzerland.
# Section 2.2.2, pg.27
# "A 1D Steady-State Pressure Distribution, Boundary Conditions of 1st and
# 2nd Kind"
#
# Author: Jenn Frederick
# Date: 07/21/2016
# *****************************************************************************
  nxyz = np.zeros(3) + 1
  dxyz = np.zeros(3) + 1.
  lxyz = np.zeros(3) + 1.
  error_analysis = np.zeros(num_tries)
  dxyz_record = np.zeros((num_tries,3))
  nxyz_record = np.zeros((num_tries,3))
  test_pass = False
  try_count = 0

  # initial discretization values
  lxyz[0] = 100.       # [m] lx 
  nxyz[0] = 5          # [m] nx
  dxyz = lxyz/nxyz     # [m]
  adjust_nondim_lxyz(dxyz,lxyz,1); dxyz = lxyz/nxyz
  K1 = 1.e-12          # [m^2]
  K2 = 3.e-12          # [m^2]
  mu = 1.e-3           # [Pa-s]
  q = -1.5e-5          # [m/s]
  p_at0 = 1.e6         # [Pa]
  ierr = 0
  
  while (not test_pass) and (try_count < num_tries): 
    print_discretization(lxyz,nxyz,dxyz)
    nx = int(nxyz[0]); ny = int(nxyz[1]); nz = int(nxyz[2])
    dx = dxyz[0]; dy = dxyz[1]; dz = dxyz[2]
    Lx = lxyz[0]; Ly = lxyz[1]; Lz = lxyz[2]
    try_count = try_count + 1

    x_soln = np.linspace(0.+(dx/2.),Lx-(dx/2.),nx)    # [m]
    x_pflotran = x_soln                               # [m]
    x_soln = np.concatenate(([0.],x_soln),axis=0)     # [m]
    p_soln = np.zeros(nx+1)                           # [Pa]
    p_pflotran = np.zeros(nx)                         # [Pa]

    # create the analytical solution
    k = -1
    for x in x_soln:
      k = k + 1
      if x <= (2.*Lx/5.):
        p_soln[k] = np.array( -(((q*mu)/K1)*x) + p_at0 )
      else:
        p_soln[k] = np.array( -(((q*mu)/K2)*x) + p_at0 + (q*mu*(2.*Lx/5.)*((1/K2)-(1/K1))) )

    # run PFLOTRAN simulation
    run_pflotran(input_prefix,nxyz,dxyz,lxyz,remove,screen_on,pf_exe,mpi_exe)

    # load data from hdf5
    index_string = 'Time:  1.00000E+01 d/Liquid_Pressure [Pa]'
    p_pflotran[:] = read_pflotran_output_1D(path+
		    '/1D_steady_pressure_BC_1st_2nd_kind.h5',index_string,remove)
    ierr = check(p_pflotran)

    # Convert pressure units [Pa -> MPa]
    p_pflotran = p_pflotran/1.e6   # [MPa]
    p_soln = p_soln/1.e6           # [MPa]

    max_percent_error = calc_relative_error(p_soln[1:nx+1],p_pflotran,ierr)
    record_error(error_analysis,nxyz_record,dxyz_record,max_percent_error,
                 nxyz,dxyz,try_count)
    test_pass = does_pass(max_percent_error,try_count,num_tries)
    increase_nxyz(nxyz,1,0); dxyz = lxyz/nxyz
    adjust_nondim_lxyz(dxyz,lxyz,1); dxyz = lxyz/nxyz

  # Plot the PFLOTRAN and analytical solutions
  plot_1D_steady(path,x_soln,p_soln,x_pflotran,p_pflotran,'Distance [m]',
                 'Pressure [MPa]',"{0:.2f}".format(max_percent_error))
  
  # Plot error analysis
  plot_error(error_analysis,nxyz_record,dxyz_record,path,try_count,1)
  
  # Add test result to report card
  add_to_report(path,test_pass,max_percent_error,ierr)

  return;
################################################################################


#==============================================================================#
def flow_steady_1D_hydrostatic(path,input_prefix,remove,screen_on,pf_exe,
				                             mpi_exe,num_tries):
#==============================================================================#
# Based on:
# Kolditz, et al. (2015) Thermo-Hydro-Mechanical-Chemical Processes in
# Fractured Porous Media: Modelling and Benchmarking, Closed Form Solutions,
# Springer International Publishing, Switzerland.
# Section 2.2.6, pg.32
# "A Hydrostatic Pressure Distribution"
#
# Author: Jenn Frederick
# Date: 08/23/2016
# *****************************************************************************
  nxyz = np.zeros(3) + 1
  dxyz = np.zeros(3) + 1.
  lxyz = np.zeros(3) + 1.
  error_analysis = np.zeros(num_tries)
  dxyz_record = np.zeros((num_tries,3))
  nxyz_record = np.zeros((num_tries,3))
  test_pass = False
  try_count = 0

  # initial discretization values
  lxyz[0] = 100.       # [m] lz 
  nxyz[0] = 10         # [m] nz
  dxyz = lxyz/nxyz     # [m]
  adjust_nondim_lxyz(dxyz,lxyz,1); dxyz = lxyz/nxyz
  g = 9.81             # [m/s^2] x-direction in PFLOTRAN
  rho = 1019.0         # [kg/m^3]
  p_offset = 101325.0  # [Pa]
  ierr = 0
  
  while (not test_pass) and (try_count < num_tries): 
    print_discretization(lxyz,nxyz,dxyz)
    nx = int(nxyz[0]); ny = int(nxyz[1]); nz = int(nxyz[2])
    dx = dxyz[0]; dy = dxyz[1]; dz = dxyz[2]
    Lx = lxyz[0]; Ly = lxyz[1]; Lz = lxyz[2]
    try_count = try_count + 1

    x_soln = np.linspace(dx,Lx,nx)    # [m]
    x_pflotran = x_soln               # [m]
    p_soln = np.zeros(nx)             # [Pa]
    p_pflotran = np.zeros(nx)         # [Pa]

    # create the analytical solution
    k = -1
    for z in x_soln:
      k = k + 1
      p_soln[k] = rho*g*(Lx-x_soln[k]) + p_offset
    
    # run PFLOTRAN simulation
    run_pflotran(input_prefix,nxyz,dxyz,lxyz,remove,screen_on,pf_exe,mpi_exe)

    # load data from hdf5
    index_string = 'Time:  1.00000E+00 y/Liquid_Pressure [Pa]'
    p_pflotran[:] = read_pflotran_output_1D(path+
		       '/1D_steady_pressure_hydrostatic.h5',index_string,remove)
    ierr = check(p_pflotran)

    # Convert pressure units [Pa -> MPa]
    p_pflotran = p_pflotran/1.e6   # [MPa]
    p_soln = p_soln/1.e6           # [MPa]

    max_percent_error = calc_relative_error(p_soln,p_pflotran,ierr)
    record_error(error_analysis,nxyz_record,dxyz_record,max_percent_error,
                 nxyz,dxyz,try_count)
    test_pass = does_pass(max_percent_error,try_count,num_tries)
    increase_nxyz(nxyz,1,0); dxyz = lxyz/nxyz
    adjust_nondim_lxyz(dxyz,lxyz,1); dxyz = lxyz/nxyz

  # Plot the PFLOTRAN and analytical solutions
  plot_1D_steady(path,x_soln,p_soln,x_pflotran,p_pflotran,'Distance [m]',
                 'Pressure [MPa]',"{0:.2f}".format(max_percent_error))
  
  # Plot error analysis
  plot_error(error_analysis,nxyz_record,dxyz_record,path,try_count,1)
  
  # Add test result to report card
  add_to_report(path,test_pass,max_percent_error,ierr)

  return;
################################################################################


#==============================================================================#
def flow_steady_2D_BC1stkind(path,input_prefix,remove,screen_on,pf_exe,
				                             mpi_exe,num_tries):
#==============================================================================#
# Based on:
# Kolditz, et al. (2015) Thermo-Hydro-Mechanical-Chemical Processes in 
# Fractured Porous Media: Modelling and Benchmarking, Closed Form Solutions,
# Springer International Publishing, Switzerland.
# Section 2.2.3, pg.29
# "A 2D Steady-State Pressure Distribution, Boundary Conditions of 1st Kind"
#
# Author: Jenn Frederick
# Date: 07/22/2016
# ******************************************************************************
  nxyz = np.zeros(3) + 1
  dxyz = np.zeros(3) + 1.
  lxyz = np.zeros(3) + 1.
  error_analysis = np.zeros(num_tries)
  dxyz_record = np.zeros((num_tries,3))
  nxyz_record = np.zeros((num_tries,3))
  test_pass = False
  try_count = 0

  # initial discretization values
  lxyz[0] = 1.       # [m] lx 
  lxyz[1] = 1.       # [m] ly 
  nxyz[0] = 5        # [m] nx
  nxyz[1] = 5        # [m] ny
  dxyz = lxyz/nxyz   # [m]
  adjust_nondim_lxyz(dxyz,lxyz,2); dxyz = lxyz/nxyz
  p0 = 1.            # [MPa]
  p_offset = 1.0     # [MPa]
  ierr = 0

  while (not test_pass) and (try_count < num_tries): 
    print_discretization(lxyz,nxyz,dxyz)
    nx = int(nxyz[0]); ny = int(nxyz[1]); nz = int(nxyz[2])
    dx = dxyz[0]; dy = dxyz[1]; dz = dxyz[2]
    Lx = lxyz[0]; Ly = lxyz[1]; Lz = lxyz[2]
    try_count = try_count + 1

    p_soln = np.zeros((nx,ny))                      # [MPa]
    p_pflotran = np.zeros((nx,ny))                  # [Pa]
    x_soln = np.linspace(0.+(dx/2.),Lx-(dx/2.),nx)  # [m]
    y_soln = np.linspace(0.+(dy/2.),Ly-(dy/2.),ny)  # [m]

    # create the analytical solution
    i = -1
    for x in x_soln:
      i = i + 1
      j = -1
      for y in y_soln:
	j = j + 1
	p_soln[i,j] = p0*(x/Lx)*(y/Ly) + p_offset    # [MPa]

    # run PFLOTRAN simulation
    run_pflotran(input_prefix,nxyz,dxyz,lxyz,remove,screen_on,pf_exe,mpi_exe)

    # load data from hdf5
    index_string = 'Time:  1.00000E+00 y/Liquid_Pressure [Pa]'
    p_pflotran[:,:] = read_pflotran_output_2D(path+
	              '/2D_steady_pressure_BC_1st_kind.h5',index_string,remove)
    ierr = check(p_pflotran)
  
    # Convert pressure units [Pa -> MPa]
    p_pflotran = p_pflotran/1.e6   # [MPa]

    max_percent_error = calc_relative_error(p_soln,p_pflotran,ierr)
    record_error(error_analysis,nxyz_record,dxyz_record,max_percent_error,
                 nxyz,dxyz,try_count)
    test_pass = does_pass(max_percent_error,try_count,num_tries)
    increase_nxyz(nxyz,2,0); dxyz = lxyz/nxyz
    adjust_nondim_lxyz(dxyz,lxyz,2); dxyz = lxyz/nxyz

  # Plot the PFLOTRAN and analytical solutions
  plot_2D_steady(path,x_soln,y_soln,p_soln,p_pflotran,'Distance [m]',
                 'Pressure [MPa]',"{0:.2f}".format(max_percent_error))
  
  # Plot error analysis
  plot_error(error_analysis,nxyz_record,dxyz_record,path,try_count,2)
  
  # Add test result to report card
  add_to_report(path,test_pass,max_percent_error,ierr)

  return;
################################################################################


#==============================================================================#
def flow_steady_2D_BC1st2ndkind(path,input_prefix,remove,screen_on,pf_exe,
				                             mpi_exe,num_tries):
#==============================================================================#
# Based on:
# Kolditz, et al. (2015) Thermo-Hydro-Mechanical-Chemical Processes in 
# Fractured Porous Media: Modelling and Benchmarking, Closed Form Solutions,
# Springer International Publishing, Switzerland.
# Section 2.2.4, pg.29
# "A 2D Steady-State Pressure Distribution, Boundary Conditions of 1st and
# 2nd Kind"
#
# Author: Jenn Frederick
# Date: 07/25/2017
# ******************************************************************************
  nxyz = np.zeros(3) + 1
  dxyz = np.zeros(3) + 1.
  lxyz = np.zeros(3) + 1.
  error_analysis = np.zeros(num_tries)
  dxyz_record = np.zeros((num_tries,3))
  nxyz_record = np.zeros((num_tries,3))
  test_pass = False
  try_count = 0

  # initial discretization values
  lxyz[0] = 2.       # [m] lx 
  lxyz[1] = 1.       # [m] ly 
  nxyz[0] = 10       # [m] nx
  nxyz[1] = 5        # [m] ny
  dxyz = lxyz/nxyz   # [m]
  adjust_nondim_lxyz(dxyz,lxyz,2); dxyz = lxyz/nxyz
  p0 = 1.            # [MPa]
  p_offset = 1.0     # [MPa]
  ierr = 0

  while (not test_pass) and (try_count < num_tries): 
    print_discretization(lxyz,nxyz,dxyz)
    nx = int(nxyz[0]); ny = int(nxyz[1]); nz = int(nxyz[2])
    dx = dxyz[0]; dy = dxyz[1]; dz = dxyz[2]
    Lx = lxyz[0]; Ly = lxyz[1]; Lz = lxyz[2]
    try_count = try_count + 1

    p_soln = np.zeros((nx,ny))                         # [MPa]
    p_pflotran = np.zeros((nx,ny))                     # [Pa]
    x_soln = np.linspace(0.+(dx/2.),(Lx)-(dx/2.),nx)   # [m]
    y_soln = np.linspace(0.+(dy/2.),Ly-(dy/2.),ny)     # [m]

    # create the analytical solution
    i = -1
    for x in x_soln:
      i = i + 1
      j = -1
      for y in y_soln:
	j = j + 1
	p_soln[i,j] = (p0/Ly)*(x+(2*y)) + p_offset

    # run PFLOTRAN simulation
    run_pflotran(input_prefix,nxyz,dxyz,lxyz,remove,screen_on,pf_exe,mpi_exe)

    # load data from hdf5
    index_string = 'Time:  1.00000E+00 y/Liquid_Pressure [Pa]'
    p_pflotran[:,:] = read_pflotran_output_2D(path+
	           '/2D_steady_pressure_BC_1st_2nd_kind.h5',index_string,remove)
    ierr = check(p_pflotran)
  
    # Convert pressure units [Pa -> MPa]
    p_pflotran = p_pflotran/1.e6   # [MPa]
    
    max_percent_error = calc_relative_error(p_soln,p_pflotran,ierr)
    record_error(error_analysis,nxyz_record,dxyz_record,max_percent_error,
                 nxyz,dxyz,try_count)
    test_pass = does_pass(max_percent_error,try_count,num_tries)
    increase_nxyz(nxyz,2,0); dxyz = lxyz/nxyz
    adjust_nondim_lxyz(dxyz,lxyz,2); dxyz = lxyz/nxyz

  # Plot the PFLOTRAN and analytical solutions
  plot_2D_steady(path,x_soln,y_soln,p_soln,p_pflotran,'Distance [m]',
                 'Pressure [MPa]',"{0:.2f}".format(max_percent_error))
  
  # Plot error analysis
  plot_error(error_analysis,nxyz_record,dxyz_record,path,try_count,2)
  
  # Add test result to report card
  add_to_report(path,test_pass,max_percent_error,ierr)

  return;
################################################################################


#==============================================================================#
def flow_steady_3D_BC1stkind(path,input_prefix,remove,screen_on,pf_exe,
				                             mpi_exe,num_tries):
#==============================================================================#
# Based on:
# Kolditz, et al. (2015) Thermo-Hydro-Mechanical-Chemical Processes in 
# Fractured Porous Media: Modelling and Benchmarking, Closed Form Solutions,
# Springer International Publishing, Switzerland.
# Section 2.2.5, pg.31
# "A 3D Steady-State Pressure Distribution"
#
# Author: Jenn Frederick
# Date: 07/25/2016
# ******************************************************************************
  nxyz = np.zeros(3) + 1
  dxyz = np.zeros(3) + 1.
  lxyz = np.zeros(3) + 1.
  error_analysis = np.zeros(num_tries)
  dxyz_record = np.zeros((num_tries,3))
  nxyz_record = np.zeros((num_tries,3))
  test_pass = False
  try_count = 0
  
  # initial discretization values
  lxyz[0] = 1.      # [m] lx
  lxyz[1] = 1.      # [m] ly
  lxyz[2] = 1.      # [m] lz
  nxyz[0] = 5       # [m] nx 
  nxyz[1] = 5       # [m] ny 
  nxyz[2] = 5       # [m] nz 
  dxyz = lxyz/nxyz  # [m]
  adjust_nondim_lxyz(dxyz,lxyz,3); dxyz = lxyz/nxyz
  p0 = 1.           # [MPa]
  ierr = 0
  
  while (not test_pass) and (try_count < num_tries): 
    print_discretization(lxyz,nxyz,dxyz)
    nx = int(nxyz[0]); ny = int(nxyz[1]); nz = int(nxyz[2])
    dx = dxyz[0]; dy = dxyz[1]; dz = dxyz[2]
    Lx = lxyz[0]; Ly = lxyz[1]; Lz = lxyz[2]
    try_count = try_count + 1

    p_soln = np.zeros((nx,ny,nz))                   # [C]
    p_pflotran = np.zeros((nx,ny,nz))               # [C]
    x_soln = np.linspace(0.+(dx/2.),Lx-(dx/2.),nx)  # [m]
    y_soln = np.linspace(0.+(dy/2.),Ly-(dx/2.),ny)  # [m]
    z_soln = np.linspace(0.+(dz/2.),Lz-(dx/2.),nz)  # [m]

    # create the analytical solution
    i = -1
    for x in x_soln:
      i = i + 1
      j = -1
      for y in y_soln:
	j = j + 1
	k = -1
	for z in z_soln:
	  k = k + 1
	  p_soln[i,j,k] = p0*((x/Lx)+(y/Ly)+(z/Lz))

    # run PFLOTRAN simulation
    run_pflotran(input_prefix,nxyz,dxyz,lxyz,remove,screen_on,pf_exe,mpi_exe)
    
    # load data from hdf5
    index_string = 'Time:  1.00000E+00 y/Liquid_Pressure [Pa]'
    p_pflotran[:,:,:] = read_pflotran_output_3D(path+
	               '/3D_steady_pressure_BC_1st_kind.h5',index_string,remove)
    ierr = check(p_pflotran)
    
    # Convert units [Pa -> MPa]
    p_pflotran = p_pflotran/1.e6

    max_percent_error = calc_relative_error(p_soln,p_pflotran,ierr)
    record_error(error_analysis,nxyz_record,dxyz_record,max_percent_error,
                 nxyz,dxyz,try_count)
    test_pass = does_pass(max_percent_error,try_count,num_tries)
    increase_nxyz(nxyz,3,0); dxyz = lxyz/nxyz
    adjust_nondim_lxyz(dxyz,lxyz,3); dxyz = lxyz/nxyz

  # Plot the PFLOTRAN and analytical solutions
  z_levels = np.array([0,math.floor((nx-1)/3),math.floor((nx-1)/1.5),(nx-1)])
  z_levels = z_levels.astype(int)
  plot_3D_steady(path,x_soln,y_soln,z_levels,p_soln,p_pflotran,'Distance [m]',
                 'Pressure [MPa]',"{0:.2f}".format(max_percent_error))
  
  # Plot error analysis
  plot_error(error_analysis,nxyz_record,dxyz_record,path,try_count,3)
  
  # Add test result to report card
  add_to_report(path,test_pass,max_percent_error,ierr)

  return;
################################################################################


################################################################################
#### TRANSIENT FLOW TESTS ######################################################
################################################################################


#==============================================================================#
def flow_transient_1D_BC1stkind(path,input_prefix,remove,screen_on,pf_exe,
				                             mpi_exe,num_tries):
#==============================================================================#
# Based on:
# Kolditz, et al. (2015) Thermo-Hydro-Mechanical-Chemical Processes in
# Fractured Porous Media: Modelling and Benchmarking, Closed Form Solutions,
# Springer International Publishing, Switzerland.
# Section 2.2.7, pg.32
# "A Transient 1D Pressure Distribution, Time-Dependent Boundary Conditions
# of 1st Kind"
# With some modification of the parameter values given in Kolditz (2015)
#
# Author: Jenn Frederick
# Date: 08/23/2016
# ******************************************************************************
  nxyz = np.zeros(3) + 1
  dxyz = np.zeros(3) + 1.
  lxyz = np.zeros(3) + 1.
  error_analysis = np.zeros(num_tries)
  dxyz_record = np.zeros((num_tries,3))
  nxyz_record = np.zeros((num_tries,3))
  test_pass = False
  try_count = 0
  
  # initial discretization values
  lxyz[0] = 20.                # [m] lx
  nxyz[0] = 6                  # [m] NOTE: nx must be an even number 
  dxyz = lxyz/nxyz             # [m]
  adjust_nondim_lxyz(dxyz,lxyz,1); dxyz = lxyz/nxyz
  k = 1.0e-14                  # [m^2]
  mu = 1.728e-3                # [Pa*sec]
  por = 0.25                   # [-]
  K = 1.0e-8                   # [1/Pa]
  pb_day = 2.0e6               # [Pa/day]
  pb_sec = 2.0e6/(24.0*3600.0) # [Pa/sec]
  p_offset = 0.25e6            # [Pa]
  chi = k/(por*K*mu)
  ierr = 0
  
  while (not test_pass) and (try_count < num_tries): 
    print_discretization(lxyz,nxyz,dxyz)
    nx = int(nxyz[0]); ny = int(nxyz[1]); nz = int(nxyz[2])
    dx = dxyz[0]; dy = dxyz[1]; dz = dxyz[2]
    Lx = lxyz[0]; Ly = lxyz[1]; Lz = lxyz[2]
    try_count = try_count + 1

    x_soln1 = np.linspace(0.+(dx/2.),(Lx/2.)-(dx/2.),nx/2.)        # [m]
    x_soln2 = np.linspace(-((Lx/2.)-(dx/2.)),-(0.+(dx/2.)),nx/2.)  # [m]
    x_soln = np.concatenate((x_soln2,x_soln1),axis=0)              # [m]
    # Add boundary values to analytical solution
    x_soln = np.concatenate(([-(Lx/2.)],x_soln),axis=0)            # [m]
    x_soln = np.concatenate((x_soln,[Lx/2.]),axis=0)               # [m]
    t_soln = np.array([0.10,0.25,0.50,0.75])                       # [day]
    p_soln = np.zeros((4,(nx+2)))                                  # [Pa]
    x_pflotran1 = np.linspace(0.+(dx/2.),(Lx/2.)-(dx/2.),nx/2.)    # [m]
    x_pflotran2 = np.linspace(-((Lx/2.)-(dx/2.)),-(0.+(dx/2.)),nx/2.)  # [m]
    x_pflotran = np.concatenate((x_pflotran2,x_pflotran1),axis=0)  # [m]
    p_pflotran = np.zeros((4,nx))                                  # [Pa]

    # create the analytical solution
    for time in range(4):
      t = t_soln[time]*(24.0*3600.0)   # [sec]
      p_soln[time,:] = pb_sec*t + ((pb_sec*(pow(x_soln,2)-pow((Lx/2.),2)))/(2.*chi))
      sum_term = np.zeros(nx+2)
      sum_term_old = np.zeros(nx+2)
      n = 0
      epsilon = 1.0
      while epsilon > epsilon_value:
	sum_term_old = sum_term
	sum_term = sum_term_old + (((pow(-1.,n))/(pow(((2*n)+1),3)))*np.cos((math.pi*x_soln*((2*n)+1))/(2*(Lx/2.)))*np.exp(-chi*pow((2*n)+1,2)*pow(math.pi,2)*(t/(4*pow((Lx/2.),2)))))
	epsilon = np.max(np.abs(sum_term_old-sum_term))
	n = n + 1
      p_soln[time,:] = p_offset + p_soln[time,:] + ((16.*pb_sec*pow((Lx/2.),2))/(chi*pow(math.pi,3)))*sum_term

    # run PFLOTRAN simulation
    run_pflotran(input_prefix,nxyz,dxyz,lxyz,remove,screen_on,pf_exe,mpi_exe)
    
    # load data from HDF5
    index_string = 'Time:  1.00000E-01 d/Liquid_Pressure [Pa]'
    p_pflotran[0,:] = read_pflotran_output_1D(path+
		      '/1D_transient_pressure_BC_1st_kind.h5',index_string,False)
    index_string = 'Time:  2.50000E-01 d/Liquid_Pressure [Pa]'
    p_pflotran[1,:] = read_pflotran_output_1D(path+
		      '/1D_transient_pressure_BC_1st_kind.h5',index_string,False)
    index_string = 'Time:  5.00000E-01 d/Liquid_Pressure [Pa]'
    p_pflotran[2,:] = read_pflotran_output_1D(path+
		      '/1D_transient_pressure_BC_1st_kind.h5',index_string,False)
    index_string = 'Time:  7.50000E-01 d/Liquid_Pressure [Pa]'
    p_pflotran[3,:] = read_pflotran_output_1D(path+
		      '/1D_transient_pressure_BC_1st_kind.h5',index_string,remove)
    ierr = check(p_pflotran)
  
    # Convert units [Pa -> MPa]
    p_pflotran = p_pflotran/1.e6
    p_soln = p_soln/1.e6

    max_percent_error = calc_relative_error(p_soln[:,1:nx+1],p_pflotran,ierr)
    record_error(error_analysis,nxyz_record,dxyz_record,max_percent_error,
                 nxyz,dxyz,try_count)
    test_pass = does_pass(max_percent_error,try_count,num_tries)
    increase_nxyz(nxyz,1,1); dxyz = lxyz/nxyz
    adjust_nondim_lxyz(dxyz,lxyz,1); dxyz = lxyz/nxyz

  # Plot the PFLOTRAN and analytical solutions
  plot_1D_transient(path,t_soln,'day',x_soln,p_soln,x_pflotran,p_pflotran,
                    'Distance [m]','Pressure [MPa]',"{0:.2f}".format(max_percent_error))
  
  # Plot error analysis
  plot_error(error_analysis,nxyz_record,dxyz_record,path,try_count,1)
  
  # Add test result to report card
  add_to_report(path,test_pass,max_percent_error,ierr)

  return;
################################################################################


#==============================================================================#
def flow_transient_1D_BC2ndkind(path,input_prefix,remove,screen_on,pf_exe,
				                             mpi_exe,num_tries):
#==============================================================================#
# Based on:
# Kolditz, et al. (2015) Thermo-Hydro-Mechanical-Chemical Processes in
# Fractured Porous Media: Modelling and Benchmarking, Closed Form Solutions,
# Springer International Publishing, Switzerland.
# Section 2.2.8, pg.20
# "A Transient 1D Pressure Distribution, Time-Dependent Boundary Conditions
# of 2nd Kind"
# With some modification of the parameter values given in Kolditz (2015)
#
# Author: Jenn Frederick
# Date: 08/24/2016
# ******************************************************************************
  nxyz = np.zeros(3) + 1
  dxyz = np.zeros(3) + 1.
  lxyz = np.zeros(3) + 1.
  error_analysis = np.zeros(num_tries)
  dxyz_record = np.zeros((num_tries,3))
  nxyz_record = np.zeros((num_tries,3))
  test_pass = False
  try_count = 0
  
  # initial discretization values
  lxyz[0] = 25.         # [m] lx
  nxyz[0] = 5           # [m] nx 
  dxyz = lxyz/nxyz      # [m]
  adjust_nondim_lxyz(dxyz,lxyz,1); dxyz = lxyz/nxyz
  k = 1.0e-14           # [m2]
  mu = 0.864e-3         # [Pa*sec]
  por = 0.20 
  K = 1.0e-8            # [1/Pa]
  q = 9.0e-6            # [m/s/day]
  p_offset = 101325     # [Pa]
  chi = k/(por*K*mu)
  ierr = 0
  
  while (not test_pass) and (try_count < num_tries): 
    print_discretization(lxyz,nxyz,dxyz)
    nx = int(nxyz[0]); ny = int(nxyz[1]); nz = int(nxyz[2])
    dx = dxyz[0]; dy = dxyz[1]; dz = dxyz[2]
    Lx = lxyz[0]; Ly = lxyz[1]; Lz = lxyz[2]
    try_count = try_count + 1

    x_soln = np.linspace(0.+(dx/2.),Lx-(dx/2.),nx)  # [m]
    x_soln = np.concatenate((x_soln,[Lx]),axis=0)   # [m]
    p_soln = np.zeros((4,nx+1))                     # [Pa]
    x_pflotran = x_soln[0:nx]                       # [m]
    p_pflotran = np.zeros((4,nx))                   # [Pa]
    t_soln = np.array([0.01,0.04,0.09,0.12])        # [day]

    # create the analytical solution
    p1 = np.zeros(nx+1)
    p2 = np.zeros(nx+1)
    i3erfc1 = np.zeros(nx+1)
    i3erfc2 = np.zeros(nx+1)
    for time in range(4):
      t = t_soln[time]*24.0*3600.0  # [sec]
      sum_term = np.zeros(nx+1)
      sum_term_old = np.zeros(nx+1)
      n = 0
      epsilon = 1.0
      while epsilon > epsilon_value:
	p1 = ((((2*n)+1)*Lx)-x_soln)/(2.*math.sqrt(chi*t))
	i3erfc1 = (1./12.)*np.sqrt(math.pi)*p1**3*erf(1.0*p1) - (1./12.)*np.sqrt(math.pi)*p1**3 + (1./3.)*p1**2*np.exp(-1.0*p1**2) + 0.5*p1*(-0.5*p1*np.exp(-1.0*p1**2) + 0.25*np.sqrt(math.pi)*erf(1.0*p1)) - 0.125*np.sqrt(math.pi)*p1 + (1./12.)*np.exp(-1.0*p1**2)
	p2 = ((((2*n)+1)*Lx)+x_soln)/(2.*math.sqrt(chi*t))
	i3erfc2 = (1./12.)*np.sqrt(math.pi)*p2**3*erf(1.0*p2) - (1./12.)*np.sqrt(math.pi)*p2**3 + (1./3.)*p2**2*np.exp(-1.0*p2**2) + 0.5*p2*(-0.5*p2*np.exp(-1.0*p2**2) + 0.25*np.sqrt(math.pi)*erf(1.0*p2)) - 0.125*np.sqrt(math.pi)*p2 + (1./12.)*np.exp(-1.0*p2**2)
	sum_term_old = sum_term
	sum_term = sum_term_old + (2./math.sqrt(math.pi))*i3erfc1 + (2./math.sqrt(math.pi))*i3erfc2
	epsilon = np.max(np.abs(sum_term_old-sum_term))
	n = n + 1
      p_soln[time,:] = p_offset + ((8*q*(1./(24.*3600.))*math.sqrt(chi*pow(t,3)))/(k/mu))*sum_term

    # To calculate the definite integral, use sympy:
    # p1, s = sym.symbols('p1 s')
    # i3erfc1 = sym.integrate(((pow((s-p1),3))/(3.*2.*1.))*sym.exp(-1.*pow(s,2)),(s,p1,sym.oo))
    # p2, s = sym.symbols('p2 s')
    # i3erfc2 = sym.integrate(((pow((s-p2),3))/(3.*2.*1.))*sym.exp(-1.*pow(s,2)),(s,p2,sym.oo))

    # run PFLOTRAN simulation
    run_pflotran(input_prefix,nxyz,dxyz,lxyz,remove,screen_on,pf_exe,mpi_exe)
    
    # load data from HDF5
    index_string = 'Time:  1.00000E-02 d/Liquid_Pressure [Pa]'
    p_pflotran[0,:] = read_pflotran_output_1D(path+
		      '/1D_transient_pressure_BC_2nd_kind.h5',index_string,False)
    index_string = 'Time:  4.00000E-02 d/Liquid_Pressure [Pa]'
    p_pflotran[1,:] = read_pflotran_output_1D(path+
		      '/1D_transient_pressure_BC_2nd_kind.h5',index_string,False)
    index_string = 'Time:  9.00000E-02 d/Liquid_Pressure [Pa]'
    p_pflotran[2,:] = read_pflotran_output_1D(path+
		      '/1D_transient_pressure_BC_2nd_kind.h5',index_string,False)
    index_string = 'Time:  1.20000E-01 d/Liquid_Pressure [Pa]'
    p_pflotran[3,:] = read_pflotran_output_1D(path+
		      '/1D_transient_pressure_BC_2nd_kind.h5',index_string,remove)
    ierr = check(p_pflotran)
    
    # Convert units [Pa -> MPa]
    p_pflotran = p_pflotran/1.e6
    p_soln = p_soln/1.e6

    max_percent_error = calc_relative_error(p_soln[:,0:nx],p_pflotran,ierr)
    record_error(error_analysis,nxyz_record,dxyz_record,max_percent_error,
                 nxyz,dxyz,try_count)
    test_pass = does_pass(max_percent_error,try_count,num_tries)
    increase_nxyz(nxyz,1,0); dxyz = lxyz/nxyz
    adjust_nondim_lxyz(dxyz,lxyz,1); dxyz = lxyz/nxyz

  # Plot the PFLOTRAN and analytical solutions
  plot_1D_transient(path,t_soln,'day',x_soln,p_soln,x_pflotran,p_pflotran,
                    'Distance [m]','Pressure [MPa]',"{0:.2f}".format(max_percent_error))
  
  # Plot error analysis
  plot_error(error_analysis,nxyz_record,dxyz_record,path,try_count,1)
  
  # Add test result to report card
  add_to_report(path,test_pass,max_percent_error,ierr)

  return;
################################################################################


#==============================================================================#
def flow_transient_1D_BC1st2ndkind(path,input_prefix,remove,screen_on,pf_exe,
				                             mpi_exe,num_tries):
#==============================================================================#
# Based on:
# Kolditz, et al. (2015) Thermo-Hydro-Mechanical-Chemical Processes in
# Fractured Porous Media: Modelling and Benchmarking, Closed Form Solutions,
# Springer International Publishing, Switzerland.
# Section 2.2.9, pg.35
# "A Transient 1D Pressure Distribution, Non-Zero Initial Pressure,
# Boundary Conditions of 1st and 2nd Kind"
# With some modification of the parameter values given in Kolditz (2015)
#
# Author: Jenn Frederick
# Date: 09/09/2016
# ******************************************************************************
  nxyz = np.zeros(3) + 1
  dxyz = np.zeros(3) + 1.
  lxyz = np.zeros(3) + 1.
  error_analysis = np.zeros(num_tries)
  dxyz_record = np.zeros((num_tries,3))
  nxyz_record = np.zeros((num_tries,3))
  test_pass = False
  try_count = 0
  
  # initial discretization values
  lxyz[0] = 100.        # [m] lx
  nxyz[0] = 10          # [m] nx 
  dxyz = lxyz/nxyz      # [m]
  adjust_nondim_lxyz(dxyz,lxyz,1); dxyz = lxyz/nxyz
  k = 1.0e-14           # [m2]
  mu = 1.728e-3         # [Pa-s]
  por = 0.20
  K = 1.0e-9            # [1/Pa]
  p_offset = 0.101325   # [MPa]
  chi = k/(por*K*mu)
  ierr = 0
  
  while (not test_pass) and (try_count < num_tries): 
    print_discretization(lxyz,nxyz,dxyz)
    nx = int(nxyz[0]); ny = int(nxyz[1]); nz = int(nxyz[2])
    dx = dxyz[0]; dy = dxyz[1]; dz = dxyz[2]
    Lx = lxyz[0]; Ly = lxyz[1]; Lz = lxyz[2]
    try_count = try_count + 1

    x_soln = np.linspace(0.+(dx/2.),Lx-(dx/2.),nx)      # [m]
    x_pflotran = x_soln                                 # [m]
    t_soln = np.array([0.05,0.10,0.25,0.50])            # [day]
    p_soln = np.zeros((4,nx))                           # [MPa]
    p_pflotran = np.zeros((4,nx))                       # [Pa]

    # create the analytical solution
    for time in range(4):
      t = t_soln[time]*24.0*3600.0  # [sec]
      sum_term_old = np.zeros(nx)
      sum_term = np.zeros(nx)
      n = 1
      epsilon = 1.0
      while epsilon > epsilon_value:
	sum_term_old = sum_term
	sum_term = sum_term_old + (np.cos(n*math.pi*x_soln/Lx)*np.exp(-chi*pow(n,2)*pow(math.pi,2)*t/pow(Lx,2))*(80./(3.*pow((n*math.pi),2)))*np.cos(n*math.pi/2.)*np.sin(n*math.pi/4.)*np.sin(3.*n*math.pi/20.))
	epsilon = np.max(np.abs(sum_term_old-sum_term))
	n = n + 1
      p_soln[time,:] = 0.50 + sum_term + p_offset
      
    # run PFLOTRAN simulation
    run_pflotran(input_prefix,nxyz,dxyz,lxyz,remove,screen_on,pf_exe,mpi_exe)
    
    # load data from HDF5
    index_string = 'Time:  5.00000E-02 d/Liquid_Pressure [Pa]'
    p_pflotran[0,:] = read_pflotran_output_1D(path+
		 '/1D_transient_pressure_BC_1st_2nd_kind.h5',index_string,False)
    index_string = 'Time:  1.00000E-01 d/Liquid_Pressure [Pa]'
    p_pflotran[1,:] = read_pflotran_output_1D(path+
		 '/1D_transient_pressure_BC_1st_2nd_kind.h5',index_string,False)
    index_string = 'Time:  2.50000E-01 d/Liquid_Pressure [Pa]'
    p_pflotran[2,:] = read_pflotran_output_1D(path+
		 '/1D_transient_pressure_BC_1st_2nd_kind.h5',index_string,False)
    index_string = 'Time:  5.00000E-01 d/Liquid_Pressure [Pa]'
    p_pflotran[3,:] = read_pflotran_output_1D(path+
		'/1D_transient_pressure_BC_1st_2nd_kind.h5',index_string,remove)
    ierr = check(p_pflotran)

    # Convert units [Pa -> MPa]
    p_pflotran = p_pflotran/1.e6

    max_percent_error = calc_relative_error(p_soln,p_pflotran,ierr)
    record_error(error_analysis,nxyz_record,dxyz_record,max_percent_error,
                 nxyz,dxyz,try_count)
    test_pass = does_pass(max_percent_error,try_count,num_tries)
    increase_nxyz(nxyz,1,0); dxyz = lxyz/nxyz
    adjust_nondim_lxyz(dxyz,lxyz,1); dxyz = lxyz/nxyz

  # Plot the PFLOTRAN and analytical solutions
  plot_1D_transient(path,t_soln,'day',x_soln,p_soln,x_pflotran,p_pflotran,
                    'Distance [m]','Pressure [MPa]',"{0:.2f}".format(max_percent_error))

  # Plot error analysis
  plot_error(error_analysis,nxyz_record,dxyz_record,path,try_count,1)
  
  # Add test result to report card
  add_to_report(path,test_pass,max_percent_error,ierr)

  return;
################################################################################


#==============================================================================#
def flow_transient_2D_BC1st2ndkind(path,input_prefix,remove,screen_on,pf_exe,
				                             mpi_exe,num_tries):
#==============================================================================#
# Based on:
# Kolditz, et al. (2015) Thermo-Hydro-Mechanical-Chemical Processes in
# Fractured Porous Media: Modelling and Benchmarking, Closed Form Solutions,
# Springer International Publishing, Switzerland.
# Section 2.2.10, pg.37
# "A Transient 2D Pressure Distribution, Non-Zero Initial Pressure,
# Boundary Conditions of 1st and 2nd Kind"
#
# Author: Jenn Frederick
# Date: 09/09/2016
# ******************************************************************************
  nxyz = np.zeros(3) + 1
  dxyz = np.zeros(3) + 1.
  lxyz = np.zeros(3) + 1.
  error_analysis = np.zeros(num_tries)
  dxyz_record = np.zeros((num_tries,3))
  nxyz_record = np.zeros((num_tries,3))
  test_pass = False
  try_count = 0
  
  # initial discretization values
  lxyz[0] = 100.        # [m] lx
  lxyz[1] = 100.        # [m] ly
  nxyz[0] = 10          # [m] nx 
  nxyz[1] = 10          # [m] ny 
  dxyz = lxyz/nxyz      # [m]
  adjust_nondim_lxyz(dxyz,lxyz,2); dxyz = lxyz/nxyz
  k = 1.0e-14           # [m2]
  mu = 1.728e-3         # [Pa-s]
  por = 0.20
  K = 1.0e-9            # [1/Pa]
  p_offset = 0.101325   # [MPa]
  p0 = 1.               # [MPa]
  chi = k/(por*K*mu)
  ierr = 0
  
  while (not test_pass) and (try_count < num_tries): 
    print_discretization(lxyz,nxyz,dxyz)
    nx = int(nxyz[0]); ny = int(nxyz[1]); nz = int(nxyz[2])
    dx = dxyz[0]; dy = dxyz[1]; dz = dxyz[2]
    Lx = lxyz[0]; Ly = lxyz[1]; Lz = lxyz[2]
    try_count = try_count + 1

    p_soln = np.zeros((4,nx,ny))                     # [MPa]
    p_pflotran = np.zeros((4,nx,ny))                 # [Pa]
    t_soln = np.array([0.0,0.04,0.06,0.10])          # [day]
    x_soln = np.linspace(0.+(dx/2.),Lx-(dx/2.),nx)   # [m]
    y_soln = np.linspace(0.+(dy/2.),Ly-(dy/2.),ny)   # [m]
    p1x = np.zeros(int(nx))
    p2y = np.zeros(int(ny))

    # for time=0, use the actual initial condition as the analytical
    # solution, not the analytical solution with t=0:
    fx = np.zeros(int(nx))
    for i in range(int(nx)):
      x = x_soln[i]
      if (0. <= x < (Lx/10.)):
	fx[i] = 0.
      if ((Lx/10.) <= x < (4.*Lx/10.)):
	fx[i] = (10./(3.*Lx))*(x) - (1./3.)
      if ((4.*Lx/10.) <= x < (6.*Lx/10.)):
	fx[i] = 1.
      if ((6.*Lx/10.) <= x < (9.*Lx/10.)):
	fx[i] = 3. - (10./(3.*Lx))*(x)
      if ((9.*Lx/10.) <= x < Lx):
	fx[i] = 0.
      
    fy = np.zeros(int(ny))
    for j in range(int(ny)):
      y = y_soln[j]
      if (0. <= y < (Ly/10.)):
	fy[j] = 0.
      if ((Ly/10.) <= y < (4.*Ly/10.)):
	fy[j] = (10./(3.*Ly))*(y) - (1./3.)
      if ((4.*Ly/10.) <= y < (6.*Ly/10.)):
	fy[j] = 1.
      if ((6.*Ly/10.) <= y < (9.*Ly/10.)):
	fy[j] = 3. - (10./(3.*Ly))*(y)
      if ((9.*Ly/10.) <= y < Ly):
	fy[j] = 0.

    p_soln_t0 = np.zeros((nx,ny))
    for i in range(int(nx)):
      for j in range(int(ny)):
	p_soln_t0[i][j] = ( p0*fx[i]*fy[j] + p_offset )  # [MPa]

    # create the analytical solution for all other times > 0
    for time in range(4):
      t = t_soln[time]*24.0*3600.0  # [sec]
      # create p1y
      sum_term_y = np.zeros(int(ny))
      sum_term_old_y = np.zeros(int(ny))
      n = 1
      epsilon = 1
      #while n < 5001:
      while epsilon > epsilon_value:
	sum_term_old_y = sum_term_y
	sum_term_y = sum_term_old_y + (np.cos(n*math.pi*y_soln/Ly)*np.exp(-chi*pow(n,2)*pow(math.pi,2)*t/pow(Ly,2))*(80./(3.*pow((n*math.pi),2)))*np.cos(n*math.pi/2.)*np.sin(n*math.pi/4.)*np.sin(3.*n*math.pi/20.))
	epsilon = np.max(np.abs(sum_term_old_y-sum_term_y))
	n = n + 1
      p2y = 0.5 + sum_term_y
      # create p1x
      sum_term_x = np.zeros(int(nx))
      sum_term_old_x = np.zeros(int(nx))
      n = 1
      epsilon = 1
      while epsilon > epsilon_value:
	sum_term_old_x = sum_term_x
	sum_term_x = sum_term_old_x + (np.sin(n*math.pi*x_soln/Lx)*np.exp(-chi*pow(n,2)*pow(math.pi,2)*t/pow(Lx,2))*(80./(3.*pow((n*math.pi),2)))*np.sin(n*math.pi/2.)*np.sin(n*math.pi/4.)*np.sin(3.*n*math.pi/20.))
	epsilon = np.max(np.abs(sum_term_old_x-sum_term_x))
	n = n + 1
      p1x = sum_term_x
      for i in range(int(nx)):
	for j in range(int(ny)):
	  p_soln[time,i,j] = ( p0*p1x[i]*p2y[j] + p_offset ) # [MPa]

    p_soln[0,:,:] = p_soln_t0[:,:]
    
    # run PFLOTRAN simulation
    run_pflotran(input_prefix,nxyz,dxyz,lxyz,remove,screen_on,pf_exe,mpi_exe)
    
    # load data from HDF5
    index_string = 'Time:  0.00000E+00 d/Liquid_Pressure [Pa]'
    p_pflotran[0,:,:] = read_pflotran_output_2D(path+
		 '/2D_transient_pressure_BC_1st_2nd_kind.h5',index_string,False)
    index_string = 'Time:  4.00000E-02 d/Liquid_Pressure [Pa]'
    p_pflotran[1,:,:] = read_pflotran_output_2D(path+
		 '/2D_transient_pressure_BC_1st_2nd_kind.h5',index_string,False)
    index_string = 'Time:  6.00000E-02 d/Liquid_Pressure [Pa]'
    p_pflotran[2,:,:] = read_pflotran_output_2D(path+
		 '/2D_transient_pressure_BC_1st_2nd_kind.h5',index_string,False)
    index_string = 'Time:  1.00000E-01 d/Liquid_Pressure [Pa]'
    p_pflotran[3,:,:] = read_pflotran_output_2D(path+
		'/2D_transient_pressure_BC_1st_2nd_kind.h5',index_string,remove)
    ierr = check(p_pflotran)

    # Convert units [Pa -> MPa]
    p_pflotran = p_pflotran/1.e6

    max_percent_error = calc_relative_error(p_soln,p_pflotran,ierr)
    record_error(error_analysis,nxyz_record,dxyz_record,max_percent_error,
                 nxyz,dxyz,try_count)
    test_pass = does_pass(max_percent_error,try_count,num_tries)
    increase_nxyz(nxyz,2,0); dxyz = lxyz/nxyz
    adjust_nondim_lxyz(dxyz,lxyz,2); dxyz = lxyz/nxyz

  # Plot the PFLOTRAN and analytical solution:
  loaded = 0
  plot_2D_transient(path,t_soln,'day',x_soln,y_soln,p_soln,p_pflotran,
            'Distance [m]','Pressure [MPa]',"{0:.2f}".format(max_percent_error),
            loaded)
  
  # Plot error analysis
  plot_error(error_analysis,nxyz_record,dxyz_record,path,try_count,2)
  
  # Add test result to report card
  add_to_report(path,test_pass,max_percent_error,ierr)

  return;
################################################################################


################################################################################
#### STEADY GAS TESTS ##########################################################
################################################################################


#==============================================================================#
def gas_steady_1D_BC1stkind(path,input_prefix,remove,screen_on,pf_exe,
				                             mpi_exe,num_tries):
#==============================================================================#
#
# Kolditz, et al. (2015) Thermo-Hydro-Mechanical-Chemical Processes in
# Fractured Porous Media: Modelling and Benchmarking, Closed Form Solutions,
# Springer International Publishing, Switzerland.
# Section 2.3.1, pg.40
# "A 1D Steady-State Gas Pressure Distribution, Boundary Conditions of 1st Kind"
#
# Author: Jenn Frederick
# Date: 09/09/2016
#*******************************************************************************
  nxyz = np.zeros(3) + 1
  dxyz = np.zeros(3) + 1.
  lxyz = np.zeros(3) + 1.
  error_analysis = np.zeros(num_tries)
  dxyz_record = np.zeros((num_tries,3))
  nxyz_record = np.zeros((num_tries,3))
  test_pass = False
  try_count = 0
  
  # initial discretization values
  lxyz[0] = 40.     # [m] lx
  nxyz[0] = 8       # [m] nx 
  dxyz = lxyz/nxyz  # [m]
  adjust_nondim_lxyz(dxyz,lxyz,1); dxyz = lxyz/nxyz
  p0 = 2.0e5        # [Pa]
  p1 = 1.0e5        # [Pa]
  ierr = 0
  
  while (not test_pass) and (try_count < num_tries): 
    print_discretization(lxyz,nxyz,dxyz)
    nx = int(nxyz[0]); ny = int(nxyz[1]); nz = int(nxyz[2])
    dx = dxyz[0]; dy = dxyz[1]; dz = dxyz[2]
    Lx = lxyz[0]; Ly = lxyz[1]; Lz = lxyz[2]
    try_count = try_count + 1

    x_soln = np.linspace(0.+(dx/2.),Lx-(dx/2.),nx)  # [m]
    x_pflotran = x_soln                             # [m]
    x_soln = np.concatenate(([0.],x_soln),axis=0)   # [m]
    x_soln = np.concatenate((x_soln,[Lx]),axis=0)   # [m]
    p_soln = np.zeros(nx+2)                         # [Pa]
    p_pflotran = np.zeros(nx)                       # [Pa]

    # create the analytical solution
    p_soln = np.sqrt((((p1*p1) - (p0*p0))*(x_soln/Lx)) + (p0*p0)) # [Pa]
    
    # run PFLOTRAN simulation
    run_pflotran(input_prefix,nxyz,dxyz,lxyz,remove,screen_on,pf_exe,mpi_exe)
    
    # load data from hdf5
    index_string = 'Time:  1.00000E+00 y/Gas_Pressure [Pa]'
    p_pflotran[:] = read_pflotran_output_1D(path+
	            '/1D_steady_gas_BC_1st_kind.h5',index_string,remove)
    ierr = check(p_pflotran)

    # convert pressure units [Pa -> MPa]
    p_pflotran = p_pflotran/1.0e6         # [MPa]
    p_soln = p_soln/1.0e6                 # [MPa]

    max_percent_error = calc_relative_error(p_soln[1:nx+1],p_pflotran,ierr)
    record_error(error_analysis,nxyz_record,dxyz_record,max_percent_error,
                 nxyz,dxyz,try_count)
    test_pass = does_pass(max_percent_error,try_count,num_tries)
    increase_nxyz(nxyz,1,0); dxyz = lxyz/nxyz
    adjust_nondim_lxyz(dxyz,lxyz,1); dxyz = lxyz/nxyz

  # Plot the PFLOTRAN and analytical solutions
  plot_1D_steady(path,x_soln,p_soln,x_pflotran,p_pflotran,'Distance [m]',
                 'Gas Pressure [MPa]',"{0:.2f}".format(max_percent_error))
  
  # Plot error analysis
  plot_error(error_analysis,nxyz_record,dxyz_record,path,try_count,1)
  
  # Add test result to report card
  add_to_report(path,test_pass,max_percent_error,ierr)

  return;
################################################################################


#==============================================================================#
def gas_steady_1D_BC1st2ndkind(path,input_prefix,remove,screen_on,pf_exe,
				                             mpi_exe,num_tries):
#==============================================================================#
#
# Kolditz, et al. (2015) Thermo-Hydro-Mechanical-Chemical Processes in
# Fractured Porous Media: Modelling and Benchmarking, Closed Form Solutions,
# Springer International Publishing, Switzerland.
# Section 2.3.2, pg.41
# "A 1D Steady-State Gas Pressure Distribution, Boundary Conditions of 1st 
# and 2nd Kind"
#
# Author: Jenn Frederick
# Date: 11/03/2016
#*******************************************************************************
  nxyz = np.zeros(3) + 1
  dxyz = np.zeros(3) + 1.
  lxyz = np.zeros(3) + 1.
  error_analysis = np.zeros(num_tries)
  dxyz_record = np.zeros((num_tries,3))
  nxyz_record = np.zeros((num_tries,3))
  test_pass = False
  try_count = 0
  
  # initial discretization values
  lxyz[0] = 40.     # [m] lx
  nxyz[0] = 8       # [m] nx 
  adjust_nondim_lxyz(dxyz,lxyz,1); dxyz = lxyz/nxyz
  dxyz = lxyz/nxyz  # [m]
  k = 1.0e-15    # [m^2]
  mu = 1.0e-5    # [Pa*s]
  p1 = 1.0e5     # [Pa]
  Q = 0.17       # [Pa*m/s]
  ierr = 0
  
  while (not test_pass) and (try_count < num_tries): 
    print_discretization(lxyz,nxyz,dxyz)
    nx = int(nxyz[0]); ny = int(nxyz[1]); nz = int(nxyz[2])
    dx = dxyz[0]; dy = dxyz[1]; dz = dxyz[2]
    Lx = lxyz[0]; Ly = lxyz[1]; Lz = lxyz[2]
    try_count = try_count + 1

    x_soln = np.linspace(0.+(dx/2.),Lx-(dx/2.),nx)    # [m]
    x_pflotran = x_soln                               # [m]
    x_soln = np.concatenate(([0.],x_soln),axis=0)     # [m]
    x_soln = np.concatenate((x_soln,[Lx]),axis=0)     # [m]
    p_soln = np.zeros(nx+2)                           # [Pa]
    p_pflotran = np.zeros(nx)                         # [Pa]

    # create the analytical solution
    p_soln = np.sqrt(((2*Q*mu)/k)*(Lx-x_soln)+(p1**2)) # [Pa]

    # run PFLOTRAN simulation
    run_pflotran(input_prefix,nxyz,dxyz,lxyz,remove,screen_on,pf_exe,mpi_exe)

    # load data from hdf5
    index_string = 'Time:  2.00000E+01 y/Gas_Pressure [Pa]'
    p_pflotran[:] = read_pflotran_output_1D(path+
	            '/1D_steady_gas_BC_1st_2nd_kind.h5',index_string,remove)
    ierr = check(p_pflotran)

    # convert pressure units [Pa -> MPa]
    p_pflotran = p_pflotran/1.0e6         # [MPa]
    p_soln = p_soln/1.0e6                 # [MPa]

    max_percent_error = calc_relative_error(p_soln[1:nx+1],p_pflotran,ierr)
    record_error(error_analysis,nxyz_record,dxyz_record,max_percent_error,
                 nxyz,dxyz,try_count)
    test_pass = does_pass(max_percent_error,try_count,num_tries)
    increase_nxyz(nxyz,1,0); dxyz = lxyz/nxyz
    adjust_nondim_lxyz(dxyz,lxyz,1); dxyz = lxyz/nxyz

  # Plot the PFLOTRAN and analytical solutions
  plot_1D_steady(path,x_soln,p_soln,x_pflotran,p_pflotran,'Distance [m]',
                 'Gas Pressure [MPa]',"{0:.2f}".format(max_percent_error))
  
  # Plot error analysis
  plot_error(error_analysis,nxyz_record,dxyz_record,path,try_count,1)
  
  # Add test result to report card
  add_to_report(path,test_pass,max_percent_error,ierr)

  return;
################################################################################


#==============================================================================#
def gas_steady_2D_BC1st2ndkind(path,input_prefix,remove,screen_on,pf_exe,
				                             mpi_exe,num_tries):
#==============================================================================#
#
# Kolditz, et al. (2015) Thermo-Hydro-Mechanical-Chemical Processes in
# Fractured Porous Media: Modelling and Benchmarking, Closed Form Solutions,
# Springer International Publishing, Switzerland.
# Section 2.3.3, pg.42
# "A 2D Steady-State Gas Pressure Distribution, Boundary Conditions of 1st 
# and 2nd Kind"
#
# Author: Jenn Frederick
# Date: 11/04/2016
#*******************************************************************************
  nxyz = np.zeros(3) + 1
  dxyz = np.zeros(3) + 1.
  lxyz = np.zeros(3) + 1.
  error_analysis = np.zeros(num_tries)
  dxyz_record = np.zeros((num_tries,3))
  nxyz_record = np.zeros((num_tries,3))
  test_pass = False
  try_count = 0
  
  # initial discretization values
  lxyz[0] = 1.      # [m] lx
  lxyz[1] = 1.      # [m] ly
  nxyz[0] = 10      # [m] nx 
  nxyz[1] = 10      # [m] ny 
  dxyz = lxyz/nxyz  # [m]
  adjust_nondim_lxyz(dxyz,lxyz,2); dxyz = lxyz/nxyz
  p0 = 1.0e5        # [Pa]
  ierr = 0
  
  while (not test_pass) and (try_count < num_tries): 
    print_discretization(lxyz,nxyz,dxyz)
    nx = int(nxyz[0]); ny = int(nxyz[1]); nz = int(nxyz[2])
    dx = dxyz[0]; dy = dxyz[1]; dz = dxyz[2]
    Lx = lxyz[0]; Ly = lxyz[1]; Lz = lxyz[2]
    try_count = try_count + 1

    p_soln = np.zeros((nx,ny))                       # [Pa]
    p_pflotran = np.zeros((nx,ny))                   # [Pa]
    x_soln = np.linspace(0.+(dx/2.),Lx-(dx/2.),nx)   # [m]
    y_soln = np.linspace(0.+(dy/2.),Ly-(dy/2.),ny)   # [m]

    # create the analytical solution
    i = -1
    for x in x_soln:
      i = i + 1
      j = -1
      for y in y_soln:
        j = j + 1
        p_soln[i,j] = p0*np.sqrt(1+3*((x*y)/(Lx*Ly)))

    # run PFLOTRAN simulation
    os.chdir('../')
    command_line = 'python create_dataset.py'
    command_line = command_line + ' -nx ' + str(nx) + ' -ny ' + str(ny)
    command_line = command_line + ' -lx ' + str(Lx) + ' -ly ' + str(Ly)
    command_line = command_line + ' -dx ' + str(dx) + ' -dy ' + str(dy)
    os.system(command_line)
    os.chdir('general_mode')
    run_pflotran(input_prefix,nxyz,dxyz,lxyz,remove,screen_on,pf_exe,mpi_exe)

    # load data from hdf5
    index_string = 'Time:  1.00000E+01 y/Gas_Pressure [Pa]'
    p_pflotran[:,:] = read_pflotran_output_2D(path+
	              '/2D_steady_gas_BC_1st_2nd_kind.h5',index_string,remove)
    ierr = check(p_pflotran)

    # convert pressure units [Pa -> MPa]
    p_pflotran = p_pflotran/1.0e6         # [MPa]
    p_soln = p_soln/1.0e6                 # [MPa]

    max_percent_error = calc_relative_error(p_soln,p_pflotran,ierr)
    record_error(error_analysis,nxyz_record,dxyz_record,max_percent_error,
                 nxyz,dxyz,try_count)
    test_pass = does_pass(max_percent_error,try_count,num_tries)
    increase_nxyz(nxyz,2,0); dxyz = lxyz/nxyz
    adjust_nondim_lxyz(dxyz,lxyz,2); dxyz = lxyz/nxyz

  # Plot the PFLOTRAN and analytical solutions
  plot_2D_steady(path,x_soln,y_soln,p_soln,p_pflotran,'Distance [m]',
                 'Gas Pressure [MPa]',"{0:.2f}".format(max_percent_error))
  
  # Plot error analysis
  plot_error(error_analysis,nxyz_record,dxyz_record,path,try_count,2)
  
  # Add test result to report card
  add_to_report(path,test_pass,max_percent_error,ierr)

  return;
################################################################################


#==============================================================================#
def gas_steady_3D_BC2ndkind(path,input_prefix,remove,screen_on,pf_exe,
				                             mpi_exe,num_tries):
#==============================================================================#
#
# Kolditz, et al. (2015) Thermo-Hydro-Mechanical-Chemical Processes in
# Fractured Porous Media: Modelling and Benchmarking, Closed Form Solutions,
# Springer International Publishing, Switzerland.
# Section 2.3.4, pg.43
# "A 3D Steady-State Gas Pressure Distribution, Boundary Conditions of 2nd 
# Kind"
#
# Author: Jenn Frederick
# Date: 11/07/2016
#*******************************************************************************
  nxyz = np.zeros(3) + 1
  dxyz = np.zeros(3) + 1.
  lxyz = np.zeros(3) + 1.
  error_analysis = np.zeros(num_tries)
  dxyz_record = np.zeros((num_tries,3))
  nxyz_record = np.zeros((num_tries,3))
  test_pass = False
  try_count = 0
  
  # initial discretization values
  lxyz[0] = 1.      # [m] lx
  lxyz[1] = 1.      # [m] ly
  lxyz[2] = 1.      # [m] lz
  nxyz[0] = 5       # [m] nx 
  nxyz[1] = 5       # [m] ny 
  nxyz[2] = 5       # [m] nz 
  dxyz = lxyz/nxyz  # [m]
  adjust_nondim_lxyz(dxyz,lxyz,3); dxyz = lxyz/nxyz
  p0 = 1.0e5        # [Pa]
  ierr = 0
  
  while (not test_pass) and (try_count < num_tries): 
    print_discretization(lxyz,nxyz,dxyz)
    nx = int(nxyz[0]); ny = int(nxyz[1]); nz = int(nxyz[2])
    dx = dxyz[0]; dy = dxyz[1]; dz = dxyz[2]
    Lx = lxyz[0]; Ly = lxyz[1]; Lz = lxyz[2]
    try_count = try_count + 1

    p_soln = np.zeros((nx,ny,nz))                    # [Pa]
    p_pflotran = np.zeros((nx,ny,nz))                # [Pa]
    x_soln = np.linspace(0.+(dx/2.),Lx-(dx/2.),nx)   # [m]
    y_soln = np.linspace(0.+(dy/2.),Ly-(dy/2.),ny)   # [m]
    z_soln = np.linspace(0.+(dz/2.),Lz-(dz/2.),nz)   # [m]

    # create the analytical solution
    i = -1
    for x in x_soln:
      i = i + 1
      j = -1
      for y in y_soln:
        j = j + 1
        k = -1
        for z in z_soln:
          k = k + 1
          p_soln[i,j,k] = p0*np.sqrt(1.+(3./2.)*((x/Lx)*(y/Ly)+(z/Lz)))

    # run PFLOTRAN simulation
    os.chdir('../')
    command_line = 'python create_dataset.py'
    command_line = command_line + ' -nx ' +str(nx)+ ' -ny ' +str(ny)+ ' -nz ' +str(nz)
    command_line = command_line + ' -lx ' +str(Lx)+ ' -ly ' +str(Ly)+ ' -lz ' +str(Lz)
    command_line = command_line + ' -dx ' +str(dx)+ ' -dy ' +str(dy)+ ' -dz ' +str(dz)
    os.system(command_line)
    os.chdir('general_mode')
    run_pflotran(input_prefix,nxyz,dxyz,lxyz,remove,screen_on,pf_exe,mpi_exe)
    
    # load data from hdf5
    index_string = 'Time:  1.00000E+01 y/Gas_Pressure [Pa]'
    p_pflotran[:,:,:] = read_pflotran_output_3D(path+
	                '/3D_steady_gas_BC_2nd_kind.h5',index_string,remove)
    ierr = check(p_pflotran)

    # convert pressure units [Pa -> MPa]
    p_pflotran = p_pflotran/1.0e6         # [MPa]
    p_soln = p_soln/1.0e6                 # [MPa]

    max_percent_error = calc_relative_error(p_soln,p_pflotran,ierr)
    record_error(error_analysis,nxyz_record,dxyz_record,max_percent_error,
                 nxyz,dxyz,try_count)
    test_pass = does_pass(max_percent_error,try_count,num_tries)
    increase_nxyz(nxyz,3,0); dxyz = lxyz/nxyz
    adjust_nondim_lxyz(dxyz,lxyz,3); dxyz = lxyz/nxyz

  z_levels = np.array([0,math.floor((nx-1)/3),math.floor((nx-1)/1.5),(nx-1)])
  z_levels = z_levels.astype(int)
  # Plot the PFLOTRAN and analytical solutions
  plot_3D_steady(path,x_soln,y_soln,z_levels,p_soln,p_pflotran,'Distance [m]',
                 'Gas Pressure [MPa]',"{0:.2f}".format(max_percent_error))
  
  # Plot error analysis
  plot_error(error_analysis,nxyz_record,dxyz_record,path,try_count,3)
  
  # Add test result to report card
  add_to_report(path,test_pass,max_percent_error,ierr)

  return;
################################################################################

################################################################################
#### STEADY TRANSPORT TESTS ####################################################
################################################################################

#==============================================================================#
def transport_steady_1D_BC1stkind(path,input_prefix,remove,screen_on,pf_exe,
                                                         mpi_exe,num_tries):
#==============================================================================#
  nxyz = np.zeros(3) + 1
  dxyz = np.zeros(3) + 1.
  lxyz = np.zeros(3) + 1.
  error_analysis = np.zeros(num_tries)
  dxyz_record = np.zeros((num_tries,3))
  nxyz_record = np.zeros((num_tries,3))
  test_pass = False
  try_count = 0

  # initial discretization values
  lxyz[0] = 100.    # [m] lx
  nxyz[0] = 8       # [m] nx 
  dxyz = lxyz/nxyz  # [m]
  adjust_nondim_lxyz(dxyz,lxyz,1); dxyz = lxyz/nxyz
  C0 = 1.           # [M]
  ierr = 0
  
  while (not test_pass) and (try_count < num_tries): 
    print_discretization(lxyz,nxyz,dxyz)
    nx = int(nxyz[0]); ny = int(nxyz[1]); nz = int(nxyz[2])
    dx = dxyz[0]; dy = dxyz[1]; dz = dxyz[2]
    Lx = lxyz[0]; Ly = lxyz[1]; Lz = lxyz[2]
    try_count = try_count + 1

    x_soln = np.linspace(0.+(dx/2.),Lx-(dx/2.),nx)    # [m]
    x_pflotran = x_soln                               # [m]
    x_soln = np.concatenate(([0.],x_soln),axis=0)     # [m]
    x_soln = np.concatenate((x_soln,[Lx]),axis=0)     # [m]
    C_soln = np.zeros(nx+2)                           # [C]
    C_pflotran = np.zeros(nx)

    # create the analytical solution
    C_soln = np.array(x_soln/Lx + C0)                 # [C]
  
    # run PFLOTRAN simulation
    run_pflotran(input_prefix,nxyz,dxyz,lxyz,remove,screen_on,pf_exe,mpi_exe)

    # load data from hdf5
    filename = path+ '/1D_steady_transport_BC_1st_kind.h5'
    index_str = '/Time:  1.05145E+05 y/Total_tracer [M]'
    C_pflotran[:] = read_pflotran_output_1D(path+ 
                        '/1D_steady_transport_BC_1st_kind.h5',index_str,remove)
    ierr = check(C_pflotran)
    
    max_percent_error = calc_relative_error(C_soln[1:nx+1],C_pflotran,ierr)
    record_error(error_analysis,nxyz_record,dxyz_record,max_percent_error,
                 nxyz,dxyz,try_count)
    test_pass = does_pass(max_percent_error,try_count,num_tries)
    increase_nxyz(nxyz,1,0); dxyz = lxyz/nxyz
    adjust_nondim_lxyz(dxyz,lxyz,1); dxyz = lxyz/nxyz

  # Plot the PFLOTRAN and analytical solutions
  plot_1D_steady(path,x_soln,C_soln,x_pflotran,C_pflotran,'Distance [m]',
                 'Total_tracer[M]',"{0:.2f}".format(max_percent_error))
  
  # Plot error analysis
  plot_error(error_analysis,nxyz_record,dxyz_record,path,try_count,1)
  
  # Add test result to report card
  add_to_report(path,test_pass,max_percent_error,ierr)

  return;

################################################################################

#==============================================================================#
def transport_steady_2D_BC1stkind(path,input_prefix,remove,screen_on,pf_exe,
                                                        mpi_exe,num_tries):
#==============================================================================#
  nxyz = np.zeros(3) + 1
  dxyz = np.zeros(3) + 1.
  lxyz = np.zeros(3) + 1.
  error_analysis = np.zeros(num_tries)
  dxyz_record = np.zeros((num_tries,3))
  nxyz_record = np.zeros((num_tries,3))
  lxyz[0] = 1.      # [m] lx
  lxyz[1] = 1.      # [m] ly
  test_pass = False
  try_count = 0

  # initial discretization values
  nxyz[0] = 10      # [m] nx
  nxyz[1] = 10      # [m] ny
  dxyz = lxyz/nxyz  # [m] dx
  adjust_nondim_lxyz(dxyz,lxyz,2); dxyz = lxyz/nxyz
  C0 = 1.           # [M]
  ierr = 0
  # the 0.9 value
  vx = lxyz-dxyz # [m]

  while (not test_pass) and (try_count < num_tries): 
    print_discretization(lxyz,nxyz,dxyz)
    nx = int(nxyz[0]); ny = int(nxyz[1]); nz = int(nxyz[2])
    dx = dxyz[0]; dy = dxyz[1]; dz = dxyz[2]
    Lx = lxyz[0]; Ly = lxyz[1]; Lz = lxyz[2]
    try_count = try_count + 1

    x_soln = np.linspace(0.+(dx/2.),Lx-(dx/2.),nx)     # [m]
    y_soln = np.linspace(0.+(dy/2.),Ly-(dy/2.),ny)     # [m]
    C_pflotran = np.zeros((nx,ny))                     # [C]
    C_soln = np.zeros((nx,ny))                         # [C]

    # create the analytical solution
    i = -1
    for x in x_soln:
      i = i + 1
      j = -1
      for y in y_soln:
        j = j + 1
        C_soln[i,j] = C0*(x/Lx)*(y/Ly)
        
    # run PFLOTRAN simulation
    run_pflotran(input_prefix,nxyz,dxyz,lxyz,remove,screen_on,pf_exe,mpi_exe)

    # load data from hdf5
    index_string = '/Time:  1.00000E+05 y/Total_tracer [M]'
    C_pflotran[:,:] = read_pflotran_output_2D(path+ 
                     '/2D_steady_transport_BC_1st_kind.h5',index_string,remove)
    ierr = check(C_pflotran)

    max_percent_error = calc_relative_error(C_soln,C_pflotran,ierr)
    record_error(error_analysis,nxyz_record,dxyz_record,max_percent_error,
                 nxyz,dxyz,try_count)
    test_pass = does_pass(max_percent_error,try_count,num_tries)
    increase_nxyz(nxyz,2,0); dxyz = lxyz/nxyz
    adjust_nondim_lxyz(dxyz,lxyz,2); dxyz = lxyz/nxyz

  # Plot the PFLOTRAN and analytical solutions
  plot_2D_steady(path,x_soln,y_soln,C_soln,C_pflotran,'Distance [m]',
                 'Total_tracer [M]',"{0:.2f}".format(max_percent_error))
  
  # Plot error analysis
  plot_error(error_analysis,nxyz_record,dxyz_record,path,try_count,2)
  
  # Add test result to report card
  add_to_report(path,test_pass,max_percent_error,ierr)

  return;

################################################################################

#==============================================================================#
def transport_steady_3D_BC1stkind(path,input_prefix,remove,screen_on,pf_exe,
				                         mpi_exe,num_tries):
#==============================================================================#
  nxyz = np.zeros(3) + 1
  dxyz = np.zeros(3) + 1.
  lxyz = np.zeros(3) + 1.
  error_analysis = np.zeros(num_tries)
  dxyz_record = np.zeros((num_tries,3))
  nxyz_record = np.zeros((num_tries,3))
  lxyz[0] = 1.      # [m] lx
  lxyz[1] = 1.      # [m] ly
  lxyz[2] = 1.      # [m] lz
  test_pass = False
  try_count = 0

  # initial discretization values
  nxyz[0] = 5       # [m] nx
  nxyz[1] = 5       # [m] ny
  nxyz[2] = 5       # [m] nz
  adjust_nondim_lxyz(dxyz,lxyz,3); dxyz = lxyz/nxyz
  dxyz = lxyz/nxyz  # [m]
  C0 = 1.           # [M]
  ierr = 0
  
  while (not test_pass) and (try_count < num_tries): 
    print_discretization(lxyz,nxyz,dxyz)
    nx = int(nxyz[0]); ny = int(nxyz[1]); nz = int(nxyz[2])
    dx = dxyz[0]; dy = dxyz[1]; dz = dxyz[2]
    Lx = lxyz[0]; Ly = lxyz[1]; Lz = lxyz[2]
    try_count = try_count + 1

    C_soln = np.zeros((nx,ny,nz))                   # [M]
    C_pflotran = np.zeros((nx,ny,nz))               # [M]
    x_soln = np.linspace(0.+(dx/2.),Lx-(dx/2.),nx)  # [m]
    y_soln = np.linspace(0.+(dy/2.),Ly-(dx/2.),ny)  # [m]
    z_soln = np.linspace(0.+(dz/2.),Lz-(dx/2.),nz)  # [m]

    # create the analytical solution
    i = -1
    for x in x_soln:
      i = i + 1
      j = -1
      for y in y_soln:
        j = j + 1
        k = -1
        for z in z_soln:
          k = k + 1
          C_soln[i,j,k] = C0*((x/Lx)+(y/Ly)+(z/Lz))

    # run PFLOTRAN simulation
    run_pflotran(input_prefix,nxyz,dxyz,lxyz,remove,screen_on,pf_exe,mpi_exe)
    
    # load data from hdf5
    index_string = '/Time:  7.00000E+00 y/Total_tracer [M]'
    C_pflotran[:,:,:] = read_pflotran_output_3D(path+
                     '/3D_steady_transport_BC_1st_kind.h5',index_string,remove)
    ierr = check(C_pflotran)

    max_percent_error = calc_relative_error(C_soln,C_pflotran,ierr)
    record_error(error_analysis,nxyz_record,dxyz_record,max_percent_error,
                 nxyz,dxyz,try_count)
    test_pass = does_pass(max_percent_error,try_count,num_tries)
    increase_nxyz(nxyz,3,0); dxyz = lxyz/nxyz
    adjust_nondim_lxyz(dxyz,lxyz,3); dxyz = lxyz/nxyz

  # Plot the PFLOTRAN and analytical solutions
  z_levels = np.array([0,math.floor((nx-1)/3),math.floor((nx-1)/1.5),(nx-1)])
  z_levels = z_levels.astype(int)

  plot_3D_steady(path,x_soln,y_soln,z_levels,C_soln,C_pflotran,'Distance [m]',
                 'Total_tracer [M]',"{0:.2f}".format(max_percent_error))
  
  # Plot error analysis
  plot_error(error_analysis,nxyz_record,dxyz_record,path,try_count,3)
  
  # Add test result to report card
  add_to_report(path,test_pass,max_percent_error,ierr)

  return;

################################################################################

################################################################################
#### TRANSIENT TRANSPORT TESTS #################################################
################################################################################

#==============================================================================#
def transport_transient_1D_IC(path,input_prefix,remove,screen_on,pf_exe,mpi_exe,
                                                                      num_tries):
#==============================================================================#
# Based on: 
# Faure, G.(1991). Principles and Applications of Inorganic Geochemistry: 
# A Comprehensive Textbook for Geology Students. New York: Macmillan Pub.Co.      
# Section 19.3, pg.395 
# "TRANSPORT OF MATTER: DIFFUSION" (Fick's Second Law)
# 
# Fick's Second Law (Eqn. 19.63)
# c = (pi * D * t)**-0.5 * e**(- x**2 / (4 * D * t)) * c_i
#
#    where, 
#
#        t is time
#        x is distance
#        c is solute concentration at a given time
#        D is diffusion coefficient
#      c_i is initial solute concentration 
# 
# Date: 02/13/18
#*******************************************************************************
  nxyz = np.zeros(3) + 1
  dxyz = np.zeros(3) + 1.
  lxyz = np.zeros(3) + 1.
  error_analysis = np.zeros(num_tries)
  dxyz_record = np.zeros((num_tries,3))
  nxyz_record = np.zeros((num_tries,3))
  test_pass = False
  try_count = 0

  # Initial Discretization
  lxyz[0] = 20.      # [m] lx
  nxyz[0] = 35       # [m] NOTE: nx must be an odd number
  dxyz = lxyz/nxyz   # [m]           
  adjust_nondim_lxyz(dxyz,lxyz,1); dxyz = lxyz/nxyz            
  pi = math.pi       # constant
  D = 1.0e-9         # [m^2/sec]
  c_i = 20.          # [M] concentration

  while (not test_pass) and (try_count < num_tries):
    print_discretization(lxyz,nxyz,dxyz)
    nx = int(nxyz[0]); ny = int(nxyz[1]); nz = int(nxyz[2])
    dx = dxyz[0]; dy = dxyz[1]; dz = dxyz[2]
    Lx = lxyz[0]; Ly = lxyz[1]; Lz = lxyz[2]
    try_count = try_count + 1
  
    x_soln = np.linspace(0.+(dx/2.),Lx-(dx/2.),nx) - Lx/2.  # [m]
    x_pflotran = np.linspace(0.+(dx/2.),Lx-(dx/2.),nx)      # [m]
    t_soln = np.array([10.,20.,40.,80.])                    # [y]
    c_soln = np.zeros((4,nx))                               # [M]
    c_pflotran = np.zeros((4,nx))                           # [M]

    vol = dx*dy*dz
    #print 'cell volume = ' + str(vol) + ' m3'

    # create the analytical solution
    for time in range(4):
      t = t_soln[time]*365.0*24.0*3600.0     # [years -> sec]
      c_soln[time,:] = \
               (vol*c_i)*pow((4*pi*D*t),-0.5)*np.exp(-pow(x_soln,2.)/(4.*D*t))

    # run PFLOTRAN simulation
    run_pflotran(input_prefix,nxyz,dxyz,lxyz,remove,screen_on,pf_exe,mpi_exe)

    index_string = 'Time:  1.00000E+01 y/Total_tracer [M]'
    c_pflotran[0,:] = read_pflotran_output_1D(path+
                '/transient_1D_IC_subsurface_transport.h5',index_string,False)
    index_string = 'Time:  2.00000E+01 y/Total_tracer [M]'
    c_pflotran[1,:] = read_pflotran_output_1D(path+
                '/transient_1D_IC_subsurface_transport.h5',index_string,False)
    index_string = 'Time:  4.00000E+01 y/Total_tracer [M]'
    c_pflotran[2,:] = read_pflotran_output_1D(path+
                '/transient_1D_IC_subsurface_transport.h5',index_string,False)
    index_string = 'Time:  8.00000E+01 y/Total_tracer [M]'
    c_pflotran[3,:] = read_pflotran_output_1D(path+
                '/transient_1D_IC_subsurface_transport.h5',index_string,remove)
    ierr = check(c_pflotran)
    
    for g in range(4):
      area_soln = 0.
      area_p_soln = 0.
      for k in range(nx):
        area_soln = area_soln + c_soln[g,k]*dx
        area_p_soln = area_p_soln + c_pflotran[g,k]*dx 
      #print 't='+str(g)+' analytical soln area = ' + str(area_soln) 
      #print 't='+str(g)+'   pflotran soln area = ' + str(area_p_soln) 

    max_percent_error = calc_relative_error(c_soln,c_pflotran,ierr)
    record_error(error_analysis,nxyz_record,dxyz_record,max_percent_error,
                 nxyz,dxyz,try_count)
    test_pass = does_pass(max_percent_error,try_count,num_tries)
    increase_nxyz(nxyz,1,2); dxyz = lxyz/nxyz
    adjust_nondim_lxyz(dxyz,lxyz,1); dxyz = lxyz/nxyz

  # Plot the PFLOTRAN and analytical solutions
  plot_1D_transient(path,t_soln,'years',x_soln,c_soln,
                    x_soln,c_pflotran,'Distance [m]',
                    'Concentration [M]',"{0:.2f}".format(max_percent_error))
  # Plot error analysis
  plot_error(error_analysis,nxyz_record,dxyz_record,path,try_count,1)
   
  # Add test result to report card
  add_to_report(path,test_pass,max_percent_error,ierr)
 
  return;

################################################################################

#==============================================================================#
def transport_transient_1D_IC_with_flow(path,input_prefix,remove,screen_on,
                                        pf_exe,mpi_exe,num_tries):
#==============================================================================#
# Based on: 
# 
# Fick's Second Law (Eqn. 19.63)
# c = (pi * D * t)**-0.5 * e**(- x**2 / (4 * D * t)) * c_i
#
#    where, 
#
#        t is time
#        x is distance
#        c is solute concentration at a given time
#        D is diffusion coefficient
#      c_i is initial solute concentration 
# 
# Date: 02/13/18
#*******************************************************************************
  nxyz = np.zeros(3) + 1
  dxyz = np.zeros(3) + 1.
  lxyz = np.zeros(3) + 1.
  error_analysis = np.zeros(num_tries)
  dxyz_record = np.zeros((num_tries,3))
  nxyz_record = np.zeros((num_tries,3))
  test_pass = False
  try_count = 0

  # Initial Discretization
  lxyz[0] = 45.      # [m] lx
  nxyz[0] = 61       # [m] NOTE: nx must be an odd number
  dxyz = lxyz/nxyz   # [m]     
  adjust_nondim_lxyz(dxyz,lxyz,1); dxyz = lxyz/nxyz                  
  pi = math.pi       # constant
  D = 1.0e-9         # [m^2/sec]
  c_i = 20.          # [M/m3] pulse concentration
  v_0 = 5.0e-9       # [m/s] uniform x-velocity

  while (not test_pass) and (try_count < num_tries):
    print_discretization(lxyz,nxyz,dxyz)
    nx = int(nxyz[0]); ny = int(nxyz[1]); nz = int(nxyz[2])
    dx = dxyz[0]; dy = dxyz[1]; dz = dxyz[2]
    Lx = lxyz[0]; Ly = lxyz[1]; Lz = lxyz[2]
    try_count = try_count + 1
  
    x_soln = np.linspace(0.+(dx/2.),Lx-(dx/2.),nx) - (Lx/2.)  # [m]
    x_pflotran = np.linspace(0.+(dx/2.),Lx-(dx/2.),nx)         # [m]
    t_soln = np.array([10.,20.,40.,80.])                       # [y]
    c_soln = np.zeros((4,nx))                                  # [M]
    c_pflotran = np.zeros((4,nx))                              # [M]

    vol = dx*dy*dz
    
    cfl = v_0 * 5.0e-2 * (365.*24.*3600.) / dx
    print 'cfl = ' + str(cfl)

    # create the analytical solution
    for time in range(4):
      t = t_soln[time]*365.0*24.0*3600.0     # [years -> sec]
      c_soln[time,:] = \
        (vol*c_i)*pow((4*pi*D*t),-0.5)*np.exp(-pow(x_soln-v_0*t,2.)/(4.*D*t))
    
    # run PFLOTRAN simulation
    run_pflotran(input_prefix,nxyz,dxyz,lxyz,remove,screen_on,pf_exe,mpi_exe)

    index_string = 'Time:  1.00000E+01 y/Total_tracer [M]'
    c_pflotran[0,:] = read_pflotran_output_1D(path+
        '/transient_1D_IC_with_flow_subsurface_transport.h5',index_string,False)
    index_string = 'Time:  2.00000E+01 y/Total_tracer [M]'
    c_pflotran[1,:] = read_pflotran_output_1D(path+
        '/transient_1D_IC_with_flow_subsurface_transport.h5',index_string,False)
    index_string = 'Time:  4.00000E+01 y/Total_tracer [M]'
    c_pflotran[2,:] = read_pflotran_output_1D(path+
        '/transient_1D_IC_with_flow_subsurface_transport.h5',index_string,False)
    index_string = 'Time:  8.00000E+01 y/Total_tracer [M]'
    c_pflotran[3,:] = read_pflotran_output_1D(path+
        '/transient_1D_IC_with_flow_subsurface_transport.h5',index_string,remove)
    ierr = check(c_pflotran)
    
    for g in range(4):
      area_soln = 0.
      area_p_soln = 0.
      for k in range(nx):
        area_soln = area_soln + c_soln[g,k]*dx
        area_p_soln = area_p_soln + c_pflotran[g,k]*dx 
      #print 't='+str(g)+' analytical soln area = ' + str(area_soln) 
      #print 't='+str(g)+'   pflotran soln area = ' + str(area_p_soln) 

    max_percent_error = calc_relative_error(c_soln,c_pflotran,ierr)
    record_error(error_analysis,nxyz_record,dxyz_record,max_percent_error,
                 nxyz,dxyz,try_count)
    test_pass = does_pass(max_percent_error,try_count,num_tries)
    increase_nxyz(nxyz,1,2); dxyz = lxyz/nxyz
    adjust_nondim_lxyz(dxyz,lxyz,1); dxyz = lxyz/nxyz

  # Plot the PFLOTRAN and analytical solutions
  plot_1D_transient(path,t_soln,'years',x_soln,c_soln,
                    x_soln,c_pflotran,'Distance [m]',
                    'Concentration [M]',"{0:.2f}".format(max_percent_error))
  # Plot error analysis
  plot_error(error_analysis,nxyz_record,dxyz_record,path,try_count,1)
   
  # Add test result to report card
  add_to_report(path,test_pass,max_percent_error,ierr)
 
  return;

################################################################################

#==============================================================================#
def transport_transient_2D_IC_with_flow(path,input_prefix,remove,screen_on,
                                        pf_exe,mpi_exe,num_tries):
#==============================================================================#
# Based on: 
# Faure, G.(1991). Principles and Applications of Inorganic Geochemistry: 
# A Comprehensive Textbook for Geology Students. New York: Macmillan Pub.Co.      
# Section 19.3, pg.395 
# "TRANSPORT OF MATTER: DIFFUSION" (Fick's Second Law)
# 
# Fick's Second Law (Eqn. 19.63)
# c = (pi * D * t)**-0.5 * e**(- x**2 / (4 * D * t)) * c_i
#
#    where, 
#
#        t is time
#        x is distance
#        c is solute concentration at a given time
#        D is diffusion coefficient
#      c_i is initial solute concentration 
# 
# Date: 02/13/18
#*******************************************************************************
  nxyz = np.zeros(3) + 1
  dxyz = np.zeros(3) + 1.
  lxyz = np.zeros(3) + 1.
  error_analysis = np.zeros(num_tries)
  dxyz_record = np.zeros((num_tries,3))
  nxyz_record = np.zeros((num_tries,3))
  test_pass = False
  try_count = 0

  # Initial Discretization
  lxyz[0] = 20.      # [m] lx
  lxyz[1] = 10.      # [m] ly
  nxyz[0] = 11       # [m] NOTE: nx should be an odd number
  nxyz[1] = 5        # [m] NOTE: ny should be an odd number
  dxyz = lxyz/nxyz   # [m]           
  adjust_nondim_lxyz(dxyz,lxyz,2); dxyz = lxyz/nxyz            
  pi = math.pi       # constant
  D = 1.0e-9         # [m^2/sec]
  c_i = 20.          # [M] concentration pulse
  v_0 = 1.0e-8       # [m/s] uniform x-velocity

  while (not test_pass) and (try_count < num_tries):
    print_discretization(lxyz,nxyz,dxyz)
    nx = int(nxyz[0]); ny = int(nxyz[1]); nz = int(nxyz[2])
    dx = dxyz[0]; dy = dxyz[1]; dz = dxyz[2]
    Lx = lxyz[0]; Ly = lxyz[1]; Lz = lxyz[2]
    try_count = try_count + 1
  
    x_soln = np.linspace(0.+(dx/2.),Lx-(dx/2.),nx)   # [m]
    y_soln = np.linspace(0.+(dy/2.),Ly-(dy/2.),ny)   # [m]
    c_soln = np.zeros((4,nx,ny))                     # [M]
    c_pflotran = np.zeros((4,nx,ny))                 # [M]
    t_soln = np.array([10.,20.,40.,80.])             # [y]

    vol = dx*dy*dz
    #print 'cell volume = ' + str(vol) + ' m3'

    # create the analytical solution
    for time in range(4):
      t = t_soln[time]*365.0*24.0*3600.0     # [years -> sec]
      for i in range(int(nx)):
        for j in range(int(ny)):
          c_soln[time,i,j] = (vol*c_i)*pow((4*pi*D*(x_soln[i]/v_0)),-0.5)*np.exp(-pow(y_soln[j],2.)/(4.*D*t))

    # run PFLOTRAN simulation
    run_pflotran(input_prefix,nxyz,dxyz,lxyz,remove,screen_on,pf_exe,mpi_exe)

    # load data from HDF5
    index_string = 'Time:  1.00000E+01 y/Total_tracer [M]'
    c_pflotran[0,:,:] = read_pflotran_output_1D(path+
        '/transient_1D_IC_with_flow_subsurface_transport.h5',index_string,False)
    index_string = 'Time:  2.00000E+01 y/Total_tracer [M]'
    c_pflotran[1,:,:] = read_pflotran_output_1D(path+
        '/transient_1D_IC_with_flow_subsurface_transport.h5',index_string,False)
    index_string = 'Time:  4.00000E+01 y/Total_tracer [M]'
    c_pflotran[2,:,:] = read_pflotran_output_1D(path+
        '/transient_1D_IC_with_flow_subsurface_transport.h5',index_string,False)
    index_string = 'Time:  8.00000E+01 y/Total_tracer [M]'
    c_pflotran[3,:,:] = read_pflotran_output_1D(path+
       '/transient_1D_IC_with_flow_subsurface_transport.h5',index_string,remove)
    ierr = check(c_pflotran)

    max_percent_error = calc_relative_error(c_soln,c_pflotran,ierr)
    record_error(error_analysis,nxyz_record,dxyz_record,max_percent_error,
                 nxyz,dxyz,try_count)
    test_pass = does_pass(max_percent_error,try_count,num_tries)
    increase_nxyz(nxyz,2,2); dxyz = lxyz/nxyz
    adjust_nondim_lxyz(dxyz,lxyz,2); dxyz = lxyz/nxyz

  # Plot the PFLOTRAN and analytical solutions
  loaded = 0
  plot_2D_transient(path,t_soln,'years',x_soln,y_soln,c_soln,c_pflotran,
        'Distance [m]','Concentration [M]',"{0:.2f}".format(max_percent_error),
        loaded)
  # Plot error analysis
  plot_error(error_analysis,nxyz_record,dxyz_record,path,try_count,2)
  
  # Add test result to report card
  add_to_report(path,test_pass,max_percent_error,ierr)
 
  return;

################################################################################

def transport_transient_1D_IC_with_flow_pe1(path,input_prefix,remove,screen_on,
                                        pf_exe,mpi_exe,num_tries):
#==============================================================================#
# Based on: 
# 
# Fick's Second Law with Advection
# c = (4 * pi * D * t)**-0.5 * e**(- (x-v*t)**2 / (4 * D * t)) * c_i
#
#    where, 
#
#        t is time
#        x is distance
#        c is solute concentration at a given time
#        D is diffusion coefficient
#      c_i is initial solute concentration 
#        v is uniform velocity
# 
# Author: Jennifer M. Frederick
# Date: 05/10/18
#*******************************************************************************
  nxyz = np.zeros(3) + 1
  dxyz = np.zeros(3) + 1.
  lxyz = np.zeros(3) + 1.
  error_analysis = np.zeros(num_tries)
  dxyz_record = np.zeros((num_tries,3))
  nxyz_record = np.zeros((num_tries,3))
  test_pass = False
  try_count = 0

  # Initial Discretization
  lxyz[0] = 45.      # [m] lx
  nxyz[0] = 61       # [m] NOTE: nx must be an odd number
  dxyz = lxyz/nxyz   # [m]                       
  adjust_nondim_lxyz(dxyz,lxyz,1); dxyz = lxyz/nxyz
  pi = math.pi       # constant
  D = 1.0e-9         # [m^2/sec]
  c_i = 20.          # [M/m3] pulse concentration
  v_0 = 5.0e-9       # [m/s] uniform x-velocity
  
  # Peclet number = 225

  while (not test_pass) and (try_count < num_tries):
    print_discretization(lxyz,nxyz,dxyz)
    nx = int(nxyz[0]); ny = int(nxyz[1]); nz = int(nxyz[2])
    dx = dxyz[0]; dy = dxyz[1]; dz = dxyz[2]
    Lx = lxyz[0]; Ly = lxyz[1]; Lz = lxyz[2]
    try_count = try_count + 1
  
    x_soln = np.linspace(0.+(dx/2.),Lx-(dx/2.),nx) - (Lx/2.)  # [m]
    x_pflotran = np.linspace(0.+(dx/2.),Lx-(dx/2.),nx)         # [m]
    t_soln = np.array([10.,20.,40.,80.])                       # [y]
    c_soln = np.zeros((4,nx))                                  # [M]
    c_pflotran = np.zeros((4,nx))                              # [M]

    vol = dx*dy*dz
    
    cfl = v_0 * 5.0e-2 * (365.*24.*3600.) / dx
    #print 'cfl = ' + str(cfl)

    # create the analytical solution
    for time in range(4):
      t = t_soln[time]*365.0*24.0*3600.0     # [years -> sec]
      c_soln[time,:] = \
        (vol*c_i)*pow((4*pi*D*t),-0.5)*np.exp(-pow(x_soln-v_0*t,2.)/(4.*D*t))
    
    # run PFLOTRAN simulation
    run_pflotran(input_prefix,nxyz,dxyz,lxyz,remove,screen_on,pf_exe,mpi_exe)

    index_string = 'Time:  1.00000E+01 y/Total_tracer [M]'
    c_pflotran[0,:] = read_pflotran_output_1D(path+
      '/transient_1D_IC_with_flow_subsurface_transport_pe1.h5',index_string,False)
    index_string = 'Time:  2.00000E+01 y/Total_tracer [M]'
    c_pflotran[1,:] = read_pflotran_output_1D(path+
      '/transient_1D_IC_with_flow_subsurface_transport_pe1.h5',index_string,False)
    index_string = 'Time:  4.00000E+01 y/Total_tracer [M]'
    c_pflotran[2,:] = read_pflotran_output_1D(path+
      '/transient_1D_IC_with_flow_subsurface_transport_pe1.h5',index_string,False)
    index_string = 'Time:  8.00000E+01 y/Total_tracer [M]'
    c_pflotran[3,:] = read_pflotran_output_1D(path+
      '/transient_1D_IC_with_flow_subsurface_transport_pe1.h5',index_string,remove)
    ierr = check(c_pflotran)
    
    for g in range(4):
      area_soln = 0.
      area_p_soln = 0.
      for k in range(nx):
        area_soln = area_soln + c_soln[g,k]*dx
        area_p_soln = area_p_soln + c_pflotran[g,k]*dx 
      #print 't='+str(g)+' analytical soln area = ' + str(area_soln) 
      #print 't='+str(g)+'   pflotran soln area = ' + str(area_p_soln) 

    max_percent_error = calc_relative_error(c_soln,c_pflotran,ierr)
    record_error(error_analysis,nxyz_record,dxyz_record,max_percent_error,
                 nxyz,dxyz,try_count)
    test_pass = does_pass(max_percent_error,try_count,num_tries)
    increase_nxyz(nxyz,1,2); dxyz = lxyz/nxyz
    adjust_nondim_lxyz(dxyz,lxyz,1); dxyz = lxyz/nxyz

  # Plot the PFLOTRAN and analytical solutions
  plot_1D_transient(path,t_soln,'years',x_soln,c_soln,
                    x_soln,c_pflotran,'Distance [m]',
                    'Concentration [M]',"{0:.2f}".format(max_percent_error))
  # Plot error analysis
  plot_error(error_analysis,nxyz_record,dxyz_record,path,try_count,1)
   
  # Add test result to report card
  add_to_report(path,test_pass,max_percent_error,ierr)
 
  return;

################################################################################

#==============================================================================#
def transport_transient_1D_IC_with_flow_pe2(path,input_prefix,remove,screen_on,
                                        pf_exe,mpi_exe,num_tries):
#==============================================================================#
# Based on: 
# 
# Fick's Second Law with Advection
# c = (4 * pi * D * t)**-0.5 * e**(- (x-v*t)**2 / (4 * D * t)) * c_i
#
#    where, 
#
#        t is time
#        x is distance
#        c is solute concentration at a given time
#        D is diffusion coefficient
#      c_i is initial solute concentration 
#        v is uniform velocity
# 
# Author: Jennifer M. Frederick
# Date: 05/10/18
#*******************************************************************************
  nxyz = np.zeros(3) + 1
  dxyz = np.zeros(3) + 1.
  lxyz = np.zeros(3) + 1.
  error_analysis = np.zeros(num_tries)
  dxyz_record = np.zeros((num_tries,3))
  nxyz_record = np.zeros((num_tries,3))
  test_pass = False
  try_count = 0

  # Initial Discretization
  lxyz[0] = 100.     # [m] lx
  nxyz[0] = 61       # [m] NOTE: nx must be an odd number
  dxyz = lxyz/nxyz   # [m]           
  adjust_nondim_lxyz(dxyz,lxyz,1); dxyz = lxyz/nxyz            
  pi = math.pi       # constant
  D = 1.0e-7         # [m^2/sec]
  c_i = 20.          # [M/m3] pulse concentration
  v_0 = 5.0e-8       # [m/s] uniform x-velocity
  
  # Peclet number = 32.5

  while (not test_pass) and (try_count < num_tries):
    print_discretization(lxyz,nxyz,dxyz)
    nx = int(nxyz[0]); ny = int(nxyz[1]); nz = int(nxyz[2])
    dx = dxyz[0]; dy = dxyz[1]; dz = dxyz[2]
    Lx = lxyz[0]; Ly = lxyz[1]; Lz = lxyz[2]
    try_count = try_count + 1
  
    x_soln = np.linspace(0.+(dx/2.),Lx-(dx/2.),nx) - (Lx/2.)  # [m]
    x_pflotran = np.linspace(0.+(dx/2.),Lx-(dx/2.),nx)        # [m]
    t_soln = np.array([1.,4.,8.,12.])                         # [y]
    c_soln = np.zeros((4,nx))                                 # [M]
    c_pflotran = np.zeros((4,nx))                             # [M]

    vol = dx*dy*dz
    
    cfl = v_0 * 5.0e-2 * (365.*24.*3600.) / dx
    #print 'cfl = ' + str(cfl)

    # create the analytical solution
    for time in range(4):
      t = t_soln[time]*365.0*24.0*3600.0     # [years -> sec]
      c_soln[time,:] = \
        (c_i)*pow((4*pi*D*t),-0.5)*np.exp(-pow(x_soln-v_0*t,2.)/(4.*D*t))
    
    # run PFLOTRAN simulation
    run_pflotran(input_prefix,nxyz,dxyz,lxyz,remove,screen_on,pf_exe,mpi_exe)

    index_string = 'Time:  1.00000E+00 y/Total_tracer [M]'
    c_pflotran[0,:] = read_pflotran_output_1D(path+
      '/transient_1D_IC_with_flow_subsurface_transport_pe2.h5',index_string,False)
    index_string = 'Time:  4.00000E+00 y/Total_tracer [M]'
    c_pflotran[1,:] = read_pflotran_output_1D(path+
      '/transient_1D_IC_with_flow_subsurface_transport_pe2.h5',index_string,False)
    index_string = 'Time:  8.00000E+00 y/Total_tracer [M]'
    c_pflotran[2,:] = read_pflotran_output_1D(path+
      '/transient_1D_IC_with_flow_subsurface_transport_pe2.h5',index_string,False)
    index_string = 'Time:  1.20000E+01 y/Total_tracer [M]'
    c_pflotran[3,:] = read_pflotran_output_1D(path+
      '/transient_1D_IC_with_flow_subsurface_transport_pe2.h5',index_string,remove)
    ierr = check(c_pflotran)
    
    for g in range(4):
      area_soln = 0.
      area_p_soln = 0.
      for k in range(nx):
        area_soln = area_soln + c_soln[g,k]*dx
        area_p_soln = area_p_soln + c_pflotran[g,k]*dx 
      #print 't='+str(g)+' analytical soln area = ' + str(area_soln) 
      #print 't='+str(g)+'   pflotran soln area = ' + str(area_p_soln) 

    max_percent_error = calc_relative_error(c_soln,c_pflotran,ierr)
    record_error(error_analysis,nxyz_record,dxyz_record,max_percent_error,
                 nxyz,dxyz,try_count)
    test_pass = does_pass(max_percent_error,try_count,num_tries)
    increase_nxyz(nxyz,1,2); dxyz = lxyz/nxyz
    adjust_nondim_lxyz(dxyz,lxyz,1); dxyz = lxyz/nxyz

  # Plot the PFLOTRAN and analytical solutions
  plot_1D_transient(path,t_soln,'years',x_soln,c_soln,
                    x_soln,c_pflotran,'Distance [m]',
                    'Concentration [M]',"{0:.2f}".format(max_percent_error))
  # Plot error analysis
  plot_error(error_analysis,nxyz_record,dxyz_record,path,try_count,1)
   
  # Add test result to report card
  add_to_report(path,test_pass,max_percent_error,ierr)
 
  return;

################################################################################

#==============================================================================#
def transport_transient_1D_IC_with_flow_pe3(path,input_prefix,remove,screen_on,
                                        pf_exe,mpi_exe,num_tries):
#==============================================================================#
# Based on: 
# 
# Fick's Second Law with Advection
# c = (4 * pi * D * t)**-0.5 * e**(- (x-v*t)**2 / (4 * D * t)) * c_i
#
#    where, 
#
#        t is time
#        x is distance
#        c is solute concentration at a given time
#        D is diffusion coefficient
#      c_i is initial solute concentration 
#        v is uniform velocity
# 
# Author: Jennifer M. Frederick
# Date: 05/10/18
#*******************************************************************************
  nxyz = np.zeros(3) + 1
  dxyz = np.zeros(3) + 1.
  lxyz = np.zeros(3) + 1.
  error_analysis = np.zeros(num_tries)
  dxyz_record = np.zeros((num_tries,3))
  nxyz_record = np.zeros((num_tries,3))
  test_pass = False
  try_count = 0

  # Initial Discretization
  lxyz[0] = 10000.   # [m] lx
  nxyz[0] = 1601     # [m] NOTE: nx must be an odd number
  dxyz = lxyz/nxyz   # [m]      
  adjust_nondim_lxyz(dxyz,lxyz,1); dxyz = lxyz/nxyz                 
  pi = math.pi       # constant
  D = 5.0e-6         # [m^2/sec]
  c_i = 20.          # [M/m3] pulse concentration
  v_0 = 8.0e-7       # [m/s] uniform x-velocity
  
  # Peclet number = 90,000

  while (not test_pass) and (try_count < num_tries):
    print_discretization(lxyz,nxyz,dxyz)
    nx = int(nxyz[0]); ny = int(nxyz[1]); nz = int(nxyz[2])
    dx = dxyz[0]; dy = dxyz[1]; dz = dxyz[2]
    Lx = lxyz[0]; Ly = lxyz[1]; Lz = lxyz[2]
    try_count = try_count + 1
  
    x_soln = np.linspace(0.+(dx/2.),Lx-(dx/2.),nx) - (Lx/2.)  # [m]
    x_pflotran = np.linspace(0.+(dx/2.),Lx-(dx/2.),nx)        # [m]
    t_soln = np.array([30.,60.,90.,120.])                     # [y]
    c_soln = np.zeros((4,nx))                                 # [M]
    c_pflotran = np.zeros((4,nx))                             # [M]

    vol = dx*dy*dz
    
    cfl = v_0 * 5.0e-2 * (365.*24.*3600.) / dx
    print 'cfl = ' + str(cfl)

    # create the analytical solution
    for time in range(4):
      t = t_soln[time]*365.0*24.0*3600.0     # [years -> sec]
      c_soln[time,:] = \
        (c_i)*pow((4*pi*D*t),-0.5)*np.exp(-pow(x_soln-v_0*t,2.)/(4.*D*t))
    
    # run PFLOTRAN simulation
    run_pflotran(input_prefix,nxyz,dxyz,lxyz,remove,screen_on,pf_exe,mpi_exe)

    index_string = 'Time:  3.00000E+01 y/Total_tracer [M]'
    c_pflotran[0,:] = read_pflotran_output_1D(path+
      '/transient_1D_IC_with_flow_subsurface_transport_pe3.h5',index_string,False)
    index_string = 'Time:  6.00000E+01 y/Total_tracer [M]'
    c_pflotran[1,:] = read_pflotran_output_1D(path+
      '/transient_1D_IC_with_flow_subsurface_transport_pe3.h5',index_string,False)
    index_string = 'Time:  9.00000E+01 y/Total_tracer [M]'
    c_pflotran[2,:] = read_pflotran_output_1D(path+
      '/transient_1D_IC_with_flow_subsurface_transport_pe3.h5',index_string,False)
    index_string = 'Time:  1.20000E+02 y/Total_tracer [M]'
    c_pflotran[3,:] = read_pflotran_output_1D(path+
      '/transient_1D_IC_with_flow_subsurface_transport_pe3.h5',index_string,remove)
    ierr = check(c_pflotran)
    
    for g in range(4):
      area_soln = 0.
      area_p_soln = 0.
      for k in range(nx):
        area_soln = area_soln + c_soln[g,k]*dx
        area_p_soln = area_p_soln + c_pflotran[g,k]*dx 
      print 't='+str(g)+' analytical soln area = ' + str(area_soln) 
      print 't='+str(g)+'   pflotran soln area = ' + str(area_p_soln) 

    max_percent_error = calc_relative_error(c_soln,c_pflotran,ierr)
    record_error(error_analysis,nxyz_record,dxyz_record,max_percent_error,
                 nxyz,dxyz,try_count)
    test_pass = does_pass(max_percent_error,try_count,num_tries)
    increase_nxyz(nxyz,1,2); dxyz = lxyz/nxyz
    adjust_nondim_lxyz(dxyz,lxyz,1); dxyz = lxyz/nxyz

  # Plot the PFLOTRAN and analytical solutions
  plot_1D_transient(path,t_soln,'years',x_soln,c_soln,
                    x_soln,c_pflotran,'Distance [m]',
                    'Concentration [M]',"{0:.2f}".format(max_percent_error))
  # Plot error analysis
  plot_error(error_analysis,nxyz_record,dxyz_record,path,try_count,1)
   
  # Add test result to report card
  add_to_report(path,test_pass,max_percent_error,ierr)
 
  return;

################################################################################

#==============================================================================#
def transport_transient_2D_IC_with_flow(path,input_prefix,remove,screen_on,
                                        pf_exe,mpi_exe,num_tries):
#==============================================================================#
# Based on: 
# Faure, G.(1991). Principles and Applications of Inorganic Geochemistry: 
# A Comprehensive Textbook for Geology Students. New York: Macmillan Pub.Co.      
# Section 19.3, pg.395 
# "TRANSPORT OF MATTER: DIFFUSION" (Fick's Second Law)
# 
# Fick's Second Law (Eqn. 19.63)
# c = (pi * D * t)**-0.5 * e**(- x**2 / (4 * D * t)) * c_i
#
#    where, 
#
#        t is time
#        x is distance
#        c is solute concentration at a given time
#        D is diffusion coefficient
#      c_i is initial solute concentration 
# 
# Date: 02/13/18
#*******************************************************************************
  nxyz = np.zeros(3) + 1
  dxyz = np.zeros(3) + 1.
  lxyz = np.zeros(3) + 1.
  error_analysis = np.zeros(num_tries)
  dxyz_record = np.zeros((num_tries,3))
  nxyz_record = np.zeros((num_tries,3))
  test_pass = False
  try_count = 0

  # Initial Discretization
  lxyz[0] = 20.      # [m] lx
  lxyz[1] = 10.      # [m] ly
  nxyz[0] = 11       # [m] NOTE: nx should be an odd number
  nxyz[1] = 5        # [m] NOTE: ny should be an odd number
  dxyz = lxyz/nxyz   # [m]              
  adjust_nondim_lxyz(dxyz,lxyz,2); dxyz = lxyz/nxyz         
  pi = math.pi       # constant
  D = 1.0e-9         # [m^2/sec]
  c_i = 20.          # [M] concentration pulse
  v_0 = 1.0e-8       # [m/s] uniform x-velocity

  while (not test_pass) and (try_count < num_tries):
    print_discretization(lxyz,nxyz,dxyz)
    nx = int(nxyz[0]); ny = int(nxyz[1]); nz = int(nxyz[2])
    dx = dxyz[0]; dy = dxyz[1]; dz = dxyz[2]
    Lx = lxyz[0]; Ly = lxyz[1]; Lz = lxyz[2]
    try_count = try_count + 1
  
    x_soln = np.linspace(0.+(dx/2.),Lx-(dx/2.),nx)   # [m]
    y_soln = np.linspace(0.+(dy/2.),Ly-(dy/2.),ny)   # [m]
    c_soln = np.zeros((4,nx,ny))                     # [M]
    c_pflotran = np.zeros((4,nx,ny))                 # [M]
    t_soln = np.array([10.,20.,40.,80.])             # [y]

    vol = dx*dy*dz
    #print 'cell volume = ' + str(vol) + ' m3'

    # create the analytical solution
    for time in range(4):
      t = t_soln[time]*365.0*24.0*3600.0     # [years -> sec]
      for i in range(int(nx)):
        for j in range(int(ny)):
          c_soln[time,i,j] = (vol*c_i)*pow((4*pi*D*(x_soln[i]/v_0)),-0.5)*np.exp(-pow(y_soln[j],2.)/(4.*D*t))

    # run PFLOTRAN simulation
    run_pflotran(input_prefix,nxyz,dxyz,lxyz,remove,screen_on,pf_exe,mpi_exe)

    # load data from HDF5
    index_string = 'Time:  1.00000E+01 y/Total_tracer [M]'
    c_pflotran[0,:,:] = read_pflotran_output_1D(path+
        '/transient_1D_IC_with_flow_subsurface_transport.h5',index_string,False)
    index_string = 'Time:  2.00000E+01 y/Total_tracer [M]'
    c_pflotran[1,:,:] = read_pflotran_output_1D(path+
        '/transient_1D_IC_with_flow_subsurface_transport.h5',index_string,False)
    index_string = 'Time:  4.00000E+01 y/Total_tracer [M]'
    c_pflotran[2,:,:] = read_pflotran_output_1D(path+
        '/transient_1D_IC_with_flow_subsurface_transport.h5',index_string,False)
    index_string = 'Time:  8.00000E+01 y/Total_tracer [M]'
    c_pflotran[3,:,:] = read_pflotran_output_1D(path+
       '/transient_1D_IC_with_flow_subsurface_transport.h5',index_string,remove)
    ierr = check(c_pflotran)

    max_percent_error = calc_relative_error(c_soln,c_pflotran,ierr)
    record_error(error_analysis,nxyz_record,dxyz_record,max_percent_error,
                 nxyz,dxyz,try_count)
    test_pass = does_pass(max_percent_error,try_count,num_tries)
    increase_nxyz(nxyz,2,2); dxyz = lxyz/nxyz
    adjust_nondim_lxyz(dxyz,lxyz,2); dxyz = lxyz/nxyz

  # Plot the PFLOTRAN and analytical solutions
  loaded = 0
  plot_2D_transient(path,t_soln,'years',x_soln,y_soln,c_soln,c_pflotran,
        'Distance [m]','Concentration [M]',"{0:.2f}".format(max_percent_error),
        loaded)
  # Plot error analysis
  plot_error(error_analysis,nxyz_record,dxyz_record,path,try_count,2)
  
  # Add test result to report card
  add_to_report(path,test_pass,max_percent_error,ierr)
 
  return;

################################################################################

################################################################################
#### TWO-PHASE TESTS ###########################################################
################################################################################

#==============================================================================#
def BL_1D_GENERAL_IMMISC_GAS_INJ(path,input_prefix,remove,screen_on,pf_exe,
				                             mpi_exe,num_tries):
#==============================================================================#
# The classic Buckley-Leverett solution as presented in:
# "Theory of gas injection processes"
# F. M Orr, Jr
# Tie-Line publications, Copenhagen Denmark
#  or
# "Enhanced oil recovery"
# Larry W. Lake
# Prentice Hall, Upper Saddle River, NJ
# 1989
# Author: Tara LaForce
# Date: 09/20/2018
# ******************************************************************************
  nxyz = np.zeros(3) + 1
  dxyz = np.zeros(3) + 1.
  lxyz = np.zeros(3) + 1.
  error_analysis = np.zeros(num_tries)
  dxyz_record = np.zeros((num_tries,3))
  nxyz_record = np.zeros((num_tries,3))
  test_pass = False
  try_count = 0

  # initial discretization values
  lxyz[0] = 10.              # [m] lx
  nxyz[0] = 100               # [m] nx
  dxyz = lxyz/nxyz           # [m]
  ierr = 0

  # problem parameters
  # fluid parameters
  lam = 0.8 #lambda in the BURDINE_BC liquid and gas relative permeabilities (-)
  Srw = 0.1 #residual liquid sauration in the liquid relative permeability (-)
  Srg = 0.1 #residual gas saturation in the gas relative permeability (-)
  rho_w = 1.0e3 #water density (kg/m^3)
  mu_w = 1.0e-3 #water viscosity (Pa-s) 
  rho_mol_g = 4.0876e-2 #molar gas density of CO2 at 20C and 101325 Pa (kmol/m^3)
  mu_g = 1.61e-5 #gas viscosity (Pa-s)
  #calculate mass-density of CO2
  rho_g = 28.9598*rho_mol_g
  #solid parameters
  domain_m = 10.0
  A = 1.0 #cross sectional area of the domain (m^2)
  phi = 0.25 #porosity (-)
  q_mass = 2e-5 #gas mass injection rate  (g/s)
  IC = 1.0 #water saturation must be 100%
  #some tolerances
  delta = 1e-7 #step size for approximating derivatives
  conv_tol = 1e-4 #tolerance for convergencce of iterative processes
  #solution times
  t_yr = np.array([0.01,0.10,0.50,1.0])                     # years
  #for calculating dimenssionless variables
  q_vol = 1.0e-3*q_mass/rho_g #volumetric injection rate (m^3/s)
  xD_to_xm = q_vol/(phi*A) #to convert from dimensionless distance to x in m and vice versa

  while (not test_pass) and (try_count < num_tries): 
    print_discretization(lxyz,nxyz,dxyz)
    nx = int(nxyz[0]); ny = int(nxyz[1]); nz = int(nxyz[2])
    dx = dxyz[0]; dy = dxyz[1]; dz = dxyz[2]
    Lx = lxyz[0]; Ly = lxyz[1]; Lz = lxyz[2]
    try_count = try_count + 1

    x_soln = np.linspace(0.+(dx/2.),Lx-(dx/2.),nx)  # [m]
    Sw_soln = np.zeros((4,(nx)))                               # [-]
    x_pflotran = x_soln[0:nx]                       # [m]
    Sw_pflotran = np.zeros((4,nx))                                     # [-]

    #a lot of this is redundant and needs to be rationalized
    len_x = x_soln.size
    
    len_t = t_yr.size
    t_sec = np.zeros(len_t)

    fw_xD = np.zeros((len_t,len_x))
    dfw_dSw_xD = np.zeros((len_t,len_x))
    vel_x = np.zeros((len_t,len_x))


    ##############################################################################      
    # create the analytical solution

    ######################################################################
    # Subfunctions
    ######################################################################
    def find_fw(Sw):
      Sel = max(0,(Sw-Srw)/(1-Srw))
      Seg = min(1,max(0,(Sw-Srw)/(1-Srw-Srg)))
      krg = (1-Seg)**2*(1-Seg**(1+2/lam))
      krw = Sel**(3+2/lam)
      Mg = krg/mu_g
      Mw = krw/mu_w
      fw = Mw/(Mw + Mg)
      return fw

    def find_dfw_dSw(Sw):
      Sw_plus = Sw + delta
      fw_plus = find_fw(Sw_plus)
      Sw_minus = Sw - delta
      fw_minus = find_fw(Sw_minus)
      dfw_dSw = (fw_plus-fw_minus)/(Sw_plus-Sw_minus)
      return dfw_dSw

    def find_shock(Sw,fw,IC):
      if Sw == IC:
        v_shock = 0.0
      else:
        v_shock = (IC-fw)/(IC-Sw)
        return v_shock

    def find_tan(Sw,IC):
      fw = find_fw(Sw)
      dfw_dSw = find_dfw_dSw(Sw)
      v_shock = find_shock(Sw,fw,IC)
      diff = ((dfw_dSw - v_shock)**2)**0.5
      return diff

    def find_Sw_xD(Sw,vel):
      dfw_dSw = find_dfw_dSw(Sw)
      diff = ((dfw_dSw - vel)**2)**0.5
      return diff
  
    for time in range(4): 
      t = t_yr[time]*(24.0*3600.0)   # [sec]
      Sw_soln[time,:] = 0.0
      Sw_soln[time,:] = Sw_soln[time,:] + 1.0

    #####################################################################
    # Main program
    ######################################################################

    ######################################################################
    #this section finds the leading tangent shock

    #first get an approximate of the upper and lower bounds on water saturation at the shock
    Sw_now = np.linspace(0.0,1.0,101,True,False)
    len_Sw =  Sw_now.size
    
    dfw_dSw = np.zeros(len_Sw)
    
    for i in range(len_Sw):
      dfw_dSw[i] = find_dfw_dSw(Sw_now[i])
      #the correct tangent shock must be between where dfw_dSw=1 and the maximum of dfw_dSw
      if i > 0:
        if dfw_dSw[i] > 1.0 and dfw_dSw[i-1] < 1.0:
          pos_tan_S_index = i-1
          pos_tan_Sw = Sw_now[i-1]

    #find the approximate maximum of dw_dSw
    max_dfw_dSw = np.amax(dfw_dSw)
    max_v_index = np.argmax(dfw_dSw)
    Sw_max_v = Sw_now[max_v_index]

    #set the upper and lower bounds on Sw to put into the iterative solver 
    Sw_bdry = [(pos_tan_Sw,Sw_max_v)]
    #the midpoint of the interval is a good first guess
    Sw_guess = (pos_tan_Sw+Sw_max_v)/2.0
    #scipy solver to find the tangent shock where (fw-IC)/(Sw-IC)=dfw_dSw
    Sw_tan_info = minimize(find_tan,Sw_guess,IC,method='L-BFGS-B',bounds=Sw_bdry,tol=conv_tol)

    #split output into a usable form and sanity check
    Sw_tan = Sw_tan_info.x
    fw_tan = find_fw(Sw_tan)
    dfw_dSw_tan = find_dfw_dSw(Sw_tan)
    v_shock_tan = find_shock(Sw_tan,fw_tan,IC)
    if abs(dfw_dSw_tan - v_shock_tan) > 1e-3:
      print 'error! the shock solver did not converge!'
      sys.exit(0)

    ######################################################################
    # finally finding Sw on the grid for each timestep

    #all Sw must be between the Swr and the saturation at the tangent shock
    Sw_xD_bdry = [(Srw,Sw_tan)]
    #the midpoint is a good first guess
    Sw_xD_guess = (Srw+Sw_tan)/2.0

    for k in range(len_t):
      t_sec[k] = 3600*24*365.25*t_yr[k] #simulation time (sec)
      vel_x[k] = 1/(t_sec[k]*xD_to_xm)*x_soln #the dimensionless velocities corresponding to the output time and mesh

      for i in range(len_x):
        if vel_x[k,i] < v_shock_tan:
          # scipy solver to find the Sw corresponding to the dimensionless velocity behind the shock front
          Sw_xD_info = minimize(find_Sw_xD,Sw_xD_guess,vel_x[k,i],method='L-BFGS-B',bounds=Sw_xD_bdry,tol=conv_tol)
          #split output into a usable form and sanity check
          Sw_soln[k,i] = Sw_xD_info.x
          dfw_dSw_xD[k,i] = find_dfw_dSw(Sw_soln[k,i])
          err = dfw_dSw_xD[k,i] - vel_x[k,i]
          if abs(err) > 1e-3:
            print 'error! the solver for Sw_xD did not converge!'
            print Sw_soln[k,i]
            print vel_x[k,i]
            print err
            sys.exit(0)
        else: 
          # for velocities faster than the shock the saturation is the initial composition
          Sw_soln[k,i] = IC
          fw_xD[k,i] = IC
          dfw_dSw_xD[k,i] = vel_x[k,i]


    ##############################################################################      
    # run PFLOTRAN simulation
    
    run_pflotran(input_prefix,nxyz,dxyz,lxyz,remove,screen_on,pf_exe,mpi_exe)
    
    # load data from HDF5
    index_string = 'Time:  1.00000E-02 y/Liquid_Saturation'
    Sw_pflotran[0,:] = read_pflotran_output_1D(path+
		      '/1D_BL_GENERAL_IMMISC_GAS_INJ.h5',index_string,False)
    index_string = 'Time:  1.00000E-01 y/Liquid_Saturation'
    Sw_pflotran[1,:] = read_pflotran_output_1D(path+
		      '/1D_BL_GENERAL_IMMISC_GAS_INJ.h5',index_string,False)
    index_string = 'Time:  5.00000E-01 y/Liquid_Saturation'
    Sw_pflotran[2,:] = read_pflotran_output_1D(path+
		      '/1D_BL_GENERAL_IMMISC_GAS_INJ.h5',index_string,False)
    index_string = 'Time:  1.00000E+00 y/Liquid_Saturation'
    Sw_pflotran[3,:] = read_pflotran_output_1D(path+
		      '/1D_BL_GENERAL_IMMISC_GAS_INJ.h5',index_string,remove)
    ierr = check(Sw_pflotran)

    L2_percent_error = calc_L2_relative_error(Sw_soln[3,:],Sw_pflotran[3,:],ierr)
    record_error(error_analysis,nxyz_record,dxyz_record,L2_percent_error,
                 nxyz,dxyz,try_count)

    test_pass = does_pass(L2_percent_error,try_count,num_tries)
    nxyz[0] = nxyz[0]*2.
    dxyz = lxyz/nxyz

  # Plot the PFLOTRAN and analytical solutions
  plot_1D_transient(path,t_yr,'yr',x_soln,Sw_soln,x_pflotran,Sw_pflotran,
                    'Distance [m]','Water Saturation [C]',"{0:.2f}".format(L2_percent_error))
  
  # Plot error analysis
  plot_error(error_analysis,nxyz_record,dxyz_record,path,try_count,1)
  
  # Add test result to report card
  add_to_report(path,test_pass,L2_percent_error,ierr)

  return;
  


#==============================================================================#
def BL_1D_RAD_GENERAL_IMMISC_GAS_INJ(path,input_prefix,remove,screen_on,pf_exe,
				                             mpi_exe,num_tries):
#==============================================================================#
# The classic Buckley-Leverett solution as presented in:
# "Theory of gas injection processes"
# F. M Orr, Jr
# Tie-Line publications, Copenhagen Denmark
#  or
# "Enhanced oil recovery"
# Larry W. Lake
# Prentice Hall, Upper Saddle River, NJ
# 1989
# Radial coordinate transform is available in:
# Analytical Solution of Nonisothermal Buckley-Leverett Flow Including Tracers
# Dindoruk and Dindoruk
# SPE Journal, 2008
# doi: 10.2118/102266-PA

# Author: Tara LaForce
# Date: 09/20/2018
# ******************************************************************************
  nxyz = np.zeros(3) + 1
  dxyz = np.zeros(3) + 1.
  lxyz = np.zeros(3) + 1.
  error_analysis = np.zeros(num_tries)
  dxyz_record = np.zeros((num_tries,3))
  nxyz_record = np.zeros((num_tries,3))
  test_pass = False
  try_count = 0

  # initial discretization values
  lxyz[0] = 10.              # [m] lx
  nxyz[0] = 25               # [m] nx
  dxyz = lxyz/nxyz           # [m]
  ierr = 0

  # problem parameters
  # fluid parameters
  lam = 0.8 #lambda in the BURDINE_BC liquid and gas relative permeabilities (-)
  Srw = 0.1 #residual liquid sauration in the liquid relative permeability (-)
  Srg = 0.1 #residual gas saturation in the gas relative permeability (-)
  rho_w = 1.0e3 #water density (kg/m^3)
  mu_w = 1.0e-3 #water viscosity (Pa-s) 
  rho_mol_g = 4.0876e-2 #molar gas density of CO2 at 20C and 101325 Pa (kmol/m^3)
  mu_g = 1.61e-5 #gas viscosity (Pa-s)
  #calculate mass-density of CO2
  rho_g = 28.9598*rho_mol_g
  #solid parameters
  domain_m = 10.0
  h = 1.0 #height of the domain (m)
  phi = 0.25 #porosity (-)
  q_mass = 2e-4 #gas mass injection rate  (g/s)
  IC = 1.0 #water saturation must be 100%
  #some tolerances
  delta = 1e-7 #step size for approximating derivatives
  conv_tol = 1e-4 #tolerance for convergencce of iterative processes
  #solution times
  t_yr = np.array([0.01,0.10,0.50,1.0])                     # years
  #for calculating dimenssionless variables
  q_vol = 1.0e-3*q_mass/rho_g #volumetric injection rate (m^3/s)
  rD_to_rm = q_vol/(phi*h*math.pi) #to convert from dimensionless distance to x in m and vice versa

  while (not test_pass) and (try_count < num_tries): 
    print_discretization(lxyz,nxyz,dxyz)
    nx = int(nxyz[0]); ny = int(nxyz[1]); nz = int(nxyz[2])
    dx = dxyz[0]; dy = dxyz[1]; dz = dxyz[2]
    Lx = lxyz[0]; Ly = lxyz[1]; Lz = lxyz[2]
    try_count = try_count + 1

    r_soln = np.linspace(0.+(dx/2.),Lx-(dx/2.),nx)  # [m]
    r_edges = np.linspace(0.,Lx,nx+1)  # [m]
    Sw_soln = np.zeros((4,(nx)))                               # [-]
    x_pflotran = r_soln[0:nx]                       # [m]
    Sw_pflotran = np.zeros((4,nx))                                     # [-]

    #a lot of this is redundant and needs to be rationalized
    len_x = r_soln.size
    
    len_t = t_yr.size
    t_sec = np.zeros(len_t)

    fw_xD = np.zeros((len_t,len_x))
    dfw_dSw_xD = np.zeros((len_t,len_x))
    vel_x = np.zeros((len_t,len_x))


    ##############################################################################      
    # create the analytical solution

    ######################################################################
    # Subfunctions
    ######################################################################
    def find_fw(Sw):
      Sel = max(0,(Sw-Srw)/(1-Srw))
      Seg = min(1,max(0,(Sw-Srw)/(1-Srw-Srg)))
      krg = (1-Seg)**2*(1-Seg**(1+2/lam))
      krw = Sel**(3+2/lam)
      Mg = krg/mu_g
      Mw = krw/mu_w
      fw = Mw/(Mw + Mg)
      return fw

    def find_dfw_dSw(Sw):
      Sw_plus = Sw + delta
      fw_plus = find_fw(Sw_plus)
      Sw_minus = Sw - delta
      fw_minus = find_fw(Sw_minus)
      dfw_dSw = (fw_plus-fw_minus)/(Sw_plus-Sw_minus)
      return dfw_dSw

    def find_shock(Sw,fw,IC):
      if Sw == IC:
        v_shock = 0.0
      else:
        v_shock = (IC-fw)/(IC-Sw)
        return v_shock

    def find_tan(Sw,IC):
      fw = find_fw(Sw)
      dfw_dSw = find_dfw_dSw(Sw)
      v_shock = find_shock(Sw,fw,IC)
      diff = ((dfw_dSw - v_shock)**2)**0.5
      return diff

    def find_Sw_xD(Sw,vel):
      dfw_dSw = find_dfw_dSw(Sw)
      diff = ((dfw_dSw - vel)**2)**0.5
      return diff
  
    for time in range(4): 
      t = t_yr[time]*(24.0*3600.0)   # [sec]
      Sw_soln[time,:] = 0.0
      Sw_soln[time,:] = Sw_soln[time,:] + 1.0

    #####################################################################
    # Main program
    ######################################################################

    ######################################################################
    #this section finds the leading tangent shock

    #first get an approximate of the upper and lower bounds on water saturation at the shock
    Sw_now = np.linspace(0.0,1.0,101,True,False)
    len_Sw =  Sw_now.size
    
    dfw_dSw = np.zeros(len_Sw)
    
    for i in range(len_Sw):
      dfw_dSw[i] = find_dfw_dSw(Sw_now[i])
      #the correct tangent shock must be between where dfw_dSw=1 and the maximum of dfw_dSw
      if i > 0:
        if dfw_dSw[i] > 1.0 and dfw_dSw[i-1] < 1.0:
          pos_tan_S_index = i-1
          pos_tan_Sw = Sw_now[i-1]

    #find the approximate maximum of dw_dSw
    max_dfw_dSw = np.amax(dfw_dSw)
    max_v_index = np.argmax(dfw_dSw)
    Sw_max_v = Sw_now[max_v_index]

    #set the upper and lower bounds on Sw to put into the iterative solver 
    Sw_bdry = [(pos_tan_Sw,Sw_max_v)]
    #the midpoint of the interval is a good first guess
    Sw_guess = (pos_tan_Sw+Sw_max_v)/2.0
    #scipy solver to find the tangent shock where (fw-IC)/(Sw-IC)=dfw_dSw
    Sw_tan_info = minimize(find_tan,Sw_guess,IC,method='L-BFGS-B',bounds=Sw_bdry,tol=conv_tol)

    #split output into a usable form and sanity check
    Sw_tan = Sw_tan_info.x
    fw_tan = find_fw(Sw_tan)
    dfw_dSw_tan = find_dfw_dSw(Sw_tan)
    v_shock_tan = find_shock(Sw_tan,fw_tan,IC)
    if abs(dfw_dSw_tan - v_shock_tan) > 1e-3:
      print 'error! the shock solver did not converge!'
      sys.exit(0)

    ######################################################################
    # finally finding Sw on the grid for each timestep

    #all Sw must be between the Swr and the saturation at the tangent shock
    Sw_xD_bdry = [(Srw,Sw_tan)]
    #the midpoint is a good first guess
    Sw_xD_guess = (Srw+Sw_tan)/2.0

    for k in range(len_t):
      t_sec[k] = 3600*24*365.25*t_yr[k] #simulation time (sec)
      vel_x[k,:] = 1/(t_sec[k]*rD_to_rm)*np.power(r_soln,2.0) 

      for i in range(len_x):
        if vel_x[k,i] < v_shock_tan:
          # scipy solver to find the Sw corresponding to the dimensionless velocity behind the shock front
          Sw_xD_info = minimize(find_Sw_xD,Sw_xD_guess,vel_x[k,i],method='L-BFGS-B',bounds=Sw_xD_bdry,tol=conv_tol)
          #split output into a usable form and sanity check
          Sw_soln[k,i] = Sw_xD_info.x
          dfw_dSw_xD[k,i] = find_dfw_dSw(Sw_soln[k,i])
          err = dfw_dSw_xD[k,i] - vel_x[k,i]
          if abs(err) > 1e-3:
            print 'error! the solver for Sw_xD did not converge!'
            print Sw_soln[k,i]
            print vel_x[k,i]
            print err
            sys.exit(0)
        else: 
          # for velocities faster than the shock the saturation is the initial composition
          Sw_soln[k,i] = IC
          fw_xD[k,i] = IC
          dfw_dSw_xD[k,i] = vel_x[k,i]


    ##############################################################################      
    # run PFLOTRAN simulation
    
    run_pflotran(input_prefix,nxyz,dxyz,lxyz,remove,screen_on,pf_exe,mpi_exe)
    
    # load data from HDF5
    index_string = 'Time:  1.00000E-02 y/Liquid_Saturation'
    Sw_pflotran[0,:] = read_pflotran_output_1D(path+
		      '/1D_RAD_BL_GENERAL_IMMISC_GAS_INJ.h5',index_string,False)
    index_string = 'Time:  1.00000E-01 y/Liquid_Saturation'
    Sw_pflotran[1,:] = read_pflotran_output_1D(path+
		      '/1D_RAD_BL_GENERAL_IMMISC_GAS_INJ.h5',index_string,False)
    index_string = 'Time:  5.00000E-01 y/Liquid_Saturation'
    Sw_pflotran[2,:] = read_pflotran_output_1D(path+
		      '/1D_RAD_BL_GENERAL_IMMISC_GAS_INJ.h5',index_string,False)
    index_string = 'Time:  1.00000E+00 y/Liquid_Saturation'
    Sw_pflotran[3,:] = read_pflotran_output_1D(path+
		      '/1D_RAD_BL_GENERAL_IMMISC_GAS_INJ.h5',index_string,remove)
    ierr = check(Sw_pflotran)

    L2_percent_error = calc_L2_relative_error(Sw_soln[3,:],Sw_pflotran[3,:],ierr)
    record_error(error_analysis,nxyz_record,dxyz_record,L2_percent_error,
                 nxyz,dxyz,try_count)

    test_pass = does_pass(L2_percent_error,try_count,num_tries)
    nxyz[0] = nxyz[0]*2.
    dxyz = lxyz/nxyz

  # Plot the PFLOTRAN and analytical solutions
  plot_1D_transient(path,t_yr,'yr',r_soln,Sw_soln,x_pflotran,Sw_pflotran,
                    'Distance [m]','Water Saturation [C]',"{0:.2f}".format(L2_percent_error))
  
  # Plot error analysis
  plot_error(error_analysis,nxyz_record,dxyz_record,path,try_count,1)
  
  # Add test result to report card
  add_to_report(path,test_pass,L2_percent_error,ierr)

  return;
  


#==============================================================================#
def BL_1D_GENERAL_COMP_GAS_INJ(path,input_prefix,remove,screen_on,pf_exe,
				                             mpi_exe,num_tries):
#==============================================================================#
# The compositional Buckley-Leverett solution including volume change as
# components partition between phases. Presented in:
# "Theory of gas injection processes"
# Section 4.4
# F. M Orr, Jr
# Tie-Line publications, Copenhagen Denmark
# Author: Tara LaForce
# Date: 09/27/2018
# Note: This example tests the numerical solver, not the phase behavior model.
# Component phase partitioning and phase properties from the simulator are
# hard-coded into the analytical solution  
# ******************************************************************************
  nxyz = np.zeros(3) + 1
  dxyz = np.zeros(3) + 1.
  lxyz = np.zeros(3) + 1.
  error_analysis = np.zeros(num_tries)
  dxyz_record = np.zeros((num_tries,3))
  nxyz_record = np.zeros((num_tries,3))
  test_pass = False
  try_count = 0

  # initial discretization values
  lxyz[0] = 10.              # [m] lx
  nxyz[0] = 20               # [m] nx
  dxyz = lxyz/nxyz           # [m]
  ierr = 0

  #####################################################################
  # problem parameters
  #####################################################################
  # GENERAL fluid parameters
  #all taken from mphase simulation output
  lam = 0.8 #lambda in the BURDINE_BC liquid and gas relative permeabilities (-)
  Srw = 0.1 #residual liquid sauration in the liquid relative permeability (-)
  Srg = 0.1 #residual gas saturation in the gas relative permeability (-)
  MW_h2o= 18.02 #g/mol
  MW_air = 28.9598 #g/mol
  #all taken from mphase simulation output
  rho_w_pure_mass = 9.9716e2 #water density (kg/m^3) at the initial condition
  rho_w_mix_mass = 9.9716e2 #water density (kg/m^3)
  rho_g_pure_mass = 1.1845#gas density (kg/m^3)
  rho_g_mix_mass = 1.172#gas density (kg/m^3)
  #rho_w_pure_mass = rho_g_pure_mass#gas density (kg/m^3)
  #rho_w_mix_mass = rho_w_pure_mass#gas density (kg/m^3)
  mu_w = 1.0e-3#water viscosity (Pa-s) 
  mu_g = 1.61e-5 #gas viscosity (Pa-s)
  
  mol_x_w = 0.999344 #mol fraction of water in the liquid phase
  mol_x_g = 1.0 - mol_x_w  #mol fraction of air in the liquid phase
  mol_y_w = 3.111e-2  #mol fraction of water in the gas phase
  mol_y_g = 1.0 - mol_y_w  #mol fraction of air in the gas phase


  #solid parameters
  domain_m = 10.0
  A = 1.0 #cross sectional area of the domain (m^2)
  phi = 0.25 #porosity (-)
  q_mass =7.5e-5 #gas mass injection rate  (g/s)
  Init_water = 1.0 #water saturation must be 100% in analytical solution
  Inj = 0.0 #water saturation must be 0% in analytical solution

  #some tolerances
  delta = 1e-7 #step size for approximating derivatives
  conv_tol = 1e-4 #tolerance for convergencce of iterative processes

  #convert densities to mol fractions
  rho_w_pure = 1000*rho_w_pure_mass/MW_h2o #water density (mol/L)
  rho_g_pure = 1000*rho_g_pure_mass/MW_air #gas density (mol/L)
  rho_w_mix = 1000*rho_w_mix_mass/(mol_x_g*MW_air + mol_x_w*MW_h2o) 
  rho_g_mix = 1000*rho_g_mix_mass/(mol_y_g*MW_air + mol_y_w*MW_h2o)

  #cast density into dimensionless terms
  rho_w_pureD = rho_w_pure/rho_g_pure
  rho_g_pureD = rho_g_pure/rho_g_pure
  rho_w_mixD = rho_w_mix/rho_g_pure
  rho_g_mixD = rho_g_mix/rho_g_pure

  #the initial and injection conditions cast as G_w
  #component 1 is WATER, component 2 is GAS. Conditions are in terms of water content
  IC = 1.0*rho_w_pureD*Init_water
  JC = 1.0*rho_g_pureD*Inj

  #solution times
  t_yr = np.array([0.01,0.10,0.5,1.0])                     # years
  #for calculating dimenssionless variables
  q_vol = 1.0e-3*q_mass/rho_g_pure_mass #volumetric injection rate (m^3/s) for MPHASE problem
  xD_to_xm = q_vol/(phi*A) #to convert from dimensionless distance to x in m and vice versa

  len_t = t_yr.size
  t_sec = np.zeros(len_t)
  t_sec = 3600*24*365.25*t_yr #simulation time (sec)
  
  while (not test_pass) and (try_count < num_tries): 
    print_discretization(lxyz,nxyz,dxyz)
    nx = int(nxyz[0]); ny = int(nxyz[1]); nz = int(nxyz[2])
    dx = dxyz[0]; dy = dxyz[1]; dz = dxyz[2]
    Lx = lxyz[0]; Ly = lxyz[1]; Lz = lxyz[2]
    try_count = try_count + 1

    x_soln = np.linspace(0.+(dx/2.),Lx-(dx/2.),nx)  # [m]
    x_edges = np.linspace(0.,Lx,nx+1)  # [m]
    Sw_soln = np.zeros((len_t,(nx)))                               # [-]
    Cg_soln = np.zeros((len_t,(nx)))                               # [-]
    Cgtot_soln = np.zeros((len_t,(nx)))                               # [-]
    Gg_soln = np.zeros((len_t,(nx)))                               # [-]
    x_pflotran = x_soln[0:nx]                       # [m]
    Sw_pflotran = np.zeros((len_t,nx))                                     # [-]
    Cw_pflotran = np.zeros((len_t,nx))                                     # [-]
    Cw_tot_pflotran = np.zeros((len_t,nx))                                     # [-]
    Cg_pflotran = np.zeros((len_t,nx))                                     # [-]
    Cg_tot_pflotran = np.zeros((len_t,nx))                                     # [-]
    y_g_mol_pflotran = np.zeros((len_t,nx))                                     # [-]
    y_w_mol_pflotran = np.zeros((len_t,nx))                                     # [-]
    x_g_mol_pflotran = np.zeros((len_t,nx))                                     # [-]
    rho_w_pflotran = np.zeros((len_t,nx))                                     # [-]
    rho_g_pflotran = np.zeros((len_t,nx))                                     # [-]
    rho_w_mol = np.zeros(nx)                                     # [-]
    rho_g_mol = np.zeros(nx)                                     # [-]
    cu_Cg_soln = np.zeros(len_t)
    cu_Cg_pflotran = np.zeros(len_t)

    #a lot of this is redundant and needs to be rationalized
    len_x = x_soln.size
    
    vel_x = np.zeros((len_t,len_x))

    #the dimensionless velocities corresponding to the output time and mesh
    for k in range(len_t):
      vel_x[k,:] = 1/(t_sec[k]*xD_to_xm)*x_soln 

    ##############################################################################      
    # create the analytical solution

    ######################################################################
    # Subfunctions
    ######################################################################
    def find_fw(Sw):
      krg = find_krg(Sw)
      krw = find_krw(Sw)
      Mg = krg/mu_g
      Mw = krw/mu_w
      fw = Mw/(Mw + Mg)
      return fw

    def find_krw(Sw):
      Sel = max(0,(Sw-Srw)/(1-Srw))
      krw = Sel**(3.0+2.0/lam)
      return krw

    def find_krg(Sw):
      Seg = min(1,max(0,(Sw-Srw)/(1-Srw-Srg)))
      krg = (1-Seg)**2*(1-Seg**(1+2.0/lam))
      return krg

    def find_dfw_dSw(Sw):
      Sw_plus = Sw + delta
      fw_plus = find_fw(Sw_plus)
      Sw_minus = Sw - delta
      fw_minus = find_fw(Sw_minus)
      dfw_dSw = (fw_plus-fw_minus)/(Sw_plus-Sw_minus)
      return dfw_dSw

    def find_shockTR(Sw,fw,BDRY):
      #this assumes that the upstream BDRY is single component, single phase, gas
      #and will not work otherwise!!
      #notice that shock velocity is based on the WATER component mass balancce
      Gw = find_G_or_alpha(Sw,'w')
      alpha_w = find_G_or_alpha(fw,'w')
      if (1.0 - Sw) == BDRY:
        v_shock = 0.0
      else:
        v_shock = (BDRY-alpha_w)/(BDRY-Gw)
      return v_shock

    def find_tanTR(Sw,BDRY):
      fw = find_fw(Sw)
      dfw_dSw = find_dfw_dSw(Sw)
      v_shock = find_shockTR(Sw,fw,BDRY)
      diff = ((dfw_dSw - v_shock)**2)**0.5
      return diff

    def find_shockLD(Sw,vC_D,BDRY):
      #this assumes that the downstream BDRY is single component, single phase, water
      #and will not work otherwise!!
      fw = find_fw(Sw)
      Gw_A = BDRY
      al_w_A = BDRY
      Gg_A = 0.0*rho_w_pureD*1.0
      al_g_A = Gg_A
      
      Gw_B = find_G_or_alpha(Sw,'w')
      al_w_B = find_G_or_alpha(fw,'w')
      Gg_B = find_G_or_alpha(Sw,'g')
      al_g_B = find_G_or_alpha(fw,'g')
      del_Gw = Gw_A - Gw_B
      del_Gg = Gg_A - Gg_B
      
      vA_D = vC_D*(al_w_B*del_Gg - al_g_B*del_Gw)/(al_w_A*del_Gg - al_g_A*del_Gw)
      vA_over_vC = vA_D/vC_D
      v_shock = (BDRY*vA_over_vC-al_w_B)/(BDRY-Gw_B)
      return v_shock
    
    def find_tanLD(Sw,args_in_min):
      fw = find_fw(Sw)
      vC_D = args_in_min[0]
      IC = args_in_min[1]
      dfw_dSw = find_dfw_dSw(Sw)
      v_shock = find_shockLD(Sw,vC_D,IC)
      diff = ((dfw_dSw - v_shock)**2)**0.5
      return diff

    def find_Sw_xD(Sw,vel):
      dfw_dSw = find_dfw_dSw(Sw)
      diff = ((dfw_dSw - vel)**2)**0.5
      return diff

    def find_G_or_alpha(S_or_f,comp):
      if comp == 'g':
        G_or_alpha = mol_x_g*rho_w_mixD*S_or_f + mol_y_g*rho_g_mixD*(1.0 - S_or_f)
      else:
        G_or_alpha = mol_x_w*rho_w_mixD*S_or_f + mol_y_w*rho_g_mixD*(1.0 - S_or_f)
      return G_or_alpha
  
    def find_Cw_or_Cg(Sw,comp):
      if Sw <= 0.0:
        #assumes pure gas condition
        if comp == 'w':
          Gj = 0.0*rho_g_pureD*(1.0 - Sw)
        else:
          Gj = 1.0*rho_g_pureD*(1.0 - Sw)
        z_bot = rho_g_pureD
      elif Sw < 1.0:
        Gj = find_G_or_alpha(Sw,comp)
        z_bot = rho_w_mixD*Sw + rho_g_mixD*(1.0 - Sw)
      else:
        #assumes pure water condition
        if comp == 'w':
          Gj = 1.0*rho_w_pureD*Sw
        else:
          Gj = 0.0*rho_w_pureD*Sw
        z_bot = rho_w_pureD
      Cj = Gj/z_bot
      return Cj

    def find_Cwtot_or_Cgtot(Sw,comp):
      Cj = find_Cw_or_Cg(Sw,comp)
      if Sw <= 0.0:
        #assumes pure gas condition
        bot = rho_g_pure
      elif Sw < 1.0:
        bot = rho_w_mix*Sw + rho_g_mix*(1.0 - Sw)
      else:
        #assumes pure water condition
        bot = rho_w_pure
      Cjtot = phi*A*Cj*bot 
      return Cjtot


    #####################################################################
    # Main program
    ######################################################################

    ######################################################################
    #this section finds the shocks in saturation

    #first get an approximate of the upper and lower bounds on water saturation at the shock
    Sw_now = np.linspace(0.0,1.0,101,True,False)
    len_Sw =  Sw_now.size
    dfw_dSw = np.zeros(len_Sw)

    for i in range(len_Sw):
      dfw_dSw[i] = find_dfw_dSw(Sw_now[i])
      #the correct tangent shock must be where dfw_dSw is relatively clsoe to 1 and
      #the maximum of dfw_dSw
      if i > 0:
        if dfw_dSw[i] > 0.8 and dfw_dSw[i-1] < 0.8:
          pos_tan_S_index = i-1
          pos_tan_Sw = Sw_now[i-1]

    #find the approximate maximum of dw_dSw
    max_dfw_dSw = np.amax(dfw_dSw)
    max_v_index = np.argmax(dfw_dSw)
    Sw_max_v = Sw_now[max_v_index]

    #FIND THE TRAILING SHOCK
    #set the upper and lower bounds on Sw to put into the iterative solver 
    Sw_bdry = [(0.0, pos_tan_Sw)]
    #the midpoint of the interval is a good first guess
    Sw_guess = (0.0 + pos_tan_Sw)/2.0
    #scipy solver to find the tangent shock where (fw-JC)/(Sw-JC)=dfw_dSw
    Sw_tan_infoJC = minimize(find_tanTR,Sw_guess,JC,method='L-BFGS-B',bounds=Sw_bdry,tol=conv_tol)

    #split output into a usable form and sanity check
    Sw_tanJC = Sw_tan_infoJC.x
    fw_tanJC = find_fw(Sw_tanJC)
    dfw_dSw_tanJC = find_dfw_dSw(Sw_tanJC)
    v_shock_tanJC = find_shockTR(Sw_tanJC,fw_tanJC,JC)
    if abs(dfw_dSw_tanJC - v_shock_tanJC) > 1e-3:
      print 'error! the trailing shock solver did not converge!'
      print abs(dfw_dSw_tanJC - v_shock_tanJC)
      sys.exit(0)

    #find the two-phase dimensionless shock velocity
    Gw_D = JC
    al_w_D = JC
    Gg_D = 1.0*rho_g_pureD*1.0
    al_g_D = Gg_D
    Gw_C = find_G_or_alpha(Sw_tanJC,'w')
    al_w_C = find_G_or_alpha(fw_tanJC,'w')
    Gg_C = find_G_or_alpha(Sw_tanJC,'g')
    al_g_C = find_G_or_alpha(fw_tanJC,'g')
    del_Gw = Gw_D - Gw_C
    del_Gg = Gg_D - Gg_C
    vC_D = (al_g_D*del_Gw - al_w_D*del_Gg)/(al_g_C*del_Gw - al_w_C*del_Gg)

    #FIND THE LEADING SHOCK
    #set the upper and lower bounds on Sw to put into the iterative solver 
    Sw_bdry = [(Srw,Sw_max_v)]
    #the midpoint of the interval is a good first guess
    Sw_guess = (Srw+Sw_max_v)/2.0
    args_in_min = np.array([vC_D,IC])
    #scipy solver to find the tangent shock where (fw-IC)/(Sw-IC)=dfw_dSw
    Sw_tan_infoIC = minimize(find_tanLD,Sw_guess,args_in_min,method='TNC',bounds=Sw_bdry,tol=conv_tol)
    #split output into a usable form and sanity check
    Sw_tanIC = Sw_tan_infoIC.x
    fw_tanIC = find_fw(Sw_tanIC)
    dfw_dSw_tanIC = find_dfw_dSw(Sw_tanIC)
    v_shock_tanIC = find_shockLD(Sw_tanIC,vC_D,IC)
    if abs(dfw_dSw_tanIC - v_shock_tanIC) > 1e-3:
      print 'error! the shock solver did not converge!'
      sys.exit(0)

    #find the two-phase dimensionless shock velocity
    Gw_A = IC
    al_w_A = IC
    Gg_A = 0.0*rho_w_pureD*1.0
    al_g_A = Gg_A

    Gw_B = find_G_or_alpha(Sw_tanIC,'w')
    al_w_B = find_G_or_alpha(fw_tanIC,'w')
    Gg_B = find_G_or_alpha(Sw_tanIC,'g')
    al_g_B = find_G_or_alpha(fw_tanIC,'g')

    del_Gw = Gw_A - Gw_B
    del_Gg = Gg_A - Gg_B
    vA_D = vC_D*(al_g_B*del_Gw - al_w_B*del_Gg)/(al_g_A*del_Gw - al_w_A*del_Gg)

    ######################################################################
    # finally finding Sw on the grid for each timestep

    #all two-phase Sw must be between the Sw_tanJC and the saturation at the tangent shock
    Sw_xD_bdry = [(Sw_tanJC,Sw_tanIC)]
    #the midpoint is a good first guess
    Sw_xD_guess = (Srw+Sw_tanIC)/2.0

    for k in range(len_t):
      for i in range(len_x):
        if vel_x[k,i] < v_shock_tanJC:
          # for velocities slower than the trailing shock the saturation is the injection composition
          Sw_soln[k,i] = 1.e-5
          Gg_soln[k,i] = Gg_D
          z_bot = rho_g_pureD
          Cg_soln[k,i] = find_Cw_or_Cg(Sw_soln[k,i],'g')
          Cgtot_soln[k,i] = find_Cwtot_or_Cgtot(Sw_soln[k,i],'g')*(x_edges[i+1] - x_edges[i])
        elif vel_x[k,i] < vC_D*v_shock_tanIC:
          # scipy solver to find the Sw corresponding to the dimensionless velocity behind the shock front
          Sw_xD_info = minimize(find_Sw_xD,Sw_xD_guess,vel_x[k,i]/vC_D,method='TNC',bounds=Sw_xD_bdry,tol=conv_tol)
          #split output into a usable form and sanity check
          Sw_soln[k,i] = Sw_xD_info.x
          Gg_soln[k,i] = find_G_or_alpha(Sw_soln[k,i],'g')
          Cg_soln[k,i] = find_Cw_or_Cg(Sw_soln[k,i],'g')
          Cgtot_soln[k,i] = find_Cwtot_or_Cgtot(Sw_soln[k,i],'g')*(x_edges[i+1] - x_edges[i])
          dfw_dSw_xD = find_dfw_dSw(Sw_soln[k,i])
          err = dfw_dSw_xD - vel_x[k,i]/vC_D
          if abs(err) > 1e-3:
            print 'error! the solver for Sw_xD did not converge!'
            print Sw_soln[k,i]
            print vel_x[k,i]
            print err
            sys.exit(0)
        else: 
          # for velocities faster than the leading shock the saturation is the initial composition
          Sw_soln[k,i] = 1.000001
          Cg_soln[k,i] = find_Cw_or_Cg(Sw_soln[k,i],'g')
          Cgtot_soln[k,i] = find_Cwtot_or_Cgtot(Sw_soln[k,i],'g')*(x_edges[i+1] - x_edges[i])
      cu_Cg_soln[k] = np.sum(Cgtot_soln[k,:])


   ##############################################################################      
    # run PFLOTRAN simulation
    
    run_pflotran(input_prefix,nxyz,dxyz,lxyz,remove,screen_on,pf_exe,mpi_exe)

    # load data from HDF5
    index_string = 'Time:  1.00000E-02 y/Liquid_Saturation'
    Sw_pflotran[0,:] = read_pflotran_output_1D(path+
		      '/1D_BL_GENERAL_COMP_GAS_INJ.h5',index_string,False)
    index_string = 'Time:  1.00000E-01 y/Liquid_Saturation'
    Sw_pflotran[1,:] = read_pflotran_output_1D(path+
		      '/1D_BL_GENERAL_COMP_GAS_INJ.h5',index_string,False)
    index_string = 'Time:  5.00000E-01 y/Liquid_Saturation'
    Sw_pflotran[2,:] = read_pflotran_output_1D(path+
		      '/1D_BL_GENERAL_COMP_GAS_INJ.h5',index_string,False)
    index_string = 'Time:  1.00000E+00 y/Liquid_Saturation'
    Sw_pflotran[3,:] = read_pflotran_output_1D(path+
		      '/1D_BL_GENERAL_COMP_GAS_INJ.h5',index_string,remove)
    ierr = check(Sw_pflotran)

    # load data from HDF5
    index_string = 'Time:  1.00000E-02 y/X_g^l'
    x_g_mol_pflotran[0,:] = read_pflotran_output_1D(path+
		      '/1D_BL_GENERAL_COMP_GAS_INJ.h5',index_string,False)
    index_string = 'Time:  1.00000E-01 y/X_g^l'
    x_g_mol_pflotran[1,:] = read_pflotran_output_1D(path+
		      '/1D_BL_GENERAL_COMP_GAS_INJ.h5',index_string,False)
    index_string = 'Time:  5.00000E-01 y/X_g^l'
    x_g_mol_pflotran[2,:] = read_pflotran_output_1D(path+
		      '/1D_BL_GENERAL_COMP_GAS_INJ.h5',index_string,False)
    index_string = 'Time:  1.00000E+00 y/X_g^l'
    x_g_mol_pflotran[3,:] = read_pflotran_output_1D(path+
		      '/1D_BL_GENERAL_COMP_GAS_INJ.h5',index_string,remove)
    ierr = check(x_g_mol_pflotran)


    # load data from HDF5
    index_string = 'Time:  1.00000E-02 y/X_l^g'
    y_w_mol_pflotran[0,:] = read_pflotran_output_1D(path+
		      '/1D_BL_GENERAL_COMP_GAS_INJ.h5',index_string,False)
    index_string = 'Time:  1.00000E-01 y/X_l^g'
    y_w_mol_pflotran[1,:] = read_pflotran_output_1D(path+
		      '/1D_BL_GENERAL_COMP_GAS_INJ.h5',index_string,False)
    index_string = 'Time:  5.00000E-01 y/X_l^g'
    y_w_mol_pflotran[2,:] = read_pflotran_output_1D(path+
		      '/1D_BL_GENERAL_COMP_GAS_INJ.h5',index_string,False)
    index_string = 'Time:  1.00000E+00 y/X_l^g'
    y_w_mol_pflotran[3,:] = read_pflotran_output_1D(path+
		      '/1D_BL_GENERAL_COMP_GAS_INJ.h5',index_string,remove)
    ierr = check(y_w_mol_pflotran)

    y_g_mol_pflotran = 1.0 - y_w_mol_pflotran
    x_w_mol_pflotran = 1.0 - x_g_mol_pflotran

    # load data from HDF5
    index_string = 'Time:  1.00000E-02 y/Gas_Density [kg_m^3]'
    rho_g_pflotran[0,:] = read_pflotran_output_1D(path+
		      '/1D_BL_GENERAL_COMP_GAS_INJ.h5',index_string,False)
    index_string = 'Time:  1.00000E-01 y/Gas_Density [kg_m^3]'
    rho_g_pflotran[1,:] = read_pflotran_output_1D(path+
		      '/1D_BL_GENERAL_COMP_GAS_INJ.h5',index_string,False)
    index_string = 'Time:  5.00000E-01 y/Gas_Density [kg_m^3]'
    rho_g_pflotran[2,:] = read_pflotran_output_1D(path+
		      '/1D_BL_GENERAL_COMP_GAS_INJ.h5',index_string,False)
    index_string = 'Time:  1.00000E+00 y/Gas_Density [kg_m^3]'
    rho_g_pflotran[3,:] = read_pflotran_output_1D(path+
		      '/1D_BL_GENERAL_COMP_GAS_INJ.h5',index_string,remove)
    ierr = check(rho_g_pflotran)

    # load data from HDF5
    index_string = 'Time:  1.00000E-02 y/Liquid_Density [kg_m^3]'
    rho_w_pflotran[0,:] = read_pflotran_output_1D(path+
		      '/1D_BL_GENERAL_COMP_GAS_INJ.h5',index_string,False)
    index_string = 'Time:  1.00000E-01 y/Liquid_Density [kg_m^3]'
    rho_w_pflotran[1,:] = read_pflotran_output_1D(path+
		      '/1D_BL_GENERAL_COMP_GAS_INJ.h5',index_string,False)
    index_string = 'Time:  5.00000E-01 y/Liquid_Density [kg_m^3]'
    rho_w_pflotran[2,:] = read_pflotran_output_1D(path+
		      '/1D_BL_GENERAL_COMP_GAS_INJ.h5',index_string,False)
    index_string = 'Time:  1.00000E+00 y/Liquid_Density [kg_m^3]'
    rho_w_pflotran[3,:] = read_pflotran_output_1D(path+
		      '/1D_BL_GENERAL_COMP_GAS_INJ.h5',index_string,remove)
    ierr = check(rho_w_pflotran)

    for k in range(len_t):
      for i in range(len_x):
        if MW_h2o*y_w_mol_pflotran[k,i] + MW_air*y_g_mol_pflotran[k,i] > 0.0: #if no gas is present the mol fractions are all zero
          rho_g_mol[i] = 1000*rho_g_pflotran[k,i]/(y_g_mol_pflotran[k,i]*MW_air + y_w_mol_pflotran[k,i]*MW_h2o) 
        else:
          rho_g_mol[i] = 1000.0*rho_g_pflotran[i]/MW_h2o
        if MW_h2o*x_w_mol_pflotran[k,i] + MW_air*x_g_mol_pflotran[k,i] > 0.0: # in case the same thing happens when no water is present
          rho_w_mol[i] = 1000*rho_w_pflotran[k,i]/(x_g_mol_pflotran[k,i]*MW_air + x_w_mol_pflotran[k,i]*MW_h2o) 
        else:
          rho_w_mol[i] = 1000.0*rho_w_pflotran[k,i]/MW_air
        bot = rho_w_mol[i]*Sw_pflotran[k,i] + rho_g_mol[i]*(1.0 - Sw_pflotran[k,i])
        Cw_pflotran[k,i] = (x_w_mol_pflotran[k,i]*rho_w_mol[i]*Sw_pflotran[k,i] + y_w_mol_pflotran[k,i]*rho_g_mol[i]*(1.0 - Sw_pflotran[k,i]))/bot
        Cg_pflotran[k,i] = (x_g_mol_pflotran[k,i]*rho_w_mol[i]*Sw_pflotran[k,i] + y_g_mol_pflotran[k,i]*rho_g_mol[i]*(1.0 - Sw_pflotran[k,i]))/bot
        Cw_tot_pflotran[k,i] = A*phi*Cw_pflotran[k,i]*(x_edges[i+1] - x_edges[i])*bot
        Cg_tot_pflotran[k,i] = A*phi*Cg_pflotran[k,i]*(x_edges[i+1] - x_edges[i])*bot 

      cu_Cg_pflotran[k] = np.sum(Cg_tot_pflotran[k,:])

    L1_percent_error = calc_L1_relative_error(Cgtot_soln[3,:],Cg_tot_pflotran[3,:],ierr)
    record_error(error_analysis,nxyz_record,dxyz_record,L1_percent_error,
                 nxyz,dxyz,try_count)

    test_pass = does_pass(L1_percent_error,try_count,num_tries)
    nxyz[0] = nxyz[0]*2.
    dxyz = lxyz/nxyz

  # Plot the PFLOTRAN and analytical solutions
  plot_1D_transient(path,t_yr,'yr',x_soln,Cgtot_soln,x_pflotran,Cg_tot_pflotran,
                    'Distance [m]','Total air [mol]',"{0:.2f}".format(L1_percent_error))
  
  # Plot error analysis
  plot_error(error_analysis,nxyz_record,dxyz_record,path,try_count,1)
  
  # Add test result to report card
  add_to_report(path,test_pass,L1_percent_error,ierr)

  return;
################################################################################


#==============================================================================#
def BL_1D_GENERAL_COMP_WATER_INJ(path,input_prefix,remove,screen_on,pf_exe,
				                             mpi_exe,num_tries):
#==============================================================================#
# The compositional Buckley-Leverett solution including volume change as
# components partition between phases. Presented in:
# "Theory of gas injection processes"
# Section 4.4
# F. M Orr, Jr
# Tie-Line publications, Copenhagen Denmark
# Author: Tara LaForce
# Date: 09/27/2018
# Note: This example tests the numerical solver, not the phase behavior model.
# Component phase partitioning and phase properties from the simulator are
# hard-coded into the analytical solution  
# ******************************************************************************
  nxyz = np.zeros(3) + 1
  dxyz = np.zeros(3) + 1.
  lxyz = np.zeros(3) + 1.
  error_analysis = np.zeros(num_tries)
  dxyz_record = np.zeros((num_tries,3))
  nxyz_record = np.zeros((num_tries,3))
  test_pass = False
  try_count = 0

  # initial discretization values
  lxyz[0] = 10.              # [m] lx
  nxyz[0] = 20               # [m] nx
  dxyz = lxyz/nxyz           # [m]
  ierr = 0

  #####################################################################
  # problem parameters
  #####################################################################
  # GENERAL fluid parameters
  #all taken from mphase simulation output
  lam = 0.8 #lambda in the BURDINE_BC liquid and gas relative permeabilities (-)
  Srw = 0.1 #residual liquid sauration in the liquid relative permeability (-)
  Srg = 0.1 #residual gas saturation in the gas relative permeability (-)
  MW_h2o= 18.02 #g/mol
  MW_air = 28.9598 #g/mol
  #all taken from mphase simulation output
  rho_w_pure_mass = 9.9716e2 #water density (kg/m^3) at the initial condition
  rho_w_mix_mass = 9.9716e2 #water density (kg/m^3)
  rho_g_pure_mass = 1.1845#gas density (kg/m^3)
  rho_g_mix_mass = 1.171#gas density (kg/m^3)
  mu_w = 1.0e-3#water viscosity (Pa-s) 
  mu_g = 1.61e-5 #gas viscosity (Pa-s)
  
  mol_x_w = 0.9999803 #mol fraction of water in the liquid phase
  mol_x_g = 1.0 - mol_x_w  #mol fraction of CO2 in the liquid phase
  mol_y_w = 3.111e-2  #mol fraction of water in the gas phase
  mol_y_g = 1.0 - mol_y_w  #mol fraction of CO2 in the gas phase

  #solid parameters
  domain_m = 10.0
  A = 1.0 #cross sectional area of the domain (m^2)
  phi = 0.25 #porosity (-)
  q_mass = 6.0e-2 #water mass injection rate  (g/s)
  Init_water = 0.0 #water saturation must be 100% in analytical solution
  Inj = 1.0 #water saturation must be 0% in analytical solution

  #some tolerances
  delta = 1e-7 #step size for approximating derivatives
  conv_tol = 1e-4 #tolerance for convergencce of iterative processes

  #convert densities to mol fractions
  rho_w_pure = 1000*rho_w_pure_mass/MW_h2o #water density (mol/L)
  rho_g_pure = 1000*rho_g_pure_mass/MW_air #gas density (mol/L)
  rho_w_mix = 1000*rho_w_mix_mass/(mol_x_g*MW_air + mol_x_w*MW_h2o) 
  rho_g_mix = 1000*rho_g_mix_mass/(mol_y_g*MW_air + mol_y_w*MW_h2o)

  #cast density into dimensionless terms
  rho_w_pureD = rho_w_pure/rho_w_pure
  rho_g_pureD = rho_g_pure/rho_w_pure
  rho_w_mixD = rho_w_mix/rho_w_pure
  rho_g_mixD = rho_g_mix/rho_w_pure


  #the initial and injection conditions cast as G_w
  #component 1 is WATER, component 2 is GAS. Conditions are in terms of water content
  IC = 1.0*rho_g_pureD*Init_water
  JC = 1.0*rho_w_pureD*Inj

  #solution times
  t_yr = np.array([0.01,0.10,0.5,1.0])                     # years
  #for calculating dimenssionless variables
  q_vol = 1.0e-3*q_mass/rho_w_pure_mass #volumetric injection rate (m^3/s) for GENERAL water injection problem
  xD_to_xm = q_vol/(phi*A) #to convert from dimensionless distance to x in m and vice versa

  len_t = t_yr.size
  t_sec = np.zeros(len_t)
  t_sec = 3600*24*365.25*t_yr #simulation time (sec)
  
  while (not test_pass) and (try_count < num_tries): 
    print_discretization(lxyz,nxyz,dxyz)
    nx = int(nxyz[0]); ny = int(nxyz[1]); nz = int(nxyz[2])
    dx = dxyz[0]; dy = dxyz[1]; dz = dxyz[2]
    Lx = lxyz[0]; Ly = lxyz[1]; Lz = lxyz[2]
    try_count = try_count + 1

    x_soln = np.linspace(0.+(dx/2.),Lx-(dx/2.),nx)  # [m]
    x_edges = np.linspace(0.,Lx,nx+1)  # [m]
    Sw_soln = np.zeros((len_t,(nx)))                               # [-]
    Cw_soln = np.zeros((len_t,(nx)))                               # [-]
    Cg_soln = np.zeros((len_t,(nx)))                               # [-]
    Cwtot_soln = np.zeros((len_t,(nx)))                               # [-]
    Cgtot_soln = np.zeros((len_t,(nx)))                               # [-]
    x_pflotran = x_soln[0:nx]                       # [m]
    Sw_pflotran = np.zeros((len_t,nx))                                     # [-]
    Cw_pflotran = np.zeros((len_t,nx))                                     # [-]
    Cw_tot_pflotran = np.zeros((len_t,nx))                                     # [-]
    Cg_pflotran = np.zeros((len_t,nx))                                     # [-]
    Cg_tot_pflotran = np.zeros((len_t,nx))                                     # [-]
    y_g_mol_pflotran = np.zeros((len_t,nx))                                     # [-]
    y_w_mol_pflotran = np.zeros((len_t,nx))                                     # [-]
    x_g_mol_pflotran = np.zeros((len_t,nx))                                     # [-]
    rho_w_pflotran = np.zeros((len_t,nx))                                     # [-]
    rho_g_pflotran = np.zeros((len_t,nx))                                     # [-]
    rho_w_mol = np.zeros(nx)                                     # [-]
    rho_g_mol = np.zeros(nx)                                     # [-]
    cu_Cw_soln = np.zeros(len_t)
    cu_Cw_pflotran = np.zeros(len_t)

    #a lot of this is redundant and needs to be rationalized
    len_x = x_soln.size
    
    vel_x = np.zeros((len_t,len_x))

    #the dimensionless velocities corresponding to the output time and mesh
    for k in range(len_t):
      vel_x[k,:] = 1/(t_sec[k]*xD_to_xm)*x_soln 

    ##############################################################################      
    # create the analytical solution

    ######################################################################
    # Subfunctions
    ######################################################################
    def find_fw(Sw):
      krg = find_krg(Sw)
      krw = find_krw(Sw)
      Mg = krg/mu_g
      Mw = krw/mu_w
      fw = Mw/(Mw + Mg)
      return fw

    def find_krw(Sw):
      Sel = max(0,(Sw-Srw)/(1-Srw))
      krw = Sel**(3.0+2.0/lam)
      return krw

    def find_krg(Sw):
      Seg = min(1,max(0,(Sw-Srw)/(1-Srw-Srg)))
      krg = (1-Seg)**2*(1-Seg**(1+2.0/lam))
      return krg

    def find_dfw_dSw(Sw):
      Sw_plus = Sw + delta
      fw_plus = find_fw(Sw_plus)
      Sw_minus = Sw - delta
      fw_minus = find_fw(Sw_minus)
      dfw_dSw = (fw_plus-fw_minus)/(Sw_plus-Sw_minus)
      return dfw_dSw

    def find_shockTR(Sw,fw,BDRY):
      #this assumes that the upstream BDRY is single component, single phase, water
      #and will not work otherwise!!
      #notice that shock velocity is based on the GAS component mass balancce
      BDRYg = 1.0 - BDRY
      Gg = find_G_or_alpha(Sw,'g')
      alpha_g = find_G_or_alpha(fw,'g')
      if (1.0 - Sw) == BDRY:
        v_shock = 0.0
      else:
        v_shock = (BDRYg-alpha_g)/(BDRYg-Gg)
      return v_shock

    def find_tanTR(Sw,BDRY):
      fw = find_fw(Sw)
      dfw_dSw = find_dfw_dSw(Sw)
      v_shock = find_shockTR(Sw,fw,BDRY)
      diff = ((dfw_dSw - v_shock)**2)**0.5
      return diff

    def find_shockLD(Sw,vC_D,BDRY):
      #this assumes that the downstream BDRY is single component, single phase, gas
      #and will not work otherwise!!
      fw = find_fw(Sw)
      Gw_A = BDRY
      al_w_A = BDRY
      Gg_A = 1.0*rho_g_pureD*1.0
      al_g_A = Gg_A
      
      Gw_B = find_G_or_alpha(Sw,'w')
      al_w_B = find_G_or_alpha(fw,'w')
      Gg_B = find_G_or_alpha(Sw,'g')
      al_g_B = find_G_or_alpha(fw,'g')
      del_Gw = Gw_A - Gw_B
      del_Gg = Gg_A - Gg_B

      vA_D = vC_D*(al_w_B*del_Gg - al_g_B*del_Gw)/(al_w_A*del_Gg - al_g_A*del_Gw)
      vA_over_vC = vA_D/vC_D
      v_shock = (Gg_A*vA_over_vC-al_g_B)/(al_g_A-Gg_B)
      return v_shock
    
    def find_tanLD(Sw,args_in_min):
      fw = find_fw(Sw)
      vC_D = args_in_min[0]
      IC = args_in_min[1]
      dfw_dSw = find_dfw_dSw(Sw)
      v_shock = find_shockLD(Sw,vC_D,IC)
      diff = ((dfw_dSw - v_shock)**2)**0.5
      return diff

    def find_Sw_xD(Sw,vel):
      dfw_dSw = find_dfw_dSw(Sw)
      diff = ((dfw_dSw - vel)**2)**0.5
      return diff

    def find_G_or_alpha(S_or_f,comp):
      if comp == 'g':
        G_or_alpha = mol_x_g*rho_w_mixD*S_or_f + mol_y_g*rho_g_mixD*(1.0 - S_or_f)
      else:
        G_or_alpha = mol_x_w*rho_w_mixD*S_or_f + mol_y_w*rho_g_mixD*(1.0 - S_or_f)
      return G_or_alpha
  
    def find_Cw_or_Cg(Sw,comp):
      if Sw <= 0.0:
        #assumes pure gas condition
        if comp == 'w':
          Gj = 0.0*rho_g_pureD*(1.0 - Sw)
        else:
          Gj = 1.0*rho_g_pureD*(1.0 - Sw)
        z_bot = rho_g_pureD
      elif Sw < 1.0:
        Gj = find_G_or_alpha(Sw,comp)
        z_bot = rho_w_mixD*Sw + rho_g_mixD*(1.0 - Sw)
      else:
        #assumes pure water condition
        if comp == 'w':
          Gj = 1.0*rho_w_pureD*Sw
        else:
          Gj = 0.0*rho_w_pureD*Sw
        z_bot = rho_w_pureD
      Cj = Gj/z_bot
      return Cj

    def find_Cwtot_or_Cgtot(Sw,comp):
      Cj = find_Cw_or_Cg(Sw,comp)
      if Sw <= 0.0:
        #assumes pure gas condition
        bot = rho_g_pure
      elif Sw < 1.0:
        bot = rho_w_mix*Sw + rho_g_mix*(1.0 - Sw)
      else:
        #assumes pure water condition
        bot = rho_w_pure
      Cjtot = phi*A*Cj*bot 
      return Cjtot


    #####################################################################
    # Main program
    ######################################################################

    ######################################################################
    #this section finds the shocks in saturation

    #first get an approximate of the upper and lower bounds on water saturation at the shock
    Sw_now = np.linspace(0.0,1.0,101,True,False)
    len_Sw =  Sw_now.size
    dfw_dSw = np.zeros(len_Sw)

    #find the approximate maximum of dw_dSw
    max_dfw_dSw = np.amax(dfw_dSw)
    max_v_index = np.argmax(dfw_dSw)
    Sw_max_v = Sw_now[max_v_index]

    #FIND THE TRAILING SHOCK
    #set the upper and lower bounds on Sw to put into the iterative solver 
    Sw_bdry = [(Srw, 1.0 - Srg-1e-4)]
    #the midpoint of the interval is a good first guess
    Sw_guess = 0.95
    #scipy solver to find the tangent shock where (fw-JC)/(Sw-JC)=dfw_dSw
    Sw_tan_infoJC = minimize(find_tanTR,Sw_guess,JC,method='TNC',bounds=Sw_bdry,tol=conv_tol)

    #split output into a usable form and sanity check
    Sw_tanJC = Sw_tan_infoJC.x
    fw_tanJC = find_fw(Sw_tanJC)
    Gw_tanJC = find_G_or_alpha(Sw_tanJC,'w')
    al_w_tanJC = find_G_or_alpha(fw_tanJC,'w')
    dfw_dSw_tanJC = find_dfw_dSw(Sw_tanJC)
    v_shock_tanJC = find_shockTR(Sw_tanJC,fw_tanJC,JC)
    if abs(dfw_dSw_tanJC - v_shock_tanJC) > 1e-3:
      print 'error! the trailing shock solver did not converge!'
      print abs(dfw_dSw_tanJC - v_shock_tanJC)
      sys.exit(0)

    #find the two-phase dimensionless shock velocity
    Gw_D = JC*rho_w_pureD*1.0
    al_w_D = Gw_D
    Gg_D = (1.0-JC)*rho_w_pureD*1.0
    al_g_D = Gg_D
    Gw_C = Gw_tanJC
    al_w_C = al_w_tanJC
    Gg_C = find_G_or_alpha(Sw_tanJC,'g')
    al_g_C = find_G_or_alpha(fw_tanJC,'g')
    del_Gw = Gw_D - Gw_C
    del_Gg = Gg_D - Gg_C
    vC_D = (al_g_D*del_Gw - al_w_D*del_Gg)/(al_g_C*del_Gw - al_w_C*del_Gg)

    #FIND THE LEADING SHOCK
    #set the upper and lower bounds on Sw to put into the iterative solver 
    Sw_bdry = [(Sw_max_v,1.0-Srg)]
    Sw_guess = 0.88
    args_in_min = np.array([vC_D,IC])
    #scipy solver to find the tangent shock where (fw-IC)/(Sw-IC)=dfw_dSw
    Sw_tan_infoIC = minimize(find_tanLD,Sw_guess,args_in_min,method='TNC',bounds=Sw_bdry,tol=conv_tol)
    #split output into a usable form and sanity check
    Sw_tanIC = Sw_tan_infoIC.x
    fw_tanIC = find_fw(Sw_tanIC)
    dfw_dSw_tanIC = find_dfw_dSw(Sw_tanIC)
    v_shock_tanIC = find_shockLD(Sw_tanIC,vC_D,IC)
    if abs(dfw_dSw_tanIC - v_shock_tanIC) > 1e-3:
      print 'error! the shock solver did not converge!'
      sys.exit(0)

    #find the two-phase dimensionless shock velocity
    Gw_A = IC
    al_w_A = IC
    Gg_A = 1.0*rho_g_pureD*1.0
    al_g_A = Gg_A

    Gw_B = find_G_or_alpha(Sw_tanIC,'w')
    al_w_B = find_G_or_alpha(fw_tanIC,'w')
    Gg_B = find_G_or_alpha(Sw_tanIC,'g')
    al_g_B = find_G_or_alpha(fw_tanIC,'g')

    del_Gw = Gw_A - Gw_B
    del_Gg = Gg_A - Gg_B
    vA_D = vC_D*(al_g_B*del_Gw - al_w_B*del_Gg)/(al_g_A*del_Gw - al_w_A*del_Gg)

    ######################################################################
    # finally finding Sw on the grid for each timestep

    #all two-phase Sw must be between the Sw_tanJC and the saturation at the tangent shock
    Sw_xD_bdry = [(Sw_tanIC,Sw_tanJC)]
    #the midpoint is a good first guess
    Sw_xD_guess = (Sw_tanJC+Sw_tanIC)/2.0

    for k in range(len_t):
      for i in range(len_x):
        if vel_x[k,i] < v_shock_tanJC:
          # for velocities slower than the trailing shock the saturation is the injection composition
          Sw_soln[k,i] = 1.0 + 1.e-5
          Cw_soln[k,i] = find_Cw_or_Cg(Sw_soln[k,i],'w')
          Cg_soln[k,i] = find_Cw_or_Cg(Sw_soln[k,i],'g')
          Cgtot_soln[k,i] = find_Cwtot_or_Cgtot(Sw_soln[k,i],'g')*(x_edges[i+1] - x_edges[i])
          Cwtot_soln[k,i] = find_Cwtot_or_Cgtot(Sw_soln[k,i],'w')*(x_edges[i+1] - x_edges[i])
        elif vel_x[k,i] < vC_D*v_shock_tanIC:
          # scipy solver to find the Sw corresponding to the dimensionless velocity behind the shock front
          Sw_xD_info = minimize(find_Sw_xD,Sw_xD_guess,vel_x[k,i]/vC_D,method='TNC',bounds=Sw_xD_bdry,tol=conv_tol)
          #split output into a usable form and sanity check
          Sw_soln[k,i] = Sw_xD_info.x
          Cw_soln[k,i] = find_Cw_or_Cg(Sw_soln[k,i],'w')
          Cg_soln[k,i] = find_Cw_or_Cg(Sw_soln[k,i],'g')
          Cgtot_soln[k,i] = find_Cwtot_or_Cgtot(Sw_soln[k,i],'g')*(x_edges[i+1] - x_edges[i])
          Cwtot_soln[k,i] = find_Cwtot_or_Cgtot(Sw_soln[k,i],'w')*(x_edges[i+1] - x_edges[i])
          dfw_dSw_xD = find_dfw_dSw(Sw_soln[k,i])
          err = dfw_dSw_xD - vel_x[k,i]/vC_D
          if abs(err) > 1e-3:
            print 'error! the solver for Sw_xD did not converge!'
            print Sw_soln[k,i]
            print vel_x[k,i]
            print err
            sys.exit(0)
        else: 
          # for velocities faster than the leading shock the saturation is the initial composition
          Sw_soln[k,i] = -1.e-5
          Cw_soln[k,i] = find_Cw_or_Cg(Sw_soln[k,i],'w')
          Cg_soln[k,i] = find_Cw_or_Cg(Sw_soln[k,i],'g')
          Cgtot_soln[k,i] = find_Cwtot_or_Cgtot(Sw_soln[k,i],'g')*(x_edges[i+1] - x_edges[i])
          Cwtot_soln[k,i] = find_Cwtot_or_Cgtot(Sw_soln[k,i],'w')*(x_edges[i+1] - x_edges[i])
      cu_Cw_soln[k] = np.sum(Cwtot_soln[k,:])

   ##############################################################################      
    # run PFLOTRAN simulation
    
    run_pflotran(input_prefix,nxyz,dxyz,lxyz,remove,screen_on,pf_exe,mpi_exe)
    
    # load data from HDF5
    index_string = 'Time:  1.00000E-02 y/Liquid_Saturation'
    Sw_pflotran[0,:] = read_pflotran_output_1D(path+
		      '/1D_BL_GENERAL_COMP_WATER_INJ.h5',index_string,False)
    index_string = 'Time:  1.00000E-01 y/Liquid_Saturation'
    Sw_pflotran[1,:] = read_pflotran_output_1D(path+
		      '/1D_BL_GENERAL_COMP_WATER_INJ.h5',index_string,False)
    index_string = 'Time:  5.00000E-01 y/Liquid_Saturation'
    Sw_pflotran[2,:] = read_pflotran_output_1D(path+
		      '/1D_BL_GENERAL_COMP_WATER_INJ.h5',index_string,False)
    index_string = 'Time:  1.00000E+00 y/Liquid_Saturation'
    Sw_pflotran[3,:] = read_pflotran_output_1D(path+
		      '/1D_BL_GENERAL_COMP_WATER_INJ.h5',index_string,remove)
    ierr = check(Sw_pflotran)

    # load data from HDF5
    index_string = 'Time:  1.00000E-02 y/X_g^l'
    x_g_mol_pflotran[0,:] = read_pflotran_output_1D(path+
		      '/1D_BL_GENERAL_COMP_WATER_INJ.h5',index_string,False)
    index_string = 'Time:  1.00000E-01 y/X_g^l'
    x_g_mol_pflotran[1,:] = read_pflotran_output_1D(path+
		      '/1D_BL_GENERAL_COMP_WATER_INJ.h5',index_string,False)
    index_string = 'Time:  5.00000E-01 y/X_g^l'
    x_g_mol_pflotran[2,:] = read_pflotran_output_1D(path+
		      '/1D_BL_GENERAL_COMP_WATER_INJ.h5',index_string,False)
    index_string = 'Time:  1.00000E+00 y/X_g^l'
    x_g_mol_pflotran[3,:] = read_pflotran_output_1D(path+
		      '/1D_BL_GENERAL_COMP_WATER_INJ.h5',index_string,remove)
    ierr = check(x_g_mol_pflotran)


    # load data from HDF5
    index_string = 'Time:  1.00000E-02 y/X_l^g'
    y_w_mol_pflotran[0,:] = read_pflotran_output_1D(path+
		      '/1D_BL_GENERAL_COMP_WATER_INJ.h5',index_string,False)
    index_string = 'Time:  1.00000E-01 y/X_l^g'
    y_w_mol_pflotran[1,:] = read_pflotran_output_1D(path+
		      '/1D_BL_GENERAL_COMP_WATER_INJ.h5',index_string,False)
    index_string = 'Time:  5.00000E-01 y/X_l^g'
    y_w_mol_pflotran[2,:] = read_pflotran_output_1D(path+
		      '/1D_BL_GENERAL_COMP_WATER_INJ.h5',index_string,False)
    index_string = 'Time:  1.00000E+00 y/X_l^g'
    y_w_mol_pflotran[3,:] = read_pflotran_output_1D(path+
		      '/1D_BL_GENERAL_COMP_WATER_INJ.h5',index_string,remove)
    ierr = check(y_w_mol_pflotran)

    y_g_mol_pflotran = 1.0 - y_w_mol_pflotran
    x_w_mol_pflotran = 1.0 - x_g_mol_pflotran

    # load data from HDF5
    index_string = 'Time:  1.00000E-02 y/Gas_Density [kg_m^3]'
    rho_g_pflotran[0,:] = read_pflotran_output_1D(path+
		      '/1D_BL_GENERAL_COMP_WATER_INJ.h5',index_string,False)
    index_string = 'Time:  1.00000E-01 y/Gas_Density [kg_m^3]'
    rho_g_pflotran[1,:] = read_pflotran_output_1D(path+
		      '/1D_BL_GENERAL_COMP_WATER_INJ.h5',index_string,False)
    index_string = 'Time:  5.00000E-01 y/Gas_Density [kg_m^3]'
    rho_g_pflotran[2,:] = read_pflotran_output_1D(path+
		      '/1D_BL_GENERAL_COMP_WATER_INJ.h5',index_string,False)
    index_string = 'Time:  1.00000E+00 y/Gas_Density [kg_m^3]'
    rho_g_pflotran[3,:] = read_pflotran_output_1D(path+
		      '/1D_BL_GENERAL_COMP_WATER_INJ.h5',index_string,remove)
    ierr = check(rho_g_pflotran)

    # load data from HDF5
    index_string = 'Time:  1.00000E-02 y/Liquid_Density [kg_m^3]'
    rho_w_pflotran[0,:] = read_pflotran_output_1D(path+
		      '/1D_BL_GENERAL_COMP_WATER_INJ.h5',index_string,False)
    index_string = 'Time:  1.00000E-01 y/Liquid_Density [kg_m^3]'
    rho_w_pflotran[1,:] = read_pflotran_output_1D(path+
		      '/1D_BL_GENERAL_COMP_WATER_INJ.h5',index_string,False)
    index_string = 'Time:  5.00000E-01 y/Liquid_Density [kg_m^3]'
    rho_w_pflotran[2,:] = read_pflotran_output_1D(path+
		      '/1D_BL_GENERAL_COMP_WATER_INJ.h5',index_string,False)
    index_string = 'Time:  1.00000E+00 y/Liquid_Density [kg_m^3]'
    rho_w_pflotran[3,:] = read_pflotran_output_1D(path+
		      '/1D_BL_GENERAL_COMP_WATER_INJ.h5',index_string,remove)
    ierr = check(rho_w_pflotran)

    for k in range(len_t):
      for i in range(len_x):
        if MW_h2o*y_w_mol_pflotran[k,i] + MW_air*y_g_mol_pflotran[k,i] > 0.0: #if no gas is present the mol fractions are all zero
          rho_g_mol[i] = 1000*rho_g_pflotran[k,i]/(y_g_mol_pflotran[k,i]*MW_air + y_w_mol_pflotran[k,i]*MW_h2o) 
        else:
          rho_g_mol[i] = 1000.0*rho_g_pflotran[i]/MW_h2o
        if MW_h2o*x_w_mol_pflotran[k,i] + MW_air*x_g_mol_pflotran[k,i] > 0.0: # in case the same thing happens when no water is present
          rho_w_mol[i] = 1000*rho_w_pflotran[k,i]/(x_g_mol_pflotran[k,i]*MW_air + x_w_mol_pflotran[k,i]*MW_h2o) 
        else:
          rho_w_mol[i] = 1000.0*rho_w_pflotran[k,i]/MW_air
        bot = rho_w_mol[i]*Sw_pflotran[k,i] + rho_g_mol[i]*(1.0 - Sw_pflotran[k,i])
        Cw_pflotran[k,i] = (x_w_mol_pflotran[k,i]*rho_w_mol[i]*Sw_pflotran[k,i] + y_w_mol_pflotran[k,i]*rho_g_mol[i]*(1.0 - Sw_pflotran[k,i]))/bot
        Cg_pflotran[k,i] = (x_g_mol_pflotran[k,i]*rho_w_mol[i]*Sw_pflotran[k,i] + y_g_mol_pflotran[k,i]*rho_g_mol[i]*(1.0 - Sw_pflotran[k,i]))/bot
        Cw_tot_pflotran[k,i] = A*phi*Cw_pflotran[k,i]*(x_edges[i+1] - x_edges[i])*bot
        Cg_tot_pflotran[k,i] = A*phi*Cg_pflotran[k,i]*(x_edges[i+1] - x_edges[i])*bot 

      cu_Cw_pflotran[k] = np.sum(Cw_tot_pflotran[k,:])

    L1_percent_error = calc_L1_relative_error(Cwtot_soln[3,:],Cw_tot_pflotran[3,:],ierr)
    record_error(error_analysis,nxyz_record,dxyz_record,L1_percent_error,
                 nxyz,dxyz,try_count)

    test_pass = does_pass(L1_percent_error,try_count,num_tries)
    nxyz[0] = nxyz[0]*2.
    dxyz = lxyz/nxyz

  # Plot the PFLOTRAN and analytical solutions
  plot_1D_transient(path,t_yr,'yr',x_soln,Cwtot_soln,x_pflotran,Cw_tot_pflotran,
                   'Distance [m]','Total water [mol]',"{0:.2f}".format(L1_percent_error))

  # Plot error analysis
  plot_error(error_analysis,nxyz_record,dxyz_record,path,try_count,1)
  
  # Add test result to report card
  add_to_report(path,test_pass,L1_percent_error,ierr)

  return;
#==============================================================================#


#==============================================================================#
def BL_1D_RAD_GENERAL_COMP_GAS_INJ(path,input_prefix,remove,screen_on,pf_exe,
				                             mpi_exe,num_tries):
#==============================================================================#
# The compositional Buckley-Leverett solution including volume change as
# components partition between phases. Linear solution presented in:
# "Theory of gas injection processes"
# Section 4.4
# F. M Orr, Jr
# Tie-Line publications, Copenhagen Denmark
# Radial coordinate transform is available in:
# Analytical Solution of Nonisothermal Buckley-Leverett Flow Including Tracers
# Dindoruk and Dindoruk
# SPE Journal, 2008
# doi: 10.2118/102266-PA
# Author: Tara LaForce
# Date: 10/11/2018
# Note: This example tests the numerical solver, not the phase behavior model.
# Component phase partitioning and phase properties from the simulator are
# hard-coded into the analytical solution  
# ******************************************************************************

  nxyz = np.zeros(3) + 1
  dxyz = np.zeros(3) + 1.
  lxyz = np.zeros(3) + 1.
  error_analysis = np.zeros(num_tries)
  dxyz_record = np.zeros((num_tries,3))
  nxyz_record = np.zeros((num_tries,3))
  test_pass = False
  try_count = 0

  # initial discretization values
  lxyz[0] = 10.              # [m] lx
  nxyz[0] = 20               # [m] nx
  dxyz = lxyz/nxyz           # [m]
  ierr = 0

  #####################################################################
  # problem parameters
  #####################################################################
  # GENERAL fluid parameters
  #all taken from mphase simulation output
  lam = 0.8 #lambda in the BURDINE_BC liquid and gas relative permeabilities (-)
  Srw = 0.1 #residual liquid sauration in the liquid relative permeability (-)
  Srg = 0.1 #residual gas saturation in the gas relative permeability (-)
  MW_h2o= 18.02 #g/mol
  MW_air = 28.9598 #g/mol
  #all taken from mphase simulation output
  rho_w_pure_mass = 9.9716e2 #water density (kg/m^3) at the initial condition
  rho_w_mix_mass = 9.9716e2 #water density (kg/m^3)
  rho_g_pure_mass = 1.1845#gas density (kg/m^3)
  rho_g_mix_mass = 1.172#gas density (kg/m^3)
  mu_w = 1.0e-3#water viscosity (Pa-s) 
  mu_g = 1.61e-5 #gas viscosity (Pa-s)
  
  mol_x_w = 0.999344 #mol fraction of water in the liquid phase
  mol_x_g = 1.0 - mol_x_w  #mol fraction of air in the liquid phase
  mol_y_w = 3.111e-2  #mol fraction of water in the gas phase
  mol_y_g = 1.0 - mol_y_w  #mol fraction of air in the gas phase


  #solid parameters
  domain_m = lxyz
  h = 1.0 #height of the domain (m)
  phi = 0.25 #porosity (-)
  q_mass = 7.5e-4 #gas mass injection rate  (g/s)
  Init_water = 1.0 #water saturation must be 100% in analytical solution
  Inj = 0.0 #water saturation must be 0% in analytical solution

  #some tolerances
  delta = 1e-7 #step size for approximating derivatives
  conv_tol = 1e-4 #tolerance for convergencce of iterative processes

  #convert densities to mol fractions
  rho_w_pure = 1000*rho_w_pure_mass/MW_h2o #water density (mol/L)
  rho_g_pure = 1000*rho_g_pure_mass/MW_air #gas density (mol/L)
  rho_w_mix = 1000*rho_w_mix_mass/(mol_x_g*MW_air + mol_x_w*MW_h2o) 
  rho_g_mix = 1000*rho_g_mix_mass/(mol_y_g*MW_air + mol_y_w*MW_h2o)

  #cast density into dimensionless terms
  rho_w_pureD = rho_w_pure/rho_g_pure
  rho_g_pureD = rho_g_pure/rho_g_pure
  rho_w_mixD = rho_w_mix/rho_g_pure
  rho_g_mixD = rho_g_mix/rho_g_pure

  #the initial and injection conditions cast as G_w
  #component 1 is WATER, component 2 is GAS. Conditions are in terms of water content
  IC = 1.0*rho_w_pureD*Init_water
  JC = 1.0*rho_g_pureD*Inj

  #solution times
  t_yr = np.array([0.01,0.10,0.5,1.0])                     # years
  #for calculating dimenssionless variables
  q_vol = 1.0e-3*q_mass/rho_g_pure_mass #volumetric injection rate (m^3/s) for MPHASE problem
  rD_to_rm = q_vol/(phi*h*math.pi) #to convert from dimensionless distance to x in m and vice versa

  len_t = t_yr.size
  t_sec = np.zeros(len_t)
  t_sec = 3600*24*365.25*t_yr #simulation time (sec)
  
  while (not test_pass) and (try_count < num_tries): 
    print_discretization(lxyz,nxyz,dxyz)
    nx = int(nxyz[0]); ny = int(nxyz[1]); nz = int(nxyz[2])
    dx = dxyz[0]; dy = dxyz[1]; dz = dxyz[2]
    Lx = lxyz[0]; Ly = lxyz[1]; Lz = lxyz[2]
    try_count = try_count + 1

    r_soln = np.linspace(0.+(dx/2.),Lx-(dx/2.),nx)  # [m]
    r_edges = np.linspace(0.,Lx,nx+1)  # [m]
    Sw_soln = np.zeros((len_t,(nx)))                               # [-]
    Cg_soln = np.zeros((len_t,(nx)))                               # [-]
    Cgtot_soln = np.zeros((len_t,(nx)))                               # [-]
    Gg_soln = np.zeros((len_t,(nx)))                               # [-]
    x_pflotran = r_soln[0:nx]                       # [m]
    Sw_pflotran = np.zeros((len_t,nx))                                     # [-]
    Cw_pflotran = np.zeros((len_t,nx))                                     # [-]
    Cw_tot_pflotran = np.zeros((len_t,nx))                                     # [-]
    Cg_pflotran = np.zeros((len_t,nx))                                     # [-]
    Cg_tot_pflotran = np.zeros((len_t,nx))                                     # [-]
    y_g_mol_pflotran = np.zeros((len_t,nx))                                     # [-]
    y_w_mol_pflotran = np.zeros((len_t,nx))                                     # [-]
    x_g_mol_pflotran = np.zeros((len_t,nx))                                     # [-]
    rho_w_pflotran = np.zeros((len_t,nx))                                     # [-]
    rho_g_pflotran = np.zeros((len_t,nx))                                     # [-]
    rho_w_mol = np.zeros(nx)                                     # [-]
    rho_g_mol = np.zeros(nx)                                     # [-]
    cu_Cg_soln = np.zeros(len_t)
    cu_Cg_pflotran = np.zeros(len_t)

    #a lot of this is redundant and needs to be rationalized
    len_x = r_soln.size
    
    vel_x = np.zeros((len_t,len_x))

    #the dimensionless velocities corresponding to the output time and mesh
    for k in range(len_t):
      vel_x[k,:] = 1/(t_sec[k]*rD_to_rm)*np.power(r_soln,2.0) 

    ##############################################################################      
    # create the analytical solution

    ######################################################################
    # Subfunctions
    ######################################################################
    def find_fw(Sw):
      krg = find_krg(Sw)
      krw = find_krw(Sw)
      Mg = krg/mu_g
      Mw = krw/mu_w
      fw = Mw/(Mw + Mg)
      return fw

    def find_krw(Sw):
      Sel = max(0,(Sw-Srw)/(1-Srw))
      krw = Sel**(3.0+2.0/lam)
      return krw

    def find_krg(Sw):
      Seg = min(1,max(0,(Sw-Srw)/(1-Srw-Srg)))
      krg = (1-Seg)**2*(1-Seg**(1+2.0/lam))
      return krg

    def find_dfw_dSw(Sw):
      Sw_plus = Sw + delta
      fw_plus = find_fw(Sw_plus)
      Sw_minus = Sw - delta
      fw_minus = find_fw(Sw_minus)
      dfw_dSw = (fw_plus-fw_minus)/(Sw_plus-Sw_minus)
      return dfw_dSw

    def find_shockTR(Sw,fw,BDRY):
      #this assumes that the upstream BDRY is single component, single phase, gas
      #and will not work otherwise!!
      #notice that shock velocity is based on the WATER component mass balancce
      Gw = find_G_or_alpha(Sw,'w')
      alpha_w = find_G_or_alpha(fw,'w')
      if (1.0 - Sw) == BDRY:
        v_shock = 0.0
      else:
        v_shock = (BDRY-alpha_w)/(BDRY-Gw)
      return v_shock

    def find_tanTR(Sw,BDRY):
      fw = find_fw(Sw)
      dfw_dSw = find_dfw_dSw(Sw)
      v_shock = find_shockTR(Sw,fw,BDRY)
      diff = ((dfw_dSw - v_shock)**2)**0.5
      return diff

    def find_shockLD(Sw,vC_D,BDRY):
      #this assumes that the downstream BDRY is single component, single phase, water
      #and will not work otherwise!!
      fw = find_fw(Sw)
      Gw_A = BDRY
      al_w_A = BDRY
      Gg_A = 0.0*rho_w_pureD*1.0
      al_g_A = Gg_A
      
      Gw_B = find_G_or_alpha(Sw,'w')
      al_w_B = find_G_or_alpha(fw,'w')
      Gg_B = find_G_or_alpha(Sw,'g')
      al_g_B = find_G_or_alpha(fw,'g')
      del_Gw = Gw_A - Gw_B
      del_Gg = Gg_A - Gg_B
      
      vA_D = vC_D*(al_w_B*del_Gg - al_g_B*del_Gw)/(al_w_A*del_Gg - al_g_A*del_Gw)
      vA_over_vC = vA_D/vC_D
      v_shock = (BDRY*vA_over_vC-al_w_B)/(BDRY-Gw_B)
      return v_shock
    
    def find_tanLD(Sw,args_in_min):
      fw = find_fw(Sw)
      vC_D = args_in_min[0]
      IC = args_in_min[1]
      dfw_dSw = find_dfw_dSw(Sw)
      v_shock = find_shockLD(Sw,vC_D,IC)
      diff = ((dfw_dSw - v_shock)**2)**0.5
      return diff

    def find_Sw_xD(Sw,vel):
      dfw_dSw = find_dfw_dSw(Sw)
      diff = ((dfw_dSw - vel)**2)**0.5
      #diff = dfw_dSw
      return diff

    def find_G_or_alpha(S_or_f,comp):
      if comp == 'g':
        G_or_alpha = mol_x_g*rho_w_mixD*S_or_f + mol_y_g*rho_g_mixD*(1.0 - S_or_f)
      else:
        G_or_alpha = mol_x_w*rho_w_mixD*S_or_f + mol_y_w*rho_g_mixD*(1.0 - S_or_f)
      return G_or_alpha
  
    def find_Cw_or_Cg(Sw,comp):
      if Sw <= 0.0:
        #assumes pure gas condition
        if comp == 'w':
          Gj = 0.0*rho_g_pureD*(1.0 - Sw)
        else:
          Gj = 1.0*rho_g_pureD*(1.0 - Sw)
        z_bot = rho_g_pureD
      elif Sw < 1.0:
        Gj = find_G_or_alpha(Sw,comp)
        z_bot = rho_w_mixD*Sw + rho_g_mixD*(1.0 - Sw)
      else:
        #assumes pure water condition
        if comp == 'w':
          Gj = 1.0*rho_w_pureD*Sw
        else:
          Gj = 0.0*rho_w_pureD*Sw
        z_bot = rho_w_pureD
      Cj = Gj/z_bot
      return Cj

    def find_Cwtot_or_Cgtot(Sw,comp):
      Cj = find_Cw_or_Cg(Sw,comp)
      if Sw <= 0.0:
        #assumes pure gas condition
        bot = rho_g_pure
      elif Sw < 1.0:
        bot = rho_w_mix*Sw + rho_g_mix*(1.0 - Sw)
      else:
        #assumes pure water condition
        bot = rho_w_pure
      Cjtot = phi*h*math.pi*Cj*bot 
      return Cjtot


    #####################################################################
    # Main program
    ######################################################################

    ######################################################################
    #this section finds the shocks in saturation

    #first get an approximate of the upper and lower bounds on water saturation at the shock
    Sw_now = np.linspace(0.0,1.0,101,True,False)
    len_Sw =  Sw_now.size
    dfw_dSw = np.zeros(len_Sw)

    for i in range(len_Sw):
      dfw_dSw[i] = find_dfw_dSw(Sw_now[i])
      #the correct tangent shock must be where dfw_dSw is relatively clsoe to 1 and
      #the maximum of dfw_dSw
      if i > 0:
        if dfw_dSw[i] > 0.8 and dfw_dSw[i-1] < 0.8:
          pos_tan_S_index = i-1
          pos_tan_Sw = Sw_now[i-1]

    #find the approximate maximum of dw_dSw
    max_dfw_dSw = np.amax(dfw_dSw)
    max_v_index = np.argmax(dfw_dSw)
    Sw_max_v = Sw_now[max_v_index]

    #FIND THE TRAILING SHOCK
    #set the upper and lower bounds on Sw to put into the iterative solver 
    Sw_bdry = [(0.0, pos_tan_Sw)]
    #the midpoint of the interval is a good first guess
    Sw_guess = (0.0 + pos_tan_Sw)/2.0
    #scipy solver to find the tangent shock where (fw-JC)/(Sw-JC)=dfw_dSw
    Sw_tan_infoJC = minimize(find_tanTR,Sw_guess,JC,method='L-BFGS-B',bounds=Sw_bdry,tol=conv_tol)

    #split output into a usable form and sanity check
    Sw_tanJC = Sw_tan_infoJC.x
    fw_tanJC = find_fw(Sw_tanJC)
    dfw_dSw_tanJC = find_dfw_dSw(Sw_tanJC)
    v_shock_tanJC = find_shockTR(Sw_tanJC,fw_tanJC,JC)
    if abs(dfw_dSw_tanJC - v_shock_tanJC) > 1e-3:
      print 'error! the trailing shock solver did not converge!'
      print abs(dfw_dSw_tanJC - v_shock_tanJC)
      sys.exit(0)

    #find the two-phase dimensionless shock velocity
    Gw_D = JC
    al_w_D = JC
    Gg_D = 1.0*rho_g_pureD*1.0
    al_g_D = Gg_D
    Gw_C = find_G_or_alpha(Sw_tanJC,'w')
    al_w_C = find_G_or_alpha(fw_tanJC,'w')
    Gg_C = find_G_or_alpha(Sw_tanJC,'g')
    al_g_C = find_G_or_alpha(fw_tanJC,'g')
    del_Gw = Gw_D - Gw_C
    del_Gg = Gg_D - Gg_C
    vC_D = (al_g_D*del_Gw - al_w_D*del_Gg)/(al_g_C*del_Gw - al_w_C*del_Gg)

    #FIND THE LEADING SHOCK
    #set the upper and lower bounds on Sw to put into the iterative solver 
    Sw_bdry = [(Srw,Sw_max_v)]
    #the midpoint of the interval is a good first guess
    Sw_guess = (Srw+Sw_max_v)/2.0
    args_in_min = np.array([vC_D,IC])
    #scipy solver to find the tangent shock where (fw-IC)/(Sw-IC)=dfw_dSw
    Sw_tan_infoIC = minimize(find_tanLD,Sw_guess,args_in_min,method='TNC',bounds=Sw_bdry,tol=conv_tol)
    #split output into a usable form and sanity check
    Sw_tanIC = Sw_tan_infoIC.x
    fw_tanIC = find_fw(Sw_tanIC)
    dfw_dSw_tanIC = find_dfw_dSw(Sw_tanIC)
    v_shock_tanIC = find_shockLD(Sw_tanIC,vC_D,IC)
    if abs(dfw_dSw_tanIC - v_shock_tanIC) > 1e-3:
      print 'error! the shock solver did not converge!'
      sys.exit(0)

    #find the two-phase dimensionless shock velocity
    Gw_A = IC
    al_w_A = IC
    Gg_A = 0.0*rho_w_pureD*1.0
    al_g_A = Gg_A

    Gw_B = find_G_or_alpha(Sw_tanIC,'w')
    al_w_B = find_G_or_alpha(fw_tanIC,'w')
    Gg_B = find_G_or_alpha(Sw_tanIC,'g')
    al_g_B = find_G_or_alpha(fw_tanIC,'g')

    del_Gw = Gw_A - Gw_B
    del_Gg = Gg_A - Gg_B
    vA_D = vC_D*(al_g_B*del_Gw - al_w_B*del_Gg)/(al_g_A*del_Gw - al_w_A*del_Gg)

    ######################################################################
    # finally finding Sw on the grid for each timestep

    #all two-phase Sw must be between the Sw_tanJC and the saturation at the tangent shock
    Sw_xD_bdry = [(Sw_tanJC,Sw_tanIC)]
    #the midpoint is a good first guess
    Sw_xD_guess = (Srw+Sw_tanIC)/2.0

    for k in range(len_t):
      for i in range(len_x):
        if vel_x[k,i] < v_shock_tanJC:
          # for velocities slower than the trailing shock the saturation is the injection composition
          Sw_soln[k,i] = 1.e-5
          Gg_soln[k,i] = Gg_D
          z_bot = rho_g_pureD
          Cg_soln[k,i] = find_Cw_or_Cg(Sw_soln[k,i],'g')
          Cgtot_soln[k,i] = find_Cwtot_or_Cgtot(Sw_soln[k,i],'g')*(r_edges[i+1]**2.0 - r_edges[i]**2.0)
        elif vel_x[k,i] < vC_D*v_shock_tanIC:
          # scipy solver to find the Sw corresponding to the dimensionless velocity behind the shock front
          Sw_xD_info = minimize(find_Sw_xD,Sw_xD_guess,vel_x[k,i]/vC_D,method='TNC',bounds=Sw_xD_bdry,tol=conv_tol)
          #split output into a usable form and sanity check
          Sw_soln[k,i] = Sw_xD_info.x
          Gg_soln[k,i] = find_G_or_alpha(Sw_soln[k,i],'g')
          Cg_soln[k,i] = find_Cw_or_Cg(Sw_soln[k,i],'g')
          Cgtot_soln[k,i] = find_Cwtot_or_Cgtot(Sw_soln[k,i],'g')*(r_edges[i+1]**2.0 - r_edges[i]**2.0)
          dfw_dSw_xD = find_dfw_dSw(Sw_soln[k,i])
          err = dfw_dSw_xD - vel_x[k,i]/vC_D
          if abs(err) > 1e-3:
            print 'error! the solver for Sw_xD did not converge!'
            print Sw_soln[k,i]
            print vel_x[k,i]
            print err
            sys.exit(0)
        else: 
          # for velocities faster than the leading shock the saturation is the initial composition
          Sw_soln[k,i] = 1.000001
          Cg_soln[k,i] = find_Cw_or_Cg(Sw_soln[k,i],'g')
          Cgtot_soln[k,i] = find_Cwtot_or_Cgtot(Sw_soln[k,i],'g')*(r_edges[i+1]**2.0 - r_edges[i]**2.0)
      cu_Cg_soln[k] = np.sum(Cgtot_soln[k,:])


   ##############################################################################      
    # run PFLOTRAN simulation
    
    run_pflotran(input_prefix,nxyz,dxyz,lxyz,remove,screen_on,pf_exe,mpi_exe)
    
    # load data from HDF5
    index_string = 'Time:  1.00000E-02 y/Liquid_Saturation'
    Sw_pflotran[0,:] = read_pflotran_output_1D(path+
		      '/1D_RAD_BL_GENERAL_COMP_GAS_INJ.h5',index_string,False)
    index_string = 'Time:  1.00000E-01 y/Liquid_Saturation'
    Sw_pflotran[1,:] = read_pflotran_output_1D(path+
		      '/1D_RAD_BL_GENERAL_COMP_GAS_INJ.h5',index_string,False)
    index_string = 'Time:  5.00000E-01 y/Liquid_Saturation'
    Sw_pflotran[2,:] = read_pflotran_output_1D(path+
		      '/1D_RAD_BL_GENERAL_COMP_GAS_INJ.h5',index_string,False)
    index_string = 'Time:  1.00000E+00 y/Liquid_Saturation'
    Sw_pflotran[3,:] = read_pflotran_output_1D(path+
		      '/1D_RAD_BL_GENERAL_COMP_GAS_INJ.h5',index_string,remove)
    ierr = check(Sw_pflotran)

    # load data from HDF5
    index_string = 'Time:  1.00000E-02 y/X_g^l'
    x_g_mol_pflotran[0,:] = read_pflotran_output_1D(path+
		      '/1D_RAD_BL_GENERAL_COMP_GAS_INJ.h5',index_string,False)
    index_string = 'Time:  1.00000E-01 y/X_g^l'
    x_g_mol_pflotran[1,:] = read_pflotran_output_1D(path+
		      '/1D_RAD_BL_GENERAL_COMP_GAS_INJ.h5',index_string,False)
    index_string = 'Time:  5.00000E-01 y/X_g^l'
    x_g_mol_pflotran[2,:] = read_pflotran_output_1D(path+
		      '/1D_RAD_BL_GENERAL_COMP_GAS_INJ.h5',index_string,False)
    index_string = 'Time:  1.00000E+00 y/X_g^l'
    x_g_mol_pflotran[3,:] = read_pflotran_output_1D(path+
		      '/1D_RAD_BL_GENERAL_COMP_GAS_INJ.h5',index_string,remove)
    ierr = check(x_g_mol_pflotran)


    # load data from HDF5
    index_string = 'Time:  1.00000E-02 y/X_l^g'
    y_w_mol_pflotran[0,:] = read_pflotran_output_1D(path+
		      '/1D_RAD_BL_GENERAL_COMP_GAS_INJ.h5',index_string,False)
    index_string = 'Time:  1.00000E-01 y/X_l^g'
    y_w_mol_pflotran[1,:] = read_pflotran_output_1D(path+
		      '/1D_RAD_BL_GENERAL_COMP_GAS_INJ.h5',index_string,False)
    index_string = 'Time:  5.00000E-01 y/X_l^g'
    y_w_mol_pflotran[2,:] = read_pflotran_output_1D(path+
		      '/1D_RAD_BL_GENERAL_COMP_GAS_INJ.h5',index_string,False)
    index_string = 'Time:  1.00000E+00 y/X_l^g'
    y_w_mol_pflotran[3,:] = read_pflotran_output_1D(path+
		      '/1D_RAD_BL_GENERAL_COMP_GAS_INJ.h5',index_string,remove)
    ierr = check(y_w_mol_pflotran)

    y_g_mol_pflotran = 1.0 - y_w_mol_pflotran
    x_w_mol_pflotran = 1.0 - x_g_mol_pflotran

    # load data from HDF5
    index_string = 'Time:  1.00000E-02 y/Gas_Density [kg_m^3]'
    rho_g_pflotran[0,:] = read_pflotran_output_1D(path+
		      '/1D_RAD_BL_GENERAL_COMP_GAS_INJ.h5',index_string,False)
    index_string = 'Time:  1.00000E-01 y/Gas_Density [kg_m^3]'
    rho_g_pflotran[1,:] = read_pflotran_output_1D(path+
		      '/1D_RAD_BL_GENERAL_COMP_GAS_INJ.h5',index_string,False)
    index_string = 'Time:  5.00000E-01 y/Gas_Density [kg_m^3]'
    rho_g_pflotran[2,:] = read_pflotran_output_1D(path+
		      '/1D_RAD_BL_GENERAL_COMP_GAS_INJ.h5',index_string,False)
    index_string = 'Time:  1.00000E+00 y/Gas_Density [kg_m^3]'
    rho_g_pflotran[3,:] = read_pflotran_output_1D(path+
		      '/1D_RAD_BL_GENERAL_COMP_GAS_INJ.h5',index_string,remove)
    ierr = check(rho_g_pflotran)

    # load data from HDF5
    index_string = 'Time:  1.00000E-02 y/Liquid_Density [kg_m^3]'
    rho_w_pflotran[0,:] = read_pflotran_output_1D(path+
		      '/1D_RAD_BL_GENERAL_COMP_GAS_INJ.h5',index_string,False)
    index_string = 'Time:  1.00000E-01 y/Liquid_Density [kg_m^3]'
    rho_w_pflotran[1,:] = read_pflotran_output_1D(path+
		      '/1D_RAD_BL_GENERAL_COMP_GAS_INJ.h5',index_string,False)
    index_string = 'Time:  5.00000E-01 y/Liquid_Density [kg_m^3]'
    rho_w_pflotran[2,:] = read_pflotran_output_1D(path+
		      '/1D_RAD_BL_GENERAL_COMP_GAS_INJ.h5',index_string,False)
    index_string = 'Time:  1.00000E+00 y/Liquid_Density [kg_m^3]'
    rho_w_pflotran[3,:] = read_pflotran_output_1D(path+
		      '/1D_RAD_BL_GENERAL_COMP_GAS_INJ.h5',index_string,remove)
    ierr = check(rho_w_pflotran)

    for k in range(len_t):
      for i in range(len_x):
        if MW_h2o*y_w_mol_pflotran[k,i] + MW_air*y_g_mol_pflotran[k,i] > 0.0: #if no gas is present the mol fractions are all zero
          rho_g_mol[i] = 1000*rho_g_pflotran[k,i]/(y_g_mol_pflotran[k,i]*MW_air + y_w_mol_pflotran[k,i]*MW_h2o) 
        else:
          rho_g_mol[i] = 1000.0*rho_g_pflotran[i]/MW_h2o
        if MW_h2o*x_w_mol_pflotran[k,i] + MW_air*x_g_mol_pflotran[k,i] > 0.0: # in case the same thing happens when no water is present
          rho_w_mol[i] = 1000*rho_w_pflotran[k,i]/(x_g_mol_pflotran[k,i]*MW_air + x_w_mol_pflotran[k,i]*MW_h2o) 
        else:
          rho_w_mol[i] = 1000.0*rho_w_pflotran[k,i]/MW_air
        bot = rho_w_mol[i]*Sw_pflotran[k,i] + rho_g_mol[i]*(1.0 - Sw_pflotran[k,i])
        Cw_pflotran[k,i] = (x_w_mol_pflotran[k,i]*rho_w_mol[i]*Sw_pflotran[k,i] + y_w_mol_pflotran[k,i]*rho_g_mol[i]*(1.0 - Sw_pflotran[k,i]))/bot
        Cg_pflotran[k,i] = (x_g_mol_pflotran[k,i]*rho_w_mol[i]*Sw_pflotran[k,i] + y_g_mol_pflotran[k,i]*rho_g_mol[i]*(1.0 - Sw_pflotran[k,i]))/bot
        Cw_tot_pflotran[k,i] = h*phi*math.pi*Cw_pflotran[k,i]*(r_edges[i+1]**2.0 - r_edges[i]**2.0)*bot
        Cg_tot_pflotran[k,i] = h*phi*math.pi*Cg_pflotran[k,i]*(r_edges[i+1]**2.0 - r_edges[i]**2.0)*bot 

      cu_Cg_pflotran[k] = np.sum(Cg_tot_pflotran[k,:])

    L1_percent_error = calc_L1_relative_error(Cgtot_soln[3,:],Cg_tot_pflotran[3,:],ierr)
    record_error(error_analysis,nxyz_record,dxyz_record,L1_percent_error,
                 nxyz,dxyz,try_count)

    test_pass = does_pass(L1_percent_error,try_count,num_tries)
    nxyz[0] = nxyz[0]*2.
    dxyz = lxyz/nxyz

  # Plot the PFLOTRAN and analytical solutions
  plot_1D_transient(path,t_yr,'yr',r_soln,Cgtot_soln,x_pflotran,Cg_tot_pflotran,
                    'Distance [m]','Total air [mol]',"{0:.2f}".format(L1_percent_error))
  
  # Plot error analysis
  plot_error(error_analysis,nxyz_record,dxyz_record,path,try_count,1)
  
  # Add test result to report card
  add_to_report(path,test_pass,L1_percent_error,ierr)

  return;

#==============================================================================#
def BL_1D_RAD_GENERAL_COMP_WATER_INJ(path,input_prefix,remove,screen_on,pf_exe,
				                             mpi_exe,num_tries):
#==============================================================================#
# The compositional Buckley-Leverett solution including volume change as
# components partition between phases. Linear solution presented in:
# "Theory of gas injection processes"
# Section 4.4
# F. M Orr, Jr
# Tie-Line publications, Copenhagen Denmark
# Radial coordinate transform is available in:
# Analytical Solution of Nonisothermal Buckley-Leverett Flow Including Tracers
# Dindoruk and Dindoruk
# SPE Journal, 2008
# doi: 10.2118/102266-PA
# Author: Tara LaForce
# Date: 10/11/2018
# Note: This example tests the numerical solver, not the phase behavior model.
# Component phase partitioning and phase properties from the simulator are
# hard-coded into the analytical solution  
# ******************************************************************************
  nxyz = np.zeros(3) + 1
  dxyz = np.zeros(3) + 1.
  lxyz = np.zeros(3) + 1.
  error_analysis = np.zeros(num_tries)
  dxyz_record = np.zeros((num_tries,3))
  nxyz_record = np.zeros((num_tries,3))
  test_pass = False
  try_count = 0

  # initial discretization values
  lxyz[0] = 10.              # [m] lx
  nxyz[0] = 20               # [m] nx
  dxyz = lxyz/nxyz           # [m]
  ierr = 0

  #####################################################################
  # problem parameters
  #####################################################################
  # GENERAL fluid parameters
  #all taken from mphase simulation output
  lam = 0.8 #lambda in the BURDINE_BC liquid and gas relative permeabilities (-)
  Srw = 0.1 #residual liquid sauration in the liquid relative permeability (-)
  Srg = 0.1 #residual gas saturation in the gas relative permeability (-)
  MW_h2o= 18.02 #g/mol
  MW_air = 28.9598 #g/mol
  #all taken from mphase simulation output
  rho_w_pure_mass = 9.9716e2 #water density (kg/m^3) at the initial condition
  rho_w_mix_mass = 9.9716e2 #water density (kg/m^3)
  rho_g_pure_mass = 1.1845#gas density (kg/m^3)
  rho_g_mix_mass = 1.171#gas density (kg/m^3)
  mu_w = 1.0e-3#water viscosity (Pa-s) 
  mu_g = 1.61e-5 #gas viscosity (Pa-s)
  
  mol_x_w = 0.9999803 #mol fraction of water in the liquid phase
  mol_x_g = 1.0 - mol_x_w  #mol fraction of CO2 in the liquid phase
  mol_y_w = 3.111e-2  #mol fraction of water in the gas phase
  mol_y_g = 1.0 - mol_y_w  #mol fraction of CO2 in the gas phase

  #solid parameters
  domain_m = 10.0
  h = 1.0 #height of the domain (m)
  phi = 0.25 #porosity (-)
  q_mass = 1.0 #water mass injection rate  (g/s)
  Init_water = 0.0 #water saturation must be 100% in analytical solution
  Inj = 1.0 #water saturation must be 0% in analytical solution

  #some tolerances
  delta = 1e-7 #step size for approximating derivatives
  conv_tol = 1e-4 #tolerance for convergencce of iterative processes

  #convert densities to mol basis
  rho_w_pure = 1000*rho_w_pure_mass/MW_h2o #water density (mol/L)
  rho_g_pure = 1000*rho_g_pure_mass/MW_air #gas density (mol/L)
  rho_w_mix = 1000*rho_w_mix_mass/(mol_x_g*MW_air + mol_x_w*MW_h2o) 
  rho_g_mix = 1000*rho_g_mix_mass/(mol_y_g*MW_air + mol_y_w*MW_h2o)

  #cast density into dimensionless terms
  rho_w_pureD = rho_w_pure/rho_w_pure
  rho_g_pureD = rho_g_pure/rho_w_pure
  rho_w_mixD = rho_w_mix/rho_w_pure
  rho_g_mixD = rho_g_mix/rho_w_pure


  #the initial and injection conditions cast as G_w
  #component 1 is WATER, component 2 is GAS. Conditions are in terms of water content
  IC = 1.0*rho_g_pureD*Init_water
  JC = 1.0*rho_w_pureD*Inj

  #solution times
  t_yr = np.array([0.01,0.10,0.5,1.0])                     # years
  #for calculating dimenssionless variables
  q_vol = 1.0e-3*q_mass/rho_w_pure_mass #volumetric injection rate (m^3/s) for GENERAL water injection problem
  rD_to_rm = q_vol/(phi*h*math.pi) #to convert from dimensionless distance to x in m and vice versa

  len_t = t_yr.size
  t_sec = np.zeros(len_t)
  t_sec = 3600*24*365.25*t_yr #simulation time (sec)
  
  while (not test_pass) and (try_count < num_tries): 
    print_discretization(lxyz,nxyz,dxyz)
    nx = int(nxyz[0]); ny = int(nxyz[1]); nz = int(nxyz[2])
    dx = dxyz[0]; dy = dxyz[1]; dz = dxyz[2]
    Lx = lxyz[0]; Ly = lxyz[1]; Lz = lxyz[2]
    try_count = try_count + 1

    r_soln = np.linspace(0.+(dx/2.),Lx-(dx/2.),nx)  # [m]
    r_edges = np.linspace(0.,Lx,nx+1)
    Sw_soln = np.zeros((len_t,(nx)))                               # [-]
    Cw_soln = np.zeros((len_t,(nx)))                               # [-]
    Cg_soln = np.zeros((len_t,(nx)))                               # [-]
    Cwtot_soln = np.zeros((len_t,(nx)))                               # [-]
    Cgtot_soln = np.zeros((len_t,(nx)))                               # [-]
    x_pflotran = r_soln[0:nx]                       # [m]
    Sw_pflotran = np.zeros((len_t,nx))                                     # [-]
    Cw_pflotran = np.zeros((len_t,nx))                                     # [-]
    Cw_tot_pflotran = np.zeros((len_t,nx))                                     # [-]
    Cg_pflotran = np.zeros((len_t,nx))                                     # [-]
    Cg_tot_pflotran = np.zeros((len_t,nx))                                     # [-]
    y_g_mol_pflotran = np.zeros((len_t,nx))                                     # [-]
    y_w_mol_pflotran = np.zeros((len_t,nx))                                     # [-]
    x_g_mol_pflotran = np.zeros((len_t,nx))                                     # [-]
    rho_w_pflotran = np.zeros((len_t,nx))                                     # [-]
    rho_g_pflotran = np.zeros((len_t,nx))                                     # [-]
    rho_w_mol = np.zeros(nx)                                     # [-]
    rho_g_mol = np.zeros(nx)                                     # [-]
    cu_Cw_soln = np.zeros(len_t)
    cu_Cw_pflotran = np.zeros(len_t)

    #a lot of this is redundant and needs to be rationalized
    len_x = r_soln.size
    
    vel_x = np.zeros((len_t,len_x))

    #the dimensionless velocities corresponding to the output time and mesh
    for k in range(len_t):
      vel_x[k,:] = 1/(t_sec[k]*rD_to_rm)*np.power(r_soln,2.0) 

    ##############################################################################      
    # create the analytical solution

    ######################################################################
    # Subfunctions
    ######################################################################
    def find_fw(Sw):
      krg = find_krg(Sw)
      krw = find_krw(Sw)
      Mg = krg/mu_g
      Mw = krw/mu_w
      fw = Mw/(Mw + Mg)
      return fw

    def find_krw(Sw):
      Sel = max(0,(Sw-Srw)/(1-Srw))
      krw = Sel**(3.0+2.0/lam)
      return krw

    def find_krg(Sw):
      Seg = min(1,max(0,(Sw-Srw)/(1-Srw-Srg)))
      krg = (1-Seg)**2*(1-Seg**(1+2.0/lam))
      return krg

    def find_dfw_dSw(Sw):
      Sw_plus = Sw + delta
      fw_plus = find_fw(Sw_plus)
      Sw_minus = Sw - delta
      fw_minus = find_fw(Sw_minus)
      dfw_dSw = (fw_plus-fw_minus)/(Sw_plus-Sw_minus)
      return dfw_dSw

    def find_shockTR(Sw,fw,BDRY):
      #this assumes that the upstream BDRY is single component, single phase, water
      #and will not work otherwise!!
      #notice that shock velocity is based on the GAS component mass balancce
      BDRYg = 1.0 - BDRY
      Gg = find_G_or_alpha(Sw,'g')
      alpha_g = find_G_or_alpha(fw,'g')
      if (1.0 - Sw) == BDRY:
        v_shock = 0.0
      else:
        v_shock = (BDRYg-alpha_g)/(BDRYg-Gg)
      return v_shock

    def find_tanTR(Sw,BDRY):
      fw = find_fw(Sw)
      dfw_dSw = find_dfw_dSw(Sw)
      v_shock = find_shockTR(Sw,fw,BDRY)
      diff = ((dfw_dSw - v_shock)**2)**0.5
      return diff

    def find_shockLD(Sw,vC_D,BDRY):
      #this assumes that the downstream BDRY is single component, single phase, gas
      #and will not work otherwise!!
      fw = find_fw(Sw)
      Gw_A = BDRY
      al_w_A = BDRY
      Gg_A = 1.0*rho_g_pureD*1.0
      al_g_A = Gg_A
      
      Gw_B = find_G_or_alpha(Sw,'w')
      al_w_B = find_G_or_alpha(fw,'w')
      Gg_B = find_G_or_alpha(Sw,'g')
      al_g_B = find_G_or_alpha(fw,'g')
      del_Gw = Gw_A - Gw_B
      del_Gg = Gg_A - Gg_B

      vA_D = vC_D*(al_w_B*del_Gg - al_g_B*del_Gw)/(al_w_A*del_Gg - al_g_A*del_Gw)
      vA_over_vC = vA_D/vC_D
      v_shock = (Gg_A*vA_over_vC-al_g_B)/(al_g_A-Gg_B)
      return v_shock
    
    def find_tanLD(Sw,args_in_min):
      fw = find_fw(Sw)
      vC_D = args_in_min[0]
      IC = args_in_min[1]
      dfw_dSw = find_dfw_dSw(Sw)
      v_shock = find_shockLD(Sw,vC_D,IC)
      diff = ((dfw_dSw - v_shock)**2)**0.5
      return diff

    def find_Sw_xD(Sw,vel):
      dfw_dSw = find_dfw_dSw(Sw)
      diff = ((dfw_dSw - vel)**2)**0.5
      return diff

    def find_G_or_alpha(S_or_f,comp):
      if comp == 'g':
        G_or_alpha = mol_x_g*rho_w_mixD*S_or_f + mol_y_g*rho_g_mixD*(1.0 - S_or_f)
      else:
        G_or_alpha = mol_x_w*rho_w_mixD*S_or_f + mol_y_w*rho_g_mixD*(1.0 - S_or_f)
      return G_or_alpha
  
    def find_Cw_or_Cg(Sw,comp):
      if Sw <= 0.0:
        #assumes pure gas condition
        if comp == 'w':
          Gj = 0.0*rho_g_pureD*(1.0 - Sw)
        else:
          Gj = 1.0*rho_g_pureD*(1.0 - Sw)
        z_bot = rho_g_pureD
      elif Sw < 1.0:
        Gj = find_G_or_alpha(Sw,comp)
        z_bot = rho_w_mixD*Sw + rho_g_mixD*(1.0 - Sw)
      else:
        #assumes pure water condition
        if comp == 'w':
          Gj = 1.0*rho_w_pureD*Sw
        else:
          Gj = 0.0*rho_w_pureD*Sw
        z_bot = rho_w_pureD
      Cj = Gj/z_bot
      return Cj

    def find_Cwtot_or_Cgtot(Sw,comp):
      Cj = find_Cw_or_Cg(Sw,comp)
      if Sw <= 0.0:
        #assumes pure gas condition
        bot = rho_g_pure
      elif Sw < 1.0:
        bot = rho_w_mix*Sw + rho_g_mix*(1.0 - Sw)
      else:
        #assumes pure water condition
        bot = rho_w_pure
      Cjtot = phi*h*math.pi*Cj*bot 
      return Cjtot


    #####################################################################
    # Main program
    ######################################################################

    ######################################################################
    #this section finds the shocks in saturation

    #first get an approximate of the upper and lower bounds on water saturation
    #in the solution
    Sw_now = np.linspace(0.0,1.0,101,True,False)
    len_Sw =  Sw_now.size
    fw_now = np.zeros(len_Sw)
    dfw_dSw = np.zeros(len_Sw)

    for i in range(len_Sw):
      fw_now[i] = find_fw(Sw_now[i])
      dfw_dSw[i] = find_dfw_dSw(Sw_now[i])
      #the correct tangent shock must be where dfw_dSw is not too small and the maximum of dfw_dSw
      if i > 0:
        if dfw_dSw[i] > 0.8 and dfw_dSw[i-1] < 0.8:
          pos_tan_S_index = i-1
          pos_tan_Sw = Sw_now[i-1]

    #find the approximate maximum of dw_dSw
    max_dfw_dSw = np.amax(dfw_dSw)
    max_v_index = np.argmax(dfw_dSw)
    Sw_max_v = Sw_now[max_v_index]

    #FIND THE TRAILING SHOCK
    #set the upper and lower bounds on Sw to put into the iterative solver 
    Sw_bdry = [(Srw, 1.0 - Srg-1e-4)]
    Sw_guess = 0.95
    #scipy solver to find the tangent shock where (fw-JC)/(Sw-JC)=dfw_dSw
    Sw_tan_infoJC = minimize(find_tanTR,Sw_guess,JC,method='TNC',bounds=Sw_bdry,tol=conv_tol)

    #split output into a usable form and sanity check
    Sw_tanJC = Sw_tan_infoJC.x
    fw_tanJC = find_fw(Sw_tanJC)
    Gw_tanJC = find_G_or_alpha(Sw_tanJC,'w')
    al_w_tanJC = find_G_or_alpha(fw_tanJC,'w')
    dfw_dSw_tanJC = find_dfw_dSw(Sw_tanJC)
    v_shock_tanJC = find_shockTR(Sw_tanJC,fw_tanJC,JC)
    if abs(dfw_dSw_tanJC - v_shock_tanJC) > 1e-3:
      print 'error! the trailing shock solver did not converge!'
      print abs(dfw_dSw_tanJC - v_shock_tanJC)
      sys.exit(0)

    #find the two-phase dimensionless shock velocity
    Gw_D = JC*rho_w_pureD*1.0
    al_w_D = Gw_D
    Gg_D = (1.0-JC)*rho_w_pureD*1.0
    al_g_D = Gg_D
    Gw_C = Gw_tanJC
    al_w_C = al_w_tanJC
    Gg_C = find_G_or_alpha(Sw_tanJC,'g')
    al_g_C = find_G_or_alpha(fw_tanJC,'g')
    del_Gw = Gw_D - Gw_C
    del_Gg = Gg_D - Gg_C
    vC_D = (al_g_D*del_Gw - al_w_D*del_Gg)/(al_g_C*del_Gw - al_w_C*del_Gg)

    #FIND THE LEADING SHOCK
    #set the upper and lower bounds on Sw to put into the iterative solver 
    Sw_bdry = [(Sw_max_v,1.0-Srg)]
    Sw_guess = 0.88
    args_in_min = np.array([vC_D,IC])
    #scipy solver to find the tangent shock where (fw-IC)/(Sw-IC)=dfw_dSw
    Sw_tan_infoIC = minimize(find_tanLD,Sw_guess,args_in_min,method='TNC',bounds=Sw_bdry,tol=conv_tol)
    #split output into a usable form and sanity check
    Sw_tanIC = Sw_tan_infoIC.x
    fw_tanIC = find_fw(Sw_tanIC)
    dfw_dSw_tanIC = find_dfw_dSw(Sw_tanIC)
    v_shock_tanIC = find_shockLD(Sw_tanIC,vC_D,IC)
    if abs(dfw_dSw_tanIC - v_shock_tanIC) > 1e-3:
      print 'error! the shock solver did not converge!'
      sys.exit(0)

    #find the two-phase dimensionless shock velocity
    Gw_A = IC
    al_w_A = IC
    Gg_A = 1.0*rho_g_pureD*1.0
    al_g_A = Gg_A

    Gw_B = find_G_or_alpha(Sw_tanIC,'w')
    al_w_B = find_G_or_alpha(fw_tanIC,'w')
    Gg_B = find_G_or_alpha(Sw_tanIC,'g')
    al_g_B = find_G_or_alpha(fw_tanIC,'g')

    del_Gw = Gw_A - Gw_B
    del_Gg = Gg_A - Gg_B
    vA_D = vC_D*(al_g_B*del_Gw - al_w_B*del_Gg)/(al_g_A*del_Gw - al_w_A*del_Gg)

    ######################################################################
    # finally finding Sw on the grid for each timestep

    #all two-phase Sw must be between the Sw_tanJC and the saturation at the tangent shock
    Sw_xD_bdry = [(Sw_tanIC,Sw_tanJC)]
    #the midpoint is a good first guess
    Sw_xD_guess = (Sw_tanJC+Sw_tanIC)/2.0

    for k in range(len_t):
      for i in range(len_x):
        if vel_x[k,i] < v_shock_tanJC:
          # for velocities slower than the trailing shock the saturation is the injection composition
          Sw_soln[k,i] = 1.0 + 1.e-5
          Cw_soln[k,i] = find_Cw_or_Cg(Sw_soln[k,i],'w')
          Cg_soln[k,i] = find_Cw_or_Cg(Sw_soln[k,i],'g')
          Cgtot_soln[k,i] = find_Cwtot_or_Cgtot(Sw_soln[k,i],'g')*(r_edges[i+1]**2.0 - r_edges[i]**2.0)
          Cwtot_soln[k,i] = find_Cwtot_or_Cgtot(Sw_soln[k,i],'w')*(r_edges[i+1]**2.0 - r_edges[i]**2.0)
        elif vel_x[k,i] < vC_D*v_shock_tanIC:
          # scipy solver to find the Sw corresponding to the dimensionless velocity behind the shock front
          Sw_xD_info = minimize(find_Sw_xD,Sw_xD_guess,vel_x[k,i]/vC_D,method='TNC',bounds=Sw_xD_bdry,tol=conv_tol)
          #split output into a usable form and sanity check
          Sw_soln[k,i] = Sw_xD_info.x
          Cw_soln[k,i] = find_Cw_or_Cg(Sw_soln[k,i],'w')
          Cg_soln[k,i] = find_Cw_or_Cg(Sw_soln[k,i],'g')
          Cgtot_soln[k,i] = find_Cwtot_or_Cgtot(Sw_soln[k,i],'g')*(r_edges[i+1]**2.0 - r_edges[i]**2.0)
          Cwtot_soln[k,i] = find_Cwtot_or_Cgtot(Sw_soln[k,i],'w')*(r_edges[i+1]**2.0 - r_edges[i]**2.0)
          dfw_dSw_xD = find_dfw_dSw(Sw_soln[k,i])
          err = dfw_dSw_xD - vel_x[k,i]/vC_D
          if abs(err) > 1e-3:
            print 'error! the solver for Sw_xD did not converge!'
            print Sw_soln[k,i]
            print vel_x[k,i]
            print err
            sys.exit(0)
        else: 
          # for velocities faster than the leading shock the saturation is the initial composition
          Sw_soln[k,i] = -1.e-5
          Cw_soln[k,i] = find_Cw_or_Cg(Sw_soln[k,i],'w')
          Cg_soln[k,i] = find_Cw_or_Cg(Sw_soln[k,i],'g')
          Cgtot_soln[k,i] = find_Cwtot_or_Cgtot(Sw_soln[k,i],'g')*(r_edges[i+1]**2.0 - r_edges[i]**2.0)
          Cwtot_soln[k,i] = find_Cwtot_or_Cgtot(Sw_soln[k,i],'w')*(r_edges[i+1]**2.0 - r_edges[i]**2.0)

      cu_Cw_soln[k] = np.sum(Cwtot_soln[k,:])

    ##############################################################################      
    # run PFLOTRAN simulation
    
    run_pflotran(input_prefix,nxyz,dxyz,lxyz,remove,screen_on,pf_exe,mpi_exe)
    
    # load data from HDF5
    index_string = 'Time:  1.00000E-02 y/Liquid_Saturation'
    Sw_pflotran[0,:] = read_pflotran_output_1D(path+
		      '/1D_RAD_BL_GENERAL_COMP_WATER_INJ.h5',index_string,False)
    index_string = 'Time:  1.00000E-01 y/Liquid_Saturation'
    Sw_pflotran[1,:] = read_pflotran_output_1D(path+
		      '/1D_RAD_BL_GENERAL_COMP_WATER_INJ.h5',index_string,False)
    index_string = 'Time:  5.00000E-01 y/Liquid_Saturation'
    Sw_pflotran[2,:] = read_pflotran_output_1D(path+
		      '/1D_RAD_BL_GENERAL_COMP_WATER_INJ.h5',index_string,False)
    index_string = 'Time:  1.00000E+00 y/Liquid_Saturation'
    Sw_pflotran[3,:] = read_pflotran_output_1D(path+
		      '/1D_RAD_BL_GENERAL_COMP_WATER_INJ.h5',index_string,remove)
    ierr = check(Sw_pflotran)

    # load data from HDF5
    index_string = 'Time:  1.00000E-02 y/X_g^l'
    x_g_mol_pflotran[0,:] = read_pflotran_output_1D(path+
		      '/1D_RAD_BL_GENERAL_COMP_WATER_INJ.h5',index_string,False)
    index_string = 'Time:  1.00000E-01 y/X_g^l'
    x_g_mol_pflotran[1,:] = read_pflotran_output_1D(path+
		      '/1D_RAD_BL_GENERAL_COMP_WATER_INJ.h5',index_string,False)
    index_string = 'Time:  5.00000E-01 y/X_g^l'
    x_g_mol_pflotran[2,:] = read_pflotran_output_1D(path+
		      '/1D_RAD_BL_GENERAL_COMP_WATER_INJ.h5',index_string,False)
    index_string = 'Time:  1.00000E+00 y/X_g^l'
    x_g_mol_pflotran[3,:] = read_pflotran_output_1D(path+
		      '/1D_RAD_BL_GENERAL_COMP_WATER_INJ.h5',index_string,remove)
    ierr = check(x_g_mol_pflotran)


    # load data from HDF5
    index_string = 'Time:  1.00000E-02 y/X_l^g'
    y_w_mol_pflotran[0,:] = read_pflotran_output_1D(path+
		      '/1D_RAD_BL_GENERAL_COMP_WATER_INJ.h5',index_string,False)
    index_string = 'Time:  1.00000E-01 y/X_l^g'
    y_w_mol_pflotran[1,:] = read_pflotran_output_1D(path+
		      '/1D_RAD_BL_GENERAL_COMP_WATER_INJ.h5',index_string,False)
    index_string = 'Time:  5.00000E-01 y/X_l^g'
    y_w_mol_pflotran[2,:] = read_pflotran_output_1D(path+
		      '/1D_RAD_BL_GENERAL_COMP_WATER_INJ.h5',index_string,False)
    index_string = 'Time:  1.00000E+00 y/X_l^g'
    y_w_mol_pflotran[3,:] = read_pflotran_output_1D(path+
		      '/1D_RAD_BL_GENERAL_COMP_WATER_INJ.h5',index_string,remove)
    ierr = check(y_w_mol_pflotran)

    y_g_mol_pflotran = 1.0 - y_w_mol_pflotran
    x_w_mol_pflotran = 1.0 - x_g_mol_pflotran

    # load data from HDF5
    index_string = 'Time:  1.00000E-02 y/Gas_Density [kg_m^3]'
    rho_g_pflotran[0,:] = read_pflotran_output_1D(path+
		      '/1D_RAD_BL_GENERAL_COMP_WATER_INJ.h5',index_string,False)
    index_string = 'Time:  1.00000E-01 y/Gas_Density [kg_m^3]'
    rho_g_pflotran[1,:] = read_pflotran_output_1D(path+
		      '/1D_RAD_BL_GENERAL_COMP_WATER_INJ.h5',index_string,False)
    index_string = 'Time:  5.00000E-01 y/Gas_Density [kg_m^3]'
    rho_g_pflotran[2,:] = read_pflotran_output_1D(path+
		      '/1D_RAD_BL_GENERAL_COMP_WATER_INJ.h5',index_string,False)
    index_string = 'Time:  1.00000E+00 y/Gas_Density [kg_m^3]'
    rho_g_pflotran[3,:] = read_pflotran_output_1D(path+
		      '/1D_RAD_BL_GENERAL_COMP_WATER_INJ.h5',index_string,remove)
    ierr = check(rho_g_pflotran)

    # load data from HDF5
    index_string = 'Time:  1.00000E-02 y/Liquid_Density [kg_m^3]'
    rho_w_pflotran[0,:] = read_pflotran_output_1D(path+
		      '/1D_RAD_BL_GENERAL_COMP_WATER_INJ.h5',index_string,False)
    index_string = 'Time:  1.00000E-01 y/Liquid_Density [kg_m^3]'
    rho_w_pflotran[1,:] = read_pflotran_output_1D(path+
		      '/1D_RAD_BL_GENERAL_COMP_WATER_INJ.h5',index_string,False)
    index_string = 'Time:  5.00000E-01 y/Liquid_Density [kg_m^3]'
    rho_w_pflotran[2,:] = read_pflotran_output_1D(path+
		      '/1D_RAD_BL_GENERAL_COMP_WATER_INJ.h5',index_string,False)
    index_string = 'Time:  1.00000E+00 y/Liquid_Density [kg_m^3]'
    rho_w_pflotran[3,:] = read_pflotran_output_1D(path+
		      '/1D_RAD_BL_GENERAL_COMP_WATER_INJ.h5',index_string,remove)
    ierr = check(rho_w_pflotran)

    for k in range(len_t):
      for i in range(len_x):
        if MW_h2o*y_w_mol_pflotran[k,i] + MW_air*y_g_mol_pflotran[k,i] > 0.0: #if no gas is present the mol fractions are all zero
          rho_g_mol[i] = 1000*rho_g_pflotran[k,i]/(y_g_mol_pflotran[k,i]*MW_air + y_w_mol_pflotran[k,i]*MW_h2o) 
        else:
          rho_g_mol[i] = 1000.0*rho_g_pflotran[i]/MW_h2o
        if MW_h2o*x_w_mol_pflotran[k,i] + MW_air*x_g_mol_pflotran[k,i] > 0.0: # in case the same thing happens when no water is present
          rho_w_mol[i] = 1000*rho_w_pflotran[k,i]/(x_g_mol_pflotran[k,i]*MW_air + x_w_mol_pflotran[k,i]*MW_h2o) 
        else:
          rho_w_mol[i] = 1000.0*rho_w_pflotran[k,i]/MW_air
        bot = rho_w_mol[i]*Sw_pflotran[k,i] + rho_g_mol[i]*(1.0 - Sw_pflotran[k,i])
        Cw_pflotran[k,i] = (x_w_mol_pflotran[k,i]*rho_w_mol[i]*Sw_pflotran[k,i] + y_w_mol_pflotran[k,i]*rho_g_mol[i]*(1.0 - Sw_pflotran[k,i]))/bot
        Cg_pflotran[k,i] = (x_g_mol_pflotran[k,i]*rho_w_mol[i]*Sw_pflotran[k,i] + y_g_mol_pflotran[k,i]*rho_g_mol[i]*(1.0 - Sw_pflotran[k,i]))/bot
        Cw_tot_pflotran[k,i] = Cw_pflotran[k,i]*h*math.pi*phi*(r_edges[i+1]**2.0 - r_edges[i]**2.0)*bot
        Cg_tot_pflotran[k,i] = Cg_pflotran[k,i]*h*math.pi*phi*(r_edges[i+1]**2.0 - r_edges[i]**2.0)*bot

      cu_Cw_pflotran[k] = np.sum(Cw_tot_pflotran[k,:])

    L1_percent_error = calc_L1_relative_error(Cwtot_soln[3,:],Cw_tot_pflotran[3,:],ierr)
    record_error(error_analysis,nxyz_record,dxyz_record,L1_percent_error,
                 nxyz,dxyz,try_count)

    test_pass = does_pass(L1_percent_error,try_count,num_tries)
    nxyz[0] = nxyz[0]*2.
    dxyz = lxyz/nxyz

  # Plot the PFLOTRAN and analytical solutions
  plot_1D_transient(path,t_yr,'yr',r_soln,Cwtot_soln,x_pflotran,Cw_tot_pflotran,
                   'Radial distance [m]','Total water [mol]',"{0:.2f}".format(L1_percent_error))
  
  # Plot error analysis
  plot_error(error_analysis,nxyz_record,dxyz_record,path,try_count,1)
  
  # Add test result to report card
  add_to_report(path,test_pass,L1_percent_error,ierr)

  return;


#==============================================================================#
def BL_1D_MPHASE_COMP_CO2_INJ(path,input_prefix,remove,screen_on,pf_exe,
				                             mpi_exe,num_tries):
#==============================================================================#
# The compositional Buckley-Leverett solution including volume change as
# components partition between phases. Presented in:
# "Theory of gas injection processes"
# Section 4.4
# F. M Orr, Jr
# Tie-Line publications, Copenhagen Denmark
# Author: Tara LaForce
# Date: 09/27/2018
# Note: This example tests the numerical solver, not the phase behavior model.
# Component phase partitioning and phase properties from the simulator are
# hard-coded into the analytical solution  
# ******************************************************************************

  nxyz = np.zeros(3) + 1
  dxyz = np.zeros(3) + 1.
  lxyz = np.zeros(3) + 1.
  error_analysis = np.zeros(num_tries)
  dxyz_record = np.zeros((num_tries,3))
  nxyz_record = np.zeros((num_tries,3))
  test_pass = False
  try_count = 0

  # initial discretization values
  lxyz[0] = 5.              # [m] lx
  nxyz[0] = 120               # [m] nx
  dxyz = lxyz/nxyz           # [m]
  ierr = 0

  #####################################################################
  # problem parameters
  #####################################################################
  # MPHASE fluid parameters
  #all taken from mphase simulation output
  lam = 0.8 #lambda in the BURDINE_BC liquid and gas relative permeabilities (-)
  Srw = 0.1 #residual liquid sauration in the liquid relative permeability (-)
  Srg = 0.1 #residual gas saturation in the gas relative permeability (-)
  MW_h2o = 18.02 #g/mol
  MW_co2 = 44.01 #g/mol
  rho_w_pure_mass = 9.924e2 #water density (kg/m^3) at the initial condition
  rho_w_mix_mass = 1.002e3 #water density (kg/m^3)
  rho_g_pure_mass = 3.790e2#gas density (kg/m^3)
  rho_g_mix_mass = 3.848e2#gas density (kg/m^3)
  mu_w = 5.41e-4#water viscosity (Pa-s) 
  mu_g = 2.8e-5 #gas viscosity (Pa-s)
  mol_x_w = 0.97994 #mol fraction of water in the liquid phase
  mol_x_g = 1.0 - mol_x_w  #mol fraction of CO2 in the liquid phase
  mol_y_w = 1.25e-3  #mol fraction of water in the gas phase
  mol_y_g = 1.0 - mol_y_w  #mol fraction of CO2 in the gas phase
  #solid parameters
  domain_m = lxyz
  A = 1.0 #cross sectional area of the domain (m^2)
  phi = 0.25 #porosity (-)
  q_mass = 8e-3 #gas mass injection rate  (g/s)
  Init_water = 1.0 #water saturation must be 100% in analytical solution
  Inj = 0.0 #water saturation must be 0% in analytical solution
  #some tolerances
  delta = 1e-7 #step size for approximating derivatives
  conv_tol = 1e-4 #tolerance for convergencce of iterative processes

  #convert densities to mol fractions
  rho_w_pure = 1000*rho_w_pure_mass/MW_h2o #water density (mol/L)
  rho_g_pure = 1000*rho_g_pure_mass/MW_co2 #gas density (mol/L)
  rho_w_mix = 1000*rho_w_mix_mass/(mol_x_g*MW_co2 + mol_x_w*MW_h2o) 
  rho_g_mix = 1000*rho_g_mix_mass/(mol_y_g*MW_co2 + mol_y_w*MW_h2o)

  #cast density into dimensionless terms
  rho_w_pureD = rho_w_pure/rho_g_pure
  rho_g_pureD = rho_g_pure/rho_g_pure
  rho_w_mixD = rho_w_mix/rho_g_pure
  rho_g_mixD = rho_g_mix/rho_g_pure

  #the initial and injection conditions cast as G_w
  #component 1 is WATER, component 2 is GAS. Conditions are in terms of water content
  IC = 1.0*rho_w_pureD*Init_water
  JC = 1.0*rho_g_pureD*Inj

  #solution times
  t_yr = np.array([0.01,0.10,0.5,0.80])                     # years
  #for calculating dimenssionless variables
  q_vol = 1.0e-3*q_mass/rho_g_pure_mass #volumetric injection rate (m^3/s) for MPHASE problem
  xD_to_xm = q_vol/(phi*A) #to convert from dimensionless distance to x in m and vice versa

  len_t = t_yr.size
  t_sec = np.zeros(len_t)
  t_sec = 3600*24*365.25*t_yr #simulation time (sec)
  
  while (not test_pass) and (try_count < num_tries): 
    print_discretization(lxyz,nxyz,dxyz)
    nx = int(nxyz[0]); ny = int(nxyz[1]); nz = int(nxyz[2])
    dx = dxyz[0]; dy = dxyz[1]; dz = dxyz[2]
    Lx = lxyz[0]; Ly = lxyz[1]; Lz = lxyz[2]
    try_count = try_count + 1

    x_soln = np.linspace(0.+(dx/2.),Lx-(dx/2.),nx)  # [m]
    x_edges = np.linspace(0.,Lx,nx+1)  # [m]
    Sw_soln = np.zeros((len_t,(nx)))                               # [-]
    Cg_soln = np.zeros((len_t,(nx)))                               # [-]
    Cgtot_soln = np.zeros((len_t,(nx)))                               # [-]
    x_pflotran = x_soln[0:nx]                       # [m]
    Sw_pflotran = np.zeros((len_t,nx))                                     # [-]
    Cg_pflotran = np.zeros((len_t,nx))                                     # [-]
    Cg_tot_pflotran = np.zeros((len_t,nx))                                     # [-]
    y_g_mol_pflotran = np.zeros((len_t,nx))                                     # [-]
    y_w_mol_pflotran = np.zeros((len_t,nx))                                     # [-]
    x_g_mol_pflotran = np.zeros((len_t,nx))                                     # [-]
    rho_w_pflotran = np.zeros((len_t,nx))                                     # [-]
    rho_g_pflotran = np.zeros((len_t,nx))                                     # [-]
    rho_w_mol = np.zeros(nx)                                     # [-]
    rho_g_mol = np.zeros(nx)                                     # [-]
    cu_Cg_soln = np.zeros(len_t)
    cu_Cg_pflotran = np.zeros(len_t)

    #a lot of this is redundant and needs to be rationalized
    len_x = x_soln.size
    
    vel_x = np.zeros((len_t,len_x))

    #the dimensionless velocities corresponding to the output time and mesh
    for k in range(len_t):
      vel_x[k,:] = 1/(t_sec[k]*xD_to_xm)*x_soln 

    ##############################################################################      
    # create the analytical solution

    ######################################################################
    # Subfunctions
    ######################################################################
    def find_fw(Sw):
      krg = find_krg(Sw)
      krw = find_krw(Sw)
      Mg = krg/mu_g
      Mw = krw/mu_w
      fw = Mw/(Mw + Mg)
      return fw

    def find_krw(Sw):
      Sel = min(1,max(0.0,(Sw-Srw)/(1.0-Srw)))
      sqrtSe = np.power(Sel,0.5)
      inner_part = 1.0 - np.power(Sel,1/lam)
      outer_part = 1.0 - np.power(inner_part,lam)
      krw = sqrtSe*np.power(outer_part,2.0)
      return krw

    def find_krg(Sw):
      Seg = min(1,max(0,(Sw-Srw)/(1.0-Srw-Srg)))
      krg = ((1.0-Seg)**2)*(1.0-Seg**2)
      return krg

    def find_dfw_dSw(Sw):
      Sw_plus = Sw + delta
      fw_plus = find_fw(Sw_plus)
      Sw_minus = Sw - delta
      fw_minus = find_fw(Sw_minus)
      dfw_dSw = (fw_plus-fw_minus)/(Sw_plus-Sw_minus)
      return dfw_dSw

    def find_shockTR(Sw,fw,BDRY):
      #this assumes that the upstream BDRY is single component, single phase, gas
      #and will not work otherwise!!
      Gw = find_G_or_alpha(Sw,'w')
      alpha_w = find_G_or_alpha(fw,'w')
      if (1.0 - Sw) == BDRY:
        v_shock = 0.0
      else:
        v_shock = (BDRY-alpha_w)/(BDRY-Gw)
      return v_shock

    def find_tanTR(Sw,BDRY):
      fw = find_fw(Sw)
      dfw_dSw = find_dfw_dSw(Sw)
      v_shock = find_shockTR(Sw,fw,BDRY)
      diff = ((dfw_dSw - v_shock)**2)**0.5
      return diff

    def find_shockLD(Sw,vC_D,BDRY):
      #this assumes that the downstream BDRY is single component, single phase, water
      #and will not work otherwise!!
      fw = find_fw(Sw)
      Gw_A = BDRY
      al_w_A = BDRY
      Gg_A = 0.0*rho_w_pureD*1.0
      al_g_A = Gg_A
      Gw_B = find_G_or_alpha(Sw,'w')
      al_w_B = find_G_or_alpha(fw,'w')
      Gg_B = find_G_or_alpha(Sw,'g')
      al_g_B = find_G_or_alpha(fw,'g')
      del_Gw = Gw_A - Gw_B
      del_Gg = Gg_A - Gg_B
      vA_D = vC_D*(al_w_B*del_Gg - al_g_B*del_Gw)/(al_w_A*del_Gg - al_g_A*del_Gw)
      vA_over_vC = vA_D/vC_D
      v_shock = (BDRY*vA_over_vC-al_w_B)/(BDRY-Gw_B)  
      return v_shock

    def find_tanLD(Sw,args_in_min):
      fw = find_fw(Sw)
      vC_D = args_in_min[0]
      IC = args_in_min[1]
      dfw_dSw = find_dfw_dSw(Sw)
      v_shock = find_shockLD(Sw,vC_D,IC)
      diff = ((dfw_dSw - v_shock)**2)**0.5
      return diff

    def find_Sw_xD(Sw,vel):
      dfw_dSw = find_dfw_dSw(Sw)
      diff = ((dfw_dSw - vel)**2)**0.5
      return diff

    def find_G_or_alpha(S_or_f,comp):
      if comp == 'g':
        G_or_alpha = mol_x_g*rho_w_mixD*S_or_f + mol_y_g*rho_g_mixD*(1.0 - S_or_f)
      else:
        G_or_alpha = mol_x_w*rho_w_mixD*S_or_f + mol_y_w*rho_g_mixD*(1.0 - S_or_f)
      return G_or_alpha

    def find_Cw_or_Cg(Sw,comp):
      if Sw <= 0.0:
        #assumes pure gas condition
        if comp == 'w':
          Gj = 0.0*rho_g_pureD*(1.0 - Sw)
        else:
          Gj = 1.0*rho_g_pureD*(1.0 - Sw)
        z_bot = rho_g_pureD
      elif Sw < 1.0:
        Gj = find_G_or_alpha(Sw,comp)
        z_bot = rho_w_mixD*Sw + rho_g_mixD*(1.0 - Sw)
      else:
        #assumes pure water condition
        if comp == 'w':
          Gj = 1.0*rho_w_pureD*Sw
        else:
          Gj = 0.0*rho_w_pureD*Sw
        z_bot = rho_w_pureD
      Cj = Gj/z_bot
      return Cj

    def find_Cwtot_or_Cgtot(Sw,comp):
      Cj = find_Cw_or_Cg(Sw,comp)
      if Sw <= 0.0:
        #assumes pure gas condition
        bot = rho_g_pure
      elif Sw < 1.0:
        bot = rho_w_mix*Sw + rho_g_mix*(1.0 - Sw)
      else:
        #assumes pure water condition
        bot = rho_w_pure
      Cjtot = phi*A*Cj*bot 
      return Cjtot

    #####################################################################
    # Main program
    ######################################################################

    ######################################################################
    #this section finds the shocks in saturation

    #first get an approximate of the upper and lower bounds on water saturation at the shock
    Sw_now = np.linspace(0.0,1.0,101,True,False)
    len_Sw =  Sw_now.size
    dfw_dSw = np.zeros(len_Sw)

    for i in range(len_Sw):
      dfw_dSw[i] = find_dfw_dSw(Sw_now[i])
      #the correct tangent shock must be where dfw_dSw is relatively clsoe to 1 and
      #the maximum of dfw_dSw
      if i > 0:
        if dfw_dSw[i] > 0.8 and dfw_dSw[i-1] < 0.8:
          pos_tan_S_index = i-1
          pos_tan_Sw = Sw_now[i-1]

    #find the approximate maximum of dw_dSw
    max_dfw_dSw = np.amax(dfw_dSw)
    max_v_index = np.argmax(dfw_dSw)
    Sw_max_v = Sw_now[max_v_index]

    #FIND THE TRAILING SHOCK
    #set the upper and lower bounds on Sw to put into the iterative solver 
    Sw_bdry = [(0.0, pos_tan_Sw)]
    #the midpoint of the interval is a good first guess
    Sw_guess = (0.0 + pos_tan_Sw)/2.0
    #scipy solver to find the tangent shock where (fw-JC)/(Sw-JC)=dfw_dSw
    Sw_tan_infoJC = minimize(find_tanTR,Sw_guess,JC,method='L-BFGS-B',bounds=Sw_bdry,tol=conv_tol)

    #split output into a usable form and sanity check
    Sw_tanJC = Sw_tan_infoJC.x
    fw_tanJC = find_fw(Sw_tanJC)
    dfw_dSw_tanJC = find_dfw_dSw(Sw_tanJC)
    v_shock_tanJC = find_shockTR(Sw_tanJC,fw_tanJC,JC)
    if abs(dfw_dSw_tanJC - v_shock_tanJC) > 1e-3:
      print 'error! the shock solver did not converge!'
      print abs(dfw_dSw_tanJC - v_shock_tanJC)
      sys.exit(0)

    #find the two-phase dimensionless shock velocity
    Gw_D = JC
    al_w_D = JC
    Gg_D = 1.0*rho_g_pureD*1.0
    al_g_D = Gg_D
    Gw_C = find_G_or_alpha(Sw_tanJC,'w')
    al_w_C = find_G_or_alpha(fw_tanJC,'w')
    Gg_C = find_G_or_alpha(Sw_tanJC,'g')
    al_g_C = find_G_or_alpha(fw_tanJC,'g')
    del_Gw = Gw_D - Gw_C
    del_Gg = Gg_D - Gg_C
    vC_D = (al_g_D*del_Gw - al_w_D*del_Gg)/(al_g_C*del_Gw - al_w_C*del_Gg)

    #FIND THE LEADING SHOCK
    #set the upper and lower bounds on Sw to put into the iterative solver 
    Sw_bdry = [(pos_tan_Sw,Sw_max_v)]
    #the midpoint of the interval is a good first guess
    Sw_guess = (pos_tan_Sw+Sw_max_v)/2.0
    args_in_min = np.array([vC_D,IC])
    #scipy solver to find the tangent shock where (fw-IC)/(Sw-IC)=dfw_dSw
    Sw_tan_infoIC = minimize(find_tanLD,Sw_guess,args_in_min,method='L-BFGS-B',bounds=Sw_bdry,tol=conv_tol)
    #split output into a usable form and sanity check
    Sw_tanIC = Sw_tan_infoIC.x
    dfw_dSw_tanIC = find_dfw_dSw(Sw_tanIC)
    v_shock_tanIC = find_shockLD(Sw_tanIC,vC_D,IC)
    if abs(dfw_dSw_tanIC - v_shock_tanIC) > 1e-3:
      print 'error! the shock solver did not converge!'
      sys.exit(0)

    ######################################################################
    # finally finding Sw on the grid for each timestep

    #all two-phase Sw must be between the Sw_tanJC and the saturation at the tangent shock
    Sw_xD_bdry = [(Sw_tanJC,Sw_tanIC)]
    #the midpoint is a good first guess
    Sw_xD_guess = (Srw+Sw_tanIC)/2.0

    for k in range(len_t):
      for i in range(len_x):
        if vel_x[k,i] < v_shock_tanJC:
          # for velocities slower than the trailing shock the saturation is the injection composition
          Sw_soln[k,i] = 1.e-5
          Cg_soln[k,i] = find_Cw_or_Cg(Sw_soln[k,i],'g')
          Cgtot_soln[k,i] = find_Cwtot_or_Cgtot(Sw_soln[k,i],'g')*(x_edges[i+1] - x_edges[i])
        elif vel_x[k,i] < vC_D*v_shock_tanIC:
          # scipy solver to find the Sw corresponding to the dimensionless velocity behind the shock front
          Sw_xD_info = minimize(find_Sw_xD,Sw_xD_guess,vel_x[k,i]/vC_D,method='L-BFGS-B',bounds=Sw_xD_bdry,tol=conv_tol)
          #split output into a usable form and sanity check
          Sw_soln[k,i] = Sw_xD_info.x
          Cg_soln[k,i] = find_Cw_or_Cg(Sw_soln[k,i],'g')
          Cgtot_soln[k,i] = find_Cwtot_or_Cgtot(Sw_soln[k,i],'g')*(x_edges[i+1] - x_edges[i])
        else: 
          # for velocities faster than the leading shock the saturation is the initial composition
          Sw_soln[k,i] = 1.000001
          Cg_soln[k,i] = find_Cw_or_Cg(Sw_soln[k,i],'g')
          Cgtot_soln[k,i] = find_Cwtot_or_Cgtot(Sw_soln[k,i],'g')*(x_edges[i+1] - x_edges[i])
      cu_Cg_soln[k] = np.sum(Cgtot_soln[k,:])

    ##############################################################################      
    # run PFLOTRAN simulation
    
    run_pflotran(input_prefix,nxyz,dxyz,lxyz,remove,screen_on,pf_exe,mpi_exe)
    # load data from HDF5
    index_string = 'Time:  1.00000E-02 y/Liquid_Saturation'
    Sw_pflotran[0,:] = read_pflotran_output_1D(path+
		      '/1D_BL_MPHASE_CO2_INJ.h5',index_string,False)
    index_string = 'Time:  1.00000E-01 y/Liquid_Saturation'
    Sw_pflotran[1,:] = read_pflotran_output_1D(path+
		      '/1D_BL_MPHASE_CO2_INJ.h5',index_string,False)
    index_string = 'Time:  5.00000E-01 y/Liquid_Saturation'
    Sw_pflotran[2,:] = read_pflotran_output_1D(path+
		      '/1D_BL_MPHASE_CO2_INJ.h5',index_string,False)
    index_string = 'Time:  8.00000E-01 y/Liquid_Saturation'
    Sw_pflotran[3,:] = read_pflotran_output_1D(path+
		      '/1D_BL_MPHASE_CO2_INJ.h5',index_string,remove)
    ierr = check(Sw_pflotran)
   
    # load data from HDF5
    index_string = 'Time:  1.00000E-02 y/Liquid_Mole_Fraction_CO2'
    x_g_mol_pflotran[0,:] = read_pflotran_output_1D(path+
		      '/1D_BL_MPHASE_CO2_INJ.h5',index_string,False)
    index_string = 'Time:  1.00000E-01 y/Liquid_Mole_Fraction_CO2'
    x_g_mol_pflotran[1,:] = read_pflotran_output_1D(path+
		      '/1D_BL_MPHASE_CO2_INJ.h5',index_string,False)
    index_string = 'Time:  5.00000E-01 y/Liquid_Mole_Fraction_CO2'
    x_g_mol_pflotran[2,:] = read_pflotran_output_1D(path+
		      '/1D_BL_MPHASE_CO2_INJ.h5',index_string,False)
    index_string = 'Time:  8.00000E-01 y/Liquid_Mole_Fraction_CO2'
    x_g_mol_pflotran[3,:] = read_pflotran_output_1D(path+
		      '/1D_BL_MPHASE_CO2_INJ.h5',index_string,remove)
    ierr = check(x_g_mol_pflotran)

    # load data from HDF5
    index_string = 'Time:  1.00000E-02 y/Gas_Mole_Fraction_H2O'
    y_w_mol_pflotran[0,:] = read_pflotran_output_1D(path+
		      '/1D_BL_MPHASE_CO2_INJ.h5',index_string,False)
    index_string = 'Time:  1.00000E-01 y/Gas_Mole_Fraction_H2O'
    y_w_mol_pflotran[1,:] = read_pflotran_output_1D(path+
		      '/1D_BL_MPHASE_CO2_INJ.h5',index_string,False)
    index_string = 'Time:  5.00000E-01 y/Gas_Mole_Fraction_H2O'
    y_w_mol_pflotran[2,:] = read_pflotran_output_1D(path+
		      '/1D_BL_MPHASE_CO2_INJ.h5',index_string,False)
    index_string = 'Time:  8.00000E-01 y/Gas_Mole_Fraction_H2O'
    y_w_mol_pflotran[3,:] = read_pflotran_output_1D(path+
		      '/1D_BL_MPHASE_CO2_INJ.h5',index_string,remove)
    ierr = check(y_w_mol_pflotran)


    y_g_mol_pflotran = 1.0 - y_w_mol_pflotran
    x_w_mol_pflotran = 1.0 - x_g_mol_pflotran

    # load data from HDF5
    index_string = 'Time:  1.00000E-02 y/Gas_Density [kg_m^3]'
    rho_g_pflotran[0,:] = read_pflotran_output_1D(path+
		      '/1D_BL_MPHASE_CO2_INJ.h5',index_string,False)
    index_string = 'Time:  1.00000E-01 y/Gas_Density [kg_m^3]'
    rho_g_pflotran[1,:] = read_pflotran_output_1D(path+
		      '/1D_BL_MPHASE_CO2_INJ.h5',index_string,False)
    index_string = 'Time:  5.00000E-01 y/Gas_Density [kg_m^3]'
    rho_g_pflotran[2,:] = read_pflotran_output_1D(path+
		      '/1D_BL_MPHASE_CO2_INJ.h5',index_string,False)
    index_string = 'Time:  8.00000E-01 y/Gas_Density [kg_m^3]'
    rho_g_pflotran[3,:] = read_pflotran_output_1D(path+
		      '/1D_BL_MPHASE_CO2_INJ.h5',index_string,remove)
    ierr = check(rho_g_pflotran)
    
    # load data from HDF5
    index_string = 'Time:  1.00000E-02 y/Liquid_Density [kg_m^3]'
    rho_w_pflotran[0,:] = read_pflotran_output_1D(path+
		      '/1D_BL_MPHASE_CO2_INJ.h5',index_string,False)
    index_string = 'Time:  1.00000E-01 y/Liquid_Density [kg_m^3]'
    rho_w_pflotran[1,:] = read_pflotran_output_1D(path+
		      '/1D_BL_MPHASE_CO2_INJ.h5',index_string,False)
    index_string = 'Time:  5.00000E-01 y/Liquid_Density [kg_m^3]'
    rho_w_pflotran[2,:] = read_pflotran_output_1D(path+
		      '/1D_BL_MPHASE_CO2_INJ.h5',index_string,False)
    index_string = 'Time:  8.00000E-01 y/Liquid_Density [kg_m^3]'
    rho_w_pflotran[3,:] = read_pflotran_output_1D(path+
		      '/1D_BL_MPHASE_CO2_INJ.h5',index_string,remove)
    ierr = check(rho_w_pflotran)

    for k in range(len_t):
      for i in range(len_x):
        if MW_h2o*y_w_mol_pflotran[k,i] + MW_co2*y_g_mol_pflotran[k,i] > 0.0: #if no gas is present the mol fractions are all zero
          rho_g_mol[i] = 1000*rho_g_pflotran[k,i]/(y_g_mol_pflotran[k,i]*MW_co2 + y_w_mol_pflotran[k,i]*MW_h2o) 
        else:
          rho_g_mol[i] = 1000.0*rho_g_pflotran[i]/MW_h2o
        if MW_h2o*x_w_mol_pflotran[k,i] + MW_co2*x_g_mol_pflotran[k,i] > 0.0: # in case the same thing happens when no water is present
          rho_w_mol[i] = 1000*rho_w_pflotran[k,i]/(x_g_mol_pflotran[k,i]*MW_co2 + x_w_mol_pflotran[k,i]*MW_h2o) 
        else:
          rho_w_mol[i] = 1000.0*rho_w_pflotran[k,i]/MW_co2
        bot = rho_w_mol[i]*Sw_pflotran[k,i] + rho_g_mol[i]*(1.0 - Sw_pflotran[k,i])
        Cg_pflotran[k,i] = (x_g_mol_pflotran[k,i]*rho_w_mol[i]*Sw_pflotran[k,i] + y_g_mol_pflotran[k,i]*rho_g_mol[i]*(1.0 - Sw_pflotran[k,i]))/bot
        Cg_tot_pflotran[k,i] = A*phi*Cg_pflotran[k,i]*(x_edges[i+1] - x_edges[i])*bot 
      cu_Cg_pflotran[k] = np.sum(Cg_tot_pflotran[k,:])

    L1_percent_error = calc_L1_relative_error(Cgtot_soln[3,:],Cg_tot_pflotran[3,:],ierr)
    record_error(error_analysis,nxyz_record,dxyz_record,L1_percent_error,
                 nxyz,dxyz,try_count)

    test_pass = does_pass(L1_percent_error,try_count,num_tries)
    nxyz[0] = nxyz[0]*2.
    dxyz = lxyz/nxyz

  # Plot the PFLOTRAN and analytical solutions
  plot_1D_transient(path,t_yr,'yr',x_soln,Cgtot_soln,x_pflotran,Cg_tot_pflotran,
                    'Distance [m]','Total CO2 [mol]',"{0:.2f}".format(L1_percent_error))
  
  # Plot error analysis
  plot_error(error_analysis,nxyz_record,dxyz_record,path,try_count,1)
  
  # Add test result to report card
  add_to_report(path,test_pass,L1_percent_error,ierr)

  return;



