.. _transport-transient-1D-IC:

***********************************************
1D Transient Transport, Initial Condition
***********************************************
:ref:`transport-transient-1D-IC-description`

:ref:`transport-transient-1D-IC-pflotran-input`

:ref:`transport-transient-1D-IC-python`


.. _transport-transient-1D-IC-description:

The Problem Description
=======================

This problem is adapted from *Faure, G. (1991), Principles and Applications
of Inorganic Chemistry: A Comprehensive Textbook for Geology Students. New York: 
Macmillan Pub.Co.*, Section 19.3, pg.395, "Transport of Matter: Diffusion 
(Fick's Second Law)."

The domain is a 1D column extending along the positive x-axis and is made up of 
35x1x1 cubic grid cells with a length of 20 meters. The material is assigned 
the following properties: porosity = 1.0; tortuosity = 1.0; diffusion 
coefficient = 1.0e9 m2/s; which are homogenous in the domain. 

The initial concentration of a generic tracer is assigned to 1.0e-20 (a value
that is essentially zero but purposefully not exactly zero) everywhere, 
except at the center cell, where it is assigned a concentration of 20 Molar 
(mol/L). This initial pulse of tracer will diffuse over time, spreading 
itself out in both directions.

In PFLOTRAN, the initial concentration pulse in the center cell is homogeneous 
within that cell. However, the analytical solution describing the solution 
to Fick's Second Law for an initial concentration pulse resembles a Dirac Delta
function, which has an infinite value over a miniscule width. As the resolution
of the grid increases, the initial condition on the PFLOTRAN grid will approach 
the Dirac Delta, but practically this limit cannot be reached. Therefore, 
for early times in the simulation, when the Dirac Delta pulse is beginning to 
relax, the PFLOTRAN solution will not match the analytical solution very well.
However, the total amount of solute is the same, which can be verified by taking
the integral of the concentration curve (ie, area under the solution curve).

The simulation is run for 80 years. The solution is plotted and compared at 
10, 20, 40, and 80 years.

Fick's Second Law governs the evolving tracer concentration,

.. math:: {{\partial c} \over {\partial t}} = D {{\partial^{2} c} \over {\partial x^{2}}}

The solution is given by,

.. math:: c(x) = \frac{c_0}{\sqrt{4 \pi Dt}}e^{-\frac{x^{2}}{4Dt}}

where :math:`D` is the diffusion coefficient, and :math:`c_0` is the initial
tracer concentration.

.. figure:: ../qa_tests/transport/transient/1D/IC/visit_figure.png
   :width: 55 %
   
   The PFLOTRAN domain set-up.
   
.. figure:: ../qa_tests/transport/transient/1D/IC/subsurface_transport/comparison_plot.png
   :width: 49 %  
   :alt: If you do not see this image, you must run the QA test suite to generate this figure.
   
   Comparison of the PFLOTRAN vs. analytical solution for SUBSURFACE_TRANSPORT 
   mode.
   
   
   
.. _transport-transient-1D-IC-pflotran-input:

The PFLOTRAN Input File (SUBSURFACE_TRANSPORT Mode)
===================================================
The SUBSURFACE_TRANSPORT Mode PFLOTRAN input file can be downloaded 
:download:`here <../qa_tests/transport/transient/1D/IC/subsurface_transport/transient_1D_IC_subsurface_transport.in>`.

.. literalinclude:: ../qa_tests/transport/transient/1D/IC/subsurface_transport/transient_1D_IC_subsurface_transport.in


  
.. _transport-transient-1D-IC-python:

The Python Script
=================

.. literalinclude:: ../qa_tests/qa_tests_engine.py
  :pyobject: transport_transient_1D_IC
  
Refer to section :ref:`python-helper-functions` for documentation on the 
``qa_tests_helper`` module, which defines the helper functions used in the
Python script above.
