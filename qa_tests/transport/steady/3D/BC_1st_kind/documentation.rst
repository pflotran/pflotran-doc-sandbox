.. _transport-steady-3D-BC-1st-kind:

*********************************************
3D Steady Transport, BCs of 1st Kind 
*********************************************
:ref:`transport-steady-3D-BC-1st-kind-description`

:ref:`transport-subsurface_transport-steady-3D-BC-1st-kind-pflotran-input`

:ref:`transport-steady-3D-BC-1st-kind-python`



.. _transport-steady-3D-BC-1st-kind-description:

The Problem Description
=======================

This problem is based off of *Kolditz, et al. (2015), 
Thermo-Hydro-Mechanical-Chemical Processes in Fractured Porous Media: 
Modelling and Benchmarking, Closed Form Solutions, Springer International 
Publishing, Switzerland.* Section 2.1.5, pg.18, "A 3D Steady-State 
Temperature Distribution, Boundary Conditions of 1st Kind." Concentrations 
are used in place of temperatures to simulate subsurface transport, solute 
diffusion only.

The domain is a 1x1x1 meter cube extending along the positive 
x-axis, y-axis, and z-axis and is made up of 8x8x8 cubic grid cells with 
dimensions 0.125x0.125x0.125 meters. The domain material is assigned the following 
properties: diffusion coefficent *C* = 1.0E-9 m^2/s; 

The concentration is initially uniform at *C(c=0)* = 1.0 M.
The boundary concentrations are:

.. math::
  C(0,y,z) = C0 \left( {{0}+{y \over L}+{z \over L}} \right) \hspace{0.25in} x=0 \hspace{0.15in} face
  
  
  C(x,0,z) = C0 \left( {{x \over L}+{0}+{z \over L}} \right) \hspace{0.25in} y=0 \hspace{0.15in} face
  
  
  C(x,y,0) = C0 \left( {{x \over L}+{y \over L}+{0}} \right) \hspace{0.25in} z=0 \hspace{0.15in} face
  
  
  C(L,y,z) = C0 \left( {{L}+{y \over L}+{z \over L}} \right) \hspace{0.25in} x=L \hspace{0.15in} face
  
  
  C(x,L,z) = C0 \left( {{x \over L}+{L}+{z \over L}} \right) \hspace{0.25in} y=L \hspace{0.15in} face
  
  
  C(x,y,L) = C0 \left( {{x \over L}+{y \over L}+{L}} \right) \hspace{0.25in} z=L \hspace{0.15in} face

where L = 1 m and C0 = 1.0 M.
The simulation is run until the steady-state concentration field
develops. 

The LaPlace equation governs the steady-state concentration field,

.. math:: {{\partial^{2} C} \over {\partial x^{2}}} + {{\partial^{2} C} \over {\partial y^{2}}} + {{\partial^{2} C} \over {\partial z^{2}}} = 0

The solution is given by,

.. math:: C(x,y,z) = C_{c=0} \left( {x \over L}+{y \over L}+{z \over L} \right)

.. figure:: ../qa_tests/transport/steady/3D/BC_1st_kind/paraview_figure.png
   :width: 55 %
   
   The PFLOTRAN domain set-up.
   
.. figure:: ../qa_tests/transport/steady/3D/BC_1st_kind/subsurface_transport/comparison_plot.png
   :width: 49 %   
   :alt: If you do not see this image, you must run the QA test suite to generate this figure.
   
   Comparison of the PFLOTRAN vs. analytical solution for SUBSURFACE_TRANSPORT mode.
   
   
   
.. _transport-subsurface_transport-steady-3D-BC-1st-kind-pflotran-input:

The PFLOTRAN Input File (SUBSURFACE_TRANSPORT Mode)
===================================================
The Subsurface_transport Mode PFLOTRAN input file can be downloaded 
:download:`here <../qa_tests/transport/steady/3D/BC_1st_kind/subsurface_transport/3D_steady_transport_BC_1st_kind.in>`.

#.. literalinclude:: ../qa_tests/transport/steady/3D/BC_1st_kind/subsurface_transport/3D_steady_transport_BC_1st_kind.in
# The reason this is commented out is because it is very long due to the million
# regions that had to be created.

  
  
.. _transport-steady-3D-BC-1st-kind-python:

The Python Script
=================

.. literalinclude:: ../qa_tests/qa_tests_engine.py
  :pyobject: transport_steady_3D_BC1stkind
  
Refer to section :ref:`python-helper-functions` for documentation on the 
``qa_tests_helper`` module, which defines the helper functions used in the
Python script above.