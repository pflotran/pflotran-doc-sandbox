.. _gas-steady-1D-pressure-BC-1st-kind:

******************************************
1D Steady Gas (Pressure), BCs of 1st Kind
******************************************
:ref:`gas-steady-1D-pressure-BC-1st-kind-description`

:ref:`gas-general-steady-1D-pressure-BC-1st-kind-pflotran-input`

:ref:`gas-steady-1D-pressure-BC-1st-kind-python`



.. _gas-steady-1D-pressure-BC-1st-kind-description:

The Problem Description
=======================

This problem is adapted from *Kolditz, et al. (2015), 
Thermo-Hydro-Mechanical-Chemical Processes in Fractured Porous Media: 
Modelling and Benchmarking, Closed Form Solutions, Springer International 
Publishing, Switzerland.* Section 2.3.1, pg.40, "A 1D Steady-State 
Gas Pressure Distribution, Boundary Conditions of 1st Kind."

The domain is a 40x5x5 meter rectangular beam extending along the positive 
x-axis and is made up of 40x1x1 cubic grid cells with dimensions 1x5x5 
meters. The materials are assigned the following properties: porosity = 0.5; 
gas viscosity :math:`\mu` = 1.0e-5 Pa-s; permeability = 1e-15 m^2; 
gas saturation :math:`S_g` = 1.0 in the entire domain.

The gas pressure is initially uniform at *p(t=0)* = 1.5e5 Pa.
The boundary gas pressures are *p(x=0)* = 2.0e5 Pa and *p(x=L)* = 1.0e5 Pa, 
where L = 40 m.
The simulation is run until the steady-state pressure distribution
develops. 

The LaPlace equation governs the steady-state gas pressure distribution,

.. math:: {{\partial^{2} p^{2}} \over {\partial x^{2}}} = 0

The solution is given by,

.. math:: p(x) = \sqrt{ax + b}

When the gas pressure boundary conditions are applied, the solution becomes,

.. math:: p(x) = \sqrt{\left({p_1^2 - p_0^2}\right) {x \over L} + p_0^2}

.. figure:: ../qa_tests/gas/steady/1D/BC_1st_kind/visit_figure.png
   :width: 55 %
   
   The PFLOTRAN domain set-up.
   
.. figure:: ../qa_tests/gas/steady/1D/BC_1st_kind/general_mode/comparison_plot.png
   :width: 49 %  
   :alt: If you do not see this image, you must run the QA test suite to generate this figure.
   
   Comparison of the PFLOTRAN vs. analytical solution for GENERAL mode.
   
   
   
.. _gas-general-steady-1D-pressure-BC-1st-kind-pflotran-input:

The PFLOTRAN Input File (GENERAL Mode)
======================================
The GENERAL Mode PFLOTRAN input file can be downloaded 
:download:`here <../qa_tests/gas/steady/1D/BC_1st_kind/general_mode/1D_steady_gas_BC_1st_kind.in>`.

.. literalinclude:: ../qa_tests/gas/steady/1D/BC_1st_kind/general_mode/1D_steady_gas_BC_1st_kind.in


  
.. _gas-steady-1D-pressure-BC-1st-kind-python:

The Python Script
=================

.. literalinclude:: ../qa_tests/qa_tests_engine.py
  :pyobject: gas_steady_1D_BC1stkind
  
Refer to section :ref:`python-helper-functions` for documentation on the 
``qa_tests_helper`` module, which defines the helper functions used in the
Python script above.
