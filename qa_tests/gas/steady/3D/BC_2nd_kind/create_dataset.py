import sys
from h5py import *
import numpy as np

nx = 1; dx = 1; lx = 1
ny = 1; dy = 1; ly = 1
nz = 1; dz = 1; lz = 1
for k in range(len(sys.argv)-1):
  k = k + 1
  if sys.argv[k] == '-nx':
    nx = int(float(sys.argv[k+1]))
  if sys.argv[k] == '-ny':
    ny = int(float(sys.argv[k+1]))
  if sys.argv[k] == '-nz':
    nz = int(float(sys.argv[k+1]))
  if sys.argv[k] == '-dx':
    dx = float(sys.argv[k+1])
  if sys.argv[k] == '-dy':
    dy = float(sys.argv[k+1])
  if sys.argv[k] == '-dz':
    dz = float(sys.argv[k+1])
  if sys.argv[k] == '-lx':
    lx = float(sys.argv[k+1])
  if sys.argv[k] == '-ly':
    ly = float(sys.argv[k+1])
  if sys.argv[k] == '-lz':
    lz = float(sys.argv[k+1])

filename = 'dataset.h5'
h5file = File(filename,mode='w')

p0 = 1.0e5   # [Pa]
K = 1.0e-15  # [m2]
mu = 1.0e-5  # [Pa-s]

# 2D Surface:
# -----------------------------------------------------------------------------
# Gas flux boundary condition x=0 face
h5grp = h5file.create_group('cell_centered_surf_west')
h5grp.attrs['Dimension'] = np.string_('YZ')
h5grp.attrs['Cell Centered'] = True
h5grp.attrs['Interpolation Method'] = np.string_('STEP')
# Delta length between points [m]
h5grp.attrs['Discretization'] = [dy,dz]
# Location of origin
h5grp.attrs['Origin'] = [0.,0.]
# Load the dataset values
rarray = np.zeros((ny,nz),'=f8')
for j in range(ny):
  for k in range(nz):
    x = 0.
    y = (float(j)*ly)/(ny) + dy/2.
    z = (float(k)*lz)/(nz) + dz/2.
    p_gas = p0*np.sqrt(1.+(3./2.)*((x/lx)*(y/ly)+(z/lz)))
    rarray[j][k] = ((3.*K)/(4.*mu))*(p0**2/lx)*(y/ly)
    # flow is out: (-)
    rarray[j][k] = -1.0*rarray[j][k]/p_gas
h5dset = h5grp.create_dataset('Data', data=rarray)

# 2D Surface:
# -----------------------------------------------------------------------------
# Gas flux boundary condition x=L face
h5grp = h5file.create_group('cell_centered_surf_east')
h5grp.attrs['Dimension'] = np.string_('YZ')
h5grp.attrs['Cell Centered'] = True
h5grp.attrs['Interpolation Method'] = np.string_('STEP')
# Delta length between points [m]
h5grp.attrs['Discretization'] = [dy,dz]
# Location of origin
h5grp.attrs['Origin'] = [0.,0.]
# Load the dataset values
rarray = np.zeros((ny,nz),'=f8')
for j in range(ny):
  for k in range(nz):
    x = lx
    y = (float(j)*ly)/(ny) + dy/2.
    z = (float(k)*lz)/(nz) + dz/2.
    p_gas = p0*np.sqrt(1+(3/2)*((x/lx)*(y/ly)+(z/lz)))  # [Pa] from analytical solution
    rarray[j][k] = ((3.*K)/(4.*mu))*(p0**2/lx)*(y/ly)   # [Pa m/s]
    rarray[j][k] = rarray[j][k]/p_gas                   # [m/s]
h5dset = h5grp.create_dataset('Data', data=rarray)

# 2D Surface:
# -----------------------------------------------------------------------------
# Gas flux boundary condition z=0 face
h5grp = h5file.create_group('cell_centered_surf_bottom')
h5grp.attrs['Dimension'] = np.string_('XY')
h5grp.attrs['Cell Centered'] = True
h5grp.attrs['Interpolation Method'] = np.string_('STEP')
# Delta length between points [m]
h5grp.attrs['Discretization'] = [dx,dy]
# Location of origin
h5grp.attrs['Origin'] = [0.,0.]
# Load the dataset values
rarray = np.zeros((nx,ny),'=f8')
for i in range(nx):
  for j in range(ny):
    x = (float(i)*lx)/(nx) + dx/2.
    y = (float(j)*ly)/(ny) + dy/2.
    z = 0.
    p_gas = p0*np.sqrt(1.+(3./2.)*((x/lx)*(y/ly)+(z/lz)))
    rarray[i][j] = ((3.*K)/(4.*mu))*(p0**2/lz)
    # flow is out: (-)
    rarray[i][j] = -1.0*rarray[i][j]/p_gas
h5dset = h5grp.create_dataset('Data', data=rarray)

# 2D Surface:
# -----------------------------------------------------------------------------
# Gas flux boundary condition z=L face
h5grp = h5file.create_group('cell_centered_surf_top')
h5grp.attrs['Dimension'] = np.string_('XY')
h5grp.attrs['Cell Centered'] = True
h5grp.attrs['Interpolation Method'] = np.string_('STEP')
# Delta length between points [m]
h5grp.attrs['Discretization'] = [dx,dy]
# Location of origin
h5grp.attrs['Origin'] = [0.,0.]
# Load the dataset values
rarray = np.zeros((nx,ny),'=f8')
for i in range(nx):
  for j in range(ny):
    x = (float(i)*lx)/(nx) + dx/2.
    y = (float(j)*ly)/(ny) + dy/2.
    z = lz
    p_gas = p0*np.sqrt(1.+(3./2.)*((x/lx)*(y/ly)+(z/lz)))
    rarray[i][j] = ((3.*K)/(4.*mu))*(p0**2/lz)
    rarray[i][j] = rarray[i][j]/p_gas
h5dset = h5grp.create_dataset('Data', data=rarray)

# 2D Surface:
# -----------------------------------------------------------------------------
# Gas flux boundary condition y=0 face
h5grp = h5file.create_group('cell_centered_surf_south')
h5grp.attrs['Dimension'] = np.string_('XZ')
h5grp.attrs['Cell Centered'] = True
h5grp.attrs['Interpolation Method'] = np.string_('STEP')
# Delta length between points [m]
h5grp.attrs['Discretization'] = [dx,dz]
# Location of origin
h5grp.attrs['Origin'] = [0.,0.]
# Load the dataset values
rarray = np.zeros((nx,nz),'=f8')
for i in range(nx):
  for k in range(nz):
    x = (float(i)*lx)/(nx) + dx/2.
    y = 0.
    z = (float(k)*lz)/(nz) + dz/2.
    p_gas = p0*np.sqrt(1.+(3./2.)*((x/lx)*(y/ly)+(z/lz)))
    rarray[i][k] = x*((3.*K)/(4.*mu))*(p0**2/ly**2)
    # flow is out: (-)
    rarray[i][k] = -1.0*rarray[i][k]/p_gas
h5dset = h5grp.create_dataset('Data', data=rarray)

# 2D Surface:
# -----------------------------------------------------------------------------
# Gas flux boundary condition y=L face
h5grp = h5file.create_group('cell_centered_surf_north')
h5grp.attrs['Dimension'] = np.string_('XZ')
h5grp.attrs['Cell Centered'] = True
h5grp.attrs['Interpolation Method'] = np.string_('STEP')
# Delta length between points [m]
h5grp.attrs['Discretization'] = [dx,dz]
# Location of origin
h5grp.attrs['Origin'] = [0.,0.]
# Load the dataset values
rarray = np.zeros((nx,nz),'=f8')
for i in range(nx):
  for k in range(nz):
    x = (float(i)*lx)/(nx) + dx/2.
    y = ly
    z = (float(k)*lz)/(nz) + dz/2.
    p_gas = p0*np.sqrt(1.+(3./2.)*((x/lx)*(y/ly)+(z/lz)))
    rarray[i][k] = x*((3.*K)/(4.*mu))*(p0**2/ly**2)
    rarray[i][k] = rarray[i][k]/p_gas
h5dset = h5grp.create_dataset('Data', data=rarray)
